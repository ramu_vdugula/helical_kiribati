﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;        // really need to change this interface
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSIndicators
    {
        // return the vermdata xml, constructed from warehouse tables
        Task<System.Xml.Linq.XDocument> getVermData(int? year);
        Task<System.Xml.Linq.XDocument> getVermDataDistrict(string district, int? year);
        Task<IDataResult> makeWarehouse(int? year);
    }
}
