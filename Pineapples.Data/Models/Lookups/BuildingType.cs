using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBuildingTypes")]
    [Description(@"Types of buildings in the school Foreign key on Building and BuildingReview")]
    public partial class BuildingType : SimpleCodeTable
    {
        [Key]

        [Column("bdlgCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bdlg Code is required")]
        [Display(Name = "bdlg Code")]
        public override string Code { get; set; }

        [Column("bdlgDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "bdlg Description is required")]
        [Display(Name = "bdlg Description")]
        public override string Description { get; set; }

        [Column("bdlgDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bdlg Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("bdlgDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bdlg Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
