using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpTeacherRole")]
    [Description(@"Job roles of teachers. Role is collected on the Teacher Survey. Roles are further divided into RoleGrades. Appointments and salary and Establishment Plan are defined at the RoleGRade level.

The Role is linked to a Education Secgtor - so the teachers role determines their sector for reporting purposes.")]
    public partial class TeacherRole : SimpleCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("codeDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "code Sort")]
        public int? codeSort { get; set; }

        [Column("trsalLevelMin", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "trsal Level Min")]
        public string trsalLevelMin { get; set; }

        [Column("trsalLevelMax", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "trsal Level Max")]
        public string trsalLevelMax { get; set; }

        [Column("trSalaryPointMin", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "tr Salary Point Min")]
        public string trSalaryPointMin { get; set; }

        [Column("trSalaryPointMax", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "tr Salary Point Max")]
        public string trSalaryPointMax { get; set; }

        [Column("trSalaryPointMedian", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "tr Salary Point Median")]
        public string trSalaryPointMedian { get; set; }

        [Column("secCode", TypeName = "nvarchar")]
        [MaxLength(3)]
        [StringLength(3)]
        [Display(Name = "sec Code")]
        public string secCode { get; set; }

        [Column("trReportsTo", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "tr Reports To")]
        public string trReportsTo { get; set; }
    }
}
