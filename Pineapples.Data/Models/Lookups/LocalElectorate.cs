using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpElectorateL")]
    [Description(@"Local Electorates. A foreign key on schools")]
    public partial class LocalElectorate : SimpleCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Description")]
        public override string Description { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "code Sort")]
        public int? codeSort { get; set; }

        [Column("dID", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "District", Description ="Province in which the electorate is located")]
        [ClientLookup("districts")]
        public string dID { get; set; }

        [Column("censusCode", TypeName = "nvarchar")]
        [MaxLength(4)]
        [StringLength(4)]
        [Display(Name = "census Code", Description ="Code used for this electorate in national census")]
        public string censusCode { get; set; }

        [Column("codeStart", TypeName = "int")]
        [Display(Name = "code Start")]
        public int? codeStart { get; set; }

        [Column("codeEnd", TypeName = "int")]
        [Display(Name = "code End")]
        public int? codeEnd { get; set; }

        [Column("codeOld", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Old")]
        public string codeOld { get; set; }

        [Column("elgisID", TypeName = "int")]
        [Display(Name = "elgis ID")]
        public int? elgisID { get; set; }
    }
}
