--  
-- Script to Update dbo.TeacherIdentity in VCNSQL89.webhost4life.com.kemis 
-- Generated Wednesday, May 11, 2016, at 04:37 PM 
--  
-- Please backup VCNSQL89.webhost4life.com.kemis before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Updating dbo.TeacherIdentity Table'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherIdentityISTEnrol')
      ALTER TABLE [dbo].[ISTEnrol] DROP CONSTRAINT [TeacherIdentityISTEnrol]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherIdentityISTWaitList')
      ALTER TABLE [dbo].[ISTWaitList] DROP CONSTRAINT [TeacherIdentityISTWaitList]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_paAssessment_Teacher')
      ALTER TABLE [dbo].[paAssessment_] DROP CONSTRAINT [FK_paAssessment_Teacher]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAppointment_FK04')
      ALTER TABLE [dbo].[TeacherAppointment] DROP CONSTRAINT [TeacherAppointment_FK04]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAttendance_FK01')
      ALTER TABLE [dbo].[TeacherAttendance] DROP CONSTRAINT [TeacherAttendance_FK01]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherLinks_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherLinks] DROP CONSTRAINT [FK_TeacherLinks_TeacherIdentity]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherNotes_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherNotes] DROP CONSTRAINT [FK_TeacherNotes_TeacherIdentity]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherSurvey_FK02')
      ALTER TABLE [dbo].[TeacherSurvey] DROP CONSTRAINT [TeacherSurvey_FK02]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherTraining_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherTraining] DROP CONSTRAINT [FK_TeacherTraining_TeacherIdentity]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__TeacherId__tDOBE__44FF419A')
      ALTER TABLE [dbo].[TeacherIdentity] DROP CONSTRAINT [DF__TeacherId__tDOBE__44FF419A]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__TeacherId__tSrcI__45F365D3')
      ALTER TABLE [dbo].[TeacherIdentity] DROP CONSTRAINT [DF__TeacherId__tSrcI__45F365D3]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__TeacherId__tPayE__46E78A0C')
      ALTER TABLE [dbo].[TeacherIdentity] DROP CONSTRAINT [DF__TeacherId__tPayE__46E78A0C]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF EXISTS (SELECT name FROM sysobjects WHERE name = N'DF__TeacherId__tEstE__47DBAE45')
      ALTER TABLE [dbo].[TeacherIdentity] DROP CONSTRAINT [DF__TeacherId__tEstE__47DBAE45]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE TABLE [dbo].[tmp_TeacherIdentity] (
   [tID] [int] IDENTITY (1, 1) NOT NULL,
   [tRegister] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tPayroll] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tProvident] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tDOB] [datetime] NULL,
   [tDOBEst] [bit] NULL CONSTRAINT [DF__TeacherId__tDOBE__44FF419A] DEFAULT ((0)),
   [tSex] [nvarchar] (1) COLLATE Latin1_General_CI_AS NULL,
   [tNamePrefix] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tGiven] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
   [tMiddleNames] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tSurname] [nvarchar] (50) COLLATE Latin1_General_CI_AS NOT NULL,
   [tNameSuffix] [nvarchar] (20) COLLATE Latin1_General_CI_AS NULL,
   [tGivenSoundex] as (soundex([tGiven])) PERSISTED,
   [tSurnameSoundex] as (soundex([tSurname])) PERSISTED,
   [tSrcID] [int] NULL CONSTRAINT [DF__TeacherId__tSrcI__45F365D3] DEFAULT ((0)),
   [tSrc] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tComment] [ntext] COLLATE Latin1_General_CI_AS NULL,
   [tYearStarted] [int] NULL,
   [tYearFinished] [int] NULL,
   [tDatePSAppointed] [datetime] NULL,
   [tDatePSClosed] [datetime] NULL,
   [tCloseReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tPayErr] [bit] NULL CONSTRAINT [DF__TeacherId__tPayE__46E78A0C] DEFAULT ((0)),
   [tPayErrUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tPayErrDate] [datetime] NULL,
   [tEstErr] [bit] NULL CONSTRAINT [DF__TeacherId__tEstE__47DBAE45] DEFAULT ((0)),
   [tEstErrUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tEstErrDate] [datetime] NULL,
   [tSalaryPoint] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [tSalaryPointDate] [datetime] NULL,
   [tSalaryPointNextIncr] [datetime] NULL,
   [tLangMajor] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
   [tLangMinor] [nvarchar] (5) COLLATE Latin1_General_CI_AS NULL,
   [tpayptCode] [nvarchar] (10) COLLATE Latin1_General_CI_AS NULL,
   [tImage] [int] NULL,
   [tDateRegister] [datetime] NULL,
   [tDateRegisterEnd] [datetime] NULL,
   [tRegisterEndReason] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [tShortName] as ([dbo].[TeacherName]((1),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
   [tFullName] as ([dbo].[TeacherName]((2),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
   [tLongName] as ([dbo].[TeacherName]((3),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
   [pCreateDateTime] [datetime] NULL,
   [pCreateUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [pEditDateTime] [datetime] NULL,
   [pEditUser] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [pEditContext] [nvarchar] (50) COLLATE Latin1_General_CI_AS NULL,
   [pRowversion] [timestamp] NOT NULL
)
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   SET IDENTITY_INSERT [dbo].[tmp_TeacherIdentity] ON
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   INSERT INTO [dbo].[tmp_TeacherIdentity] ([tID], [tRegister], [tPayroll], [tProvident], [tDOB], [tDOBEst], [tSex], [tNamePrefix], [tGiven], [tMiddleNames], [tSurname], [tNameSuffix], [tSrcID], [tSrc], [tComment], [tYearStarted], [tYearFinished], [tDatePSAppointed], [tDatePSClosed], [tCloseReason], [tPayErr], [tPayErrUser], [tPayErrDate], [tEstErr], [tEstErrUser], [tEstErrDate], [tSalaryPoint], [tSalaryPointDate], [tSalaryPointNextIncr], [tLangMajor], [tLangMinor], [tpayptCode], [tImage], [tDateRegister], [tDateRegisterEnd], [tRegisterEndReason], [pCreateDateTime], [pCreateUser], [pEditDateTime], [pEditUser], [pEditContext])
   SELECT [tID], [tRegister], [tPayroll], [tProvident], [tDOB], [tDOBEst], [tSex], [tNamePrefix], [tGiven], [tMiddleNames], [tSurname], [tNameSuffix], [tSrcID], [tSrc], [tComment], [tYearStarted], [tYearFinished], [tDatePSAppointed], [tDatePSClosed], [tCloseReason], [tPayErr], [tPayErrUser], [tPayErrDate], [tEstErr], [tEstErrUser], [tEstErrDate], [tSalaryPoint], [tSalaryPointDate], [tSalaryPointNextIncr], [tLangMajor], [tLangMinor], [tpayptCode], [tImage], [tDateRegister], [tDateRegisterEnd], [tRegisterEndReason], [pCreateDateTime], [pCreateUser], [pEditDateTime], [pEditUser], [pEditContext]
   FROM [dbo].[TeacherIdentity]
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   SET IDENTITY_INSERT [dbo].[tmp_TeacherIdentity] OFF
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   DROP TABLE [dbo].[TeacherIdentity]
GO

sp_rename N'[dbo].[tmp_TeacherIdentity]', N'TeacherIdentity'

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   ALTER TABLE [dbo].[TeacherIdentity] ADD CONSTRAINT [aaaaaTeacherIdentity1_PK] PRIMARY KEY NONCLUSTERED ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_GivenSoundex] ON [dbo].[TeacherIdentity] ([tGivenSoundex])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_Payroll] ON [dbo].[TeacherIdentity] ([tPayroll])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_Surname] ON [dbo].[TeacherIdentity] ([tSurname])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   CREATE INDEX [IX_SurnameSoundex] ON [dbo].[TeacherIdentity] ([tSurnameSoundex])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditDelete] 
   ON  [dbo].[TeacherIdentity] 
   
   WITH EXECUTE as ''pineapples''
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit ''TeacherIdentity'', null, ''D'', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey, arowName)
	SELECT DISTINCT @AuditID
	, tID
	,  isnull(tFullName,'''') 
	 + '' '' + isnull(tPayroll,'''') 
 
	FROM DELETED	



END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditInsert] 
   ON  [dbo].TeacherIdentity 
    
   WITH EXECUTE as ''pineapples''

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from INSERTED
	
	
	exec @AuditID =  audit.LogAudit ''TeacherIdentity'', null, ''I'', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey)
	SELECT DISTINCT @AuditID
	, tID
	FROM INSERTED	

-- update the created and edited fields
	UPDATE TeacherIdentity
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE TeacherIdentity.tID = I.tID
	AND aLog.auditID = @auditID


END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditUpdate] 
   ON  [dbo].TeacherIdentity 

	-- pineapples has access to write to TeacherIdentityAudit
	WITH EXECUTE AS ''pineapples''
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		
		Select @NumChanges = count(*) from INSERTED
		
		
		exec @AuditID =  audit.LogAudit ''TeacherIdentity'', null, ''U'', @Numchanges
		
		INSERT INTO audit.AuditRow
		(auditID, arowKey)
		SELECT DISTINCT @AuditID
		, tID
		FROM INSERTED	



		
		INSERT INTO audit.AuditColumn
		(arowID, acolName, acolBefore, acolAfter)	
		
		SELECT arowID
		, ColumnName
		, DValue
		, IValue
		FROM audit.AuditRow Row
		INNER JOIN 
		(
			SELECT 
			I.tID
			, case num
				when 1 then ''tID''
				when 2 then ''tRegister''
				when 3 then ''tPayroll''
				when 4 then ''tProvident''
				when 5 then ''tDOB''
				when 6 then ''tDOBEst''
				when 7 then ''tSex''
				when 8 then ''tNamePrefix''
				when 9 then ''tGiven''
				when 10 then ''tMiddleNames''
				when 11 then ''tSurname''
				when 12 then ''tNameSuffix''
				when 13 then ''tGivenSoundex''
				when 14 then ''tSurnameSoundex''
				when 15 then ''tSrcID''
				when 16 then ''tSrc''
				when 17 then ''tComment''
				when 18 then ''tYearStarted''
				when 19 then ''tYearFinished''
				when 20 then ''tDatePSAppointed''
				when 21 then ''tDatePSClosed''
				when 22 then ''tCloseReason''
				when 23 then ''tPayErr''
				when 24 then ''tPayErrUser''
				when 25 then ''tPayErrDate''
				when 26 then ''tEstErr''
				when 27 then ''tEstErrUser''
				when 28 then ''tEstErrDate''
				when 29 then ''tSalaryPoint''
				when 30 then ''tSalaryPointDate''
				when 31 then ''tSalaryPointNextIncr''
				when 32 then ''tLangMajor''
				when 33 then ''tLangMinor''
				when 34 then ''tpayptCode''
				when 35 then ''tImage''
				when 36 then ''tDateRegister''
				when 37 then ''tDateRegisterEnd''
				when 38 then ''tRegisterEndReason''
			end ColumnName
			, case num
				when 1 then cast(D.tID as nvarchar(50))
				when 2 then cast(D.tRegister as nvarchar(50))
				when 3 then cast(D.tPayroll as nvarchar(50))
				when 4 then cast(D.tProvident as nvarchar(50))
				when 5 then cast(D.tDOB as nvarchar(50))
				when 6 then cast(D.tDOBEst as nvarchar(50))
				when 7 then cast(D.tSex as nvarchar(50))
				when 8 then cast(D.tNamePrefix as nvarchar(50))
				when 9 then cast(D.tGiven as nvarchar(50))
				when 10 then cast(D.tMiddleNames as nvarchar(50))
				when 11 then cast(D.tSurname as nvarchar(50))
				when 12 then cast(D.tNameSuffix as nvarchar(50))
				when 15 then cast(D.tSrcID as nvarchar(50))
				when 16 then cast(D.tSrc as nvarchar(50))
				--when 17 then cast(D.tComment as nvarchar(50))
				when 18 then cast(D.tYearStarted as nvarchar(50))
				when 19 then cast(D.tYearFinished as nvarchar(50))
				when 20 then cast(D.tDatePSAppointed as nvarchar(50))
				when 21 then cast(D.tDatePSClosed as nvarchar(50))
				when 22 then cast(D.tCloseReason as nvarchar(50))
				when 23 then cast(D.tPayErr as nvarchar(50))
				when 24 then cast(D.tPayErrUser as nvarchar(50))
				when 25 then cast(D.tPayErrDate as nvarchar(50))
				when 26 then cast(D.tEstErr as nvarchar(50))
				when 27 then cast(D.tEstErrUser as nvarchar(50))
				when 28 then cast(D.tEstErrDate as nvarchar(50))
				when 29 then cast(D.tSalaryPoint as nvarchar(50))
				when 30 then cast(D.tSalaryPointDate as nvarchar(50))
				when 31 then cast(D.tSalaryPointNextIncr as nvarchar(50))
				when 32 then cast(D.tLangMajor as nvarchar(50))
				when 33 then cast(D.tLangMinor as nvarchar(50))
				when 34 then cast(D.tpayptCode as nvarchar(50))
				when 35 then cast(D.tImage as nvarchar(50))
				when 36 then cast(D.tDateRegister as nvarchar(50))
				when 37 then cast(D.tDateRegisterEnd as nvarchar(50))
				when 38 then cast(D.tRegisterEndReason as nvarchar(50))
			end dValue
			, case num
				when 1 then cast(I.tID as nvarchar(50))
				when 2 then cast(I.tRegister as nvarchar(50))
				when 3 then cast(I.tPayroll as nvarchar(50))
				when 4 then cast(I.tProvident as nvarchar(50))
				when 5 then cast(I.tDOB as nvarchar(50))
				when 6 then cast(I.tDOBEst as nvarchar(50))
				when 7 then cast(I.tSex as nvarchar(50))
				when 8 then cast(I.tNamePrefix as nvarchar(50))
				when 9 then cast(I.tGiven as nvarchar(50))
				when 10 then cast(I.tMiddleNames as nvarchar(50))
				when 11 then cast(I.tSurname as nvarchar(50))
				when 12 then cast(I.tNameSuffix as nvarchar(50))
				when 15 then cast(I.tSrcID as nvarchar(50))
				when 16 then cast(I.tSrc as nvarchar(50))
				--when 17 then cast(I.tComment as nvarchar(50))
				when 18 then cast(I.tYearStarted as nvarchar(50))
				when 19 then cast(I.tYearFinished as nvarchar(50))
				when 20 then cast(I.tDatePSAppointed as nvarchar(50))
				when 21 then cast(I.tDatePSClosed as nvarchar(50))
				when 22 then cast(I.tCloseReason as nvarchar(50))
				when 23 then cast(I.tPayErr as nvarchar(50))
				when 24 then cast(I.tPayErrUser as nvarchar(50))
				when 25 then cast(I.tPayErrDate as nvarchar(50))
				when 26 then cast(I.tEstErr as nvarchar(50))
				when 27 then cast(I.tEstErrUser as nvarchar(50))
				when 28 then cast(I.tEstErrDate as nvarchar(50))
				when 29 then cast(I.tSalaryPoint as nvarchar(50))
				when 30 then cast(I.tSalaryPointDate as nvarchar(50))
				when 31 then cast(I.tSalaryPointNextIncr as nvarchar(50))
				when 32 then cast(I.tLangMajor as nvarchar(50))
				when 33 then cast(I.tLangMinor as nvarchar(50))
				when 34 then cast(I.tpayptCode as nvarchar(50))
				when 35 then cast(I.tImage as nvarchar(50))
				when 36 then cast(I.tDateRegister as nvarchar(50))
				when 37 then cast(I.tDateRegisterEnd as nvarchar(50))
				when 38 then cast(I.tRegisterEndReason as nvarchar(50))
			end IValue

		FROM INSERTED I 
		INNER JOIN DELETED D
			ON I.tID = D.tID
		CROSS JOIN metaNumbers
		WHERE num between 1 and 38
		) Edits
		ON Edits.tID = Row.arowKey
		WHERE Row.auditID = @AuditID
		AND isnull(DVAlue,'''') <> isnull(IValue,'''')

		
		-- update the last edited field	
		UPDATE TeacherIdentity
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE TeacherIdentity.tID = I.tID
		AND aLog.auditID = @auditID
	
END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1

exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 06 2010
-- Description:	Enforce unique payroll numbers, registration numbers, Provident numbers
-- =============================================
CREATE TRIGGER dbo.TeacherIdentity_Validations
   ON  dbo.TeacherIdentity 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
begin try
	if (UPDATE(tPayroll) and common.Sysparam(''TEACHER_UNIQUEPAYROLL'') = ''Y'') begin
		IF Exists(
					Select TI.tPayroll
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tPayroll = INSERTED.tPayroll		-- this must be non-null
					GROUP BY TI.tPayroll
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR(''<ValidationError field="tPayroll">
						This update would create a duplicate value in Payroll number.
						</ValidationError>'', 16,1)
		end
	end
	
	if (UPDATE(tProvident) and common.Sysparam(''TEACHER_UNIQUEPROVIDENT'') = ''Y'') begin
		IF Exists(
					Select TI.tProvident
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tProvident = INSERTED.tProvident		-- this must be non-null
					GROUP BY TI.tProvident
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR(''<ValidationError field="tProvident">
						This update would create a duplicate value in Provident number.
						</ValidationError>'', 16,1)
		end
	end	

	if (UPDATE(tRegister) and common.Sysparam(''TEACHER_UNIQUEREG'') = ''Y'') begin
		IF Exists(
					Select TI.tRegister
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tRegister = INSERTED.tRegister		-- this must be non-null
					GROUP BY TI.tRegister
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR(''<ValidationError field="tProvident">
						This update would create a duplicate value in Registration number.
						</ValidationError>'', 16,1)
		end
	end	


end try
		

		
 /****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	rollback transaction
	select @errorMessage = @errorMessage + '' The transaction was rolled back.''

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
end catch
END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'dbo.TeacherIdentity Table Updated Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Update dbo.TeacherIdentity Table'
END
GO


--  
-- Script to Create pTeacherRead.TeacherReadEx in VCNSQL89.webhost4life.com.kemis 
-- Generated Wednesday, May 11, 2016, at 04:37 PM 
--  
-- Please backup VCNSQL89.webhost4life.com.kemis before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Creating pTeacherRead.TeacherReadEx Procedure'
GO

SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, QUOTED_IDENTIFIER, CONCAT_NULL_YIELDS_NULL ON
GO

SET NUMERIC_ROUNDABORT OFF
GO


exec('-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 05 2016
-- Description:	Extended read of teacher for web site
-- =============================================
CREATE PROCEDURE pTeacherRead.TeacherReadEx
	@teacherID int
AS
BEGIN

	SET NOCOUNT ON;

    Select * 
	from TeacherIdentity
	WHERE tID = @teacherID

	Select *		-- to do trim this down?
	from TeacherSurvey
	WHERE tID = @teacherID

	Select * 
	from TeacherAppointment
	WHERE tID = @teacherID




END
')
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'pTeacherRead.TeacherReadEx Procedure Added Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Add pTeacherRead.TeacherReadEx Procedure'
END
GO


--  
-- Script to Update dbo.TeacherIdentity in VCNSQL89.webhost4life.com.kemis 
-- Generated Wednesday, May 11, 2016, at 04:37 PM 
--  
-- Please backup VCNSQL89.webhost4life.com.kemis before executing this script
--  
-- ** Script Begin **
BEGIN TRANSACTION
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO

PRINT 'Updating dbo.TeacherIdentity Table'
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherIdentityISTEnrol')
      ALTER TABLE [dbo].[ISTEnrol] ADD CONSTRAINT [TeacherIdentityISTEnrol] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherIdentityISTWaitList')
      ALTER TABLE [dbo].[ISTWaitList] ADD CONSTRAINT [TeacherIdentityISTWaitList] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_paAssessment_Teacher')
      ALTER TABLE [dbo].[paAssessment_] ADD CONSTRAINT [FK_paAssessment_Teacher] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAppointment_FK04')
      ALTER TABLE [dbo].[TeacherAppointment] ADD CONSTRAINT [TeacherAppointment_FK04] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherAttendance_FK01')
      ALTER TABLE [dbo].[TeacherAttendance] ADD CONSTRAINT [TeacherAttendance_FK01] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherLinks_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherLinks] ADD CONSTRAINT [FK_TeacherLinks_TeacherIdentity] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherNotes_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherNotes] ADD CONSTRAINT [FK_TeacherNotes_TeacherIdentity] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'TeacherSurvey_FK02')
      ALTER TABLE [dbo].[TeacherSurvey] ADD CONSTRAINT [TeacherSurvey_FK02] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID])
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
   IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = N'FK_TeacherTraining_TeacherIdentity')
      ALTER TABLE [dbo].[TeacherTraining] ADD CONSTRAINT [FK_TeacherTraining_TeacherIdentity] FOREIGN KEY ([tID]) REFERENCES [dbo].[TeacherIdentity] ([tID]) ON DELETE CASCADE
GO

IF @@ERROR <> 0
   IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION
GO

IF @@TRANCOUNT = 1
BEGIN
   PRINT 'dbo.TeacherIdentity Table Updated Successfully'
   COMMIT TRANSACTION
END ELSE
BEGIN
   PRINT 'Failed To Update dbo.TeacherIdentity Table'
END
GO
