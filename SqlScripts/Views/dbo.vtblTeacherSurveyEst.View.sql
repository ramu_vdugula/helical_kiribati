SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTeacherSurveyEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year OF Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.bestssID, vtblTeacherSurvey.*
FROM
ESTIMATE_BestSurveyTeachers AS ESTIMATE
INNER JOIN
vtblTeacherSurvey
ON ESTIMATE.bestssID = vtblTeacherSurvey.ssID;
GO

