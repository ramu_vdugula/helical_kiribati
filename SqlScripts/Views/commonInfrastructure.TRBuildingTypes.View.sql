SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [commonInfrastructure].[TRBuildingTypes]
AS
Select bdlgCode
, isnull(CASE dbo.userLanguage()
	WHEN 0 THEN bdlgDescription
	WHEN 1 THEN bdlgDescriptionL1
	WHEN 2 THEN bdlgDescriptionL2

END,bdlgDescription) AS bdlgDescription
from lkpBuildingTypes;
GO

