SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveyGeo]
AS
SELECT [Survey ID]
      ,[Survey Data Year]
      ,[School No]
      ,[School Name]
      ,[SchoolID_Name]
      ,[SchoolID_Name_Type]
      ,[SchoolID_Name_District]
      ,[District Code]
      ,[District]
      ,[District Short]
      ,[Island]
      ,[Region]
      ,[Local Electorate]
      ,[Local Electorate No]
      ,[National Electorate]
      ,[National Electorate No]
      ,[AuthorityCode]
      ,[Authority]
      ,[LanguageCode]
      ,[Language]
      ,[SchoolTypeCode]
      ,[School Class]
      ,[SchoolType]
      ,[elgisID]
      ,[engisID]
      ,[igisID]
      ,[dgisID]
       ,[NumberOfSchools]
       , schElev Elevation
       , schLat Latitude
       , schLong Longitude
 FROM [DimensionSchoolSurveyNoYear]
	INNER JOIN Schools S
		ON [DimensionSchoolSurveyNoYear].[School No] = S.schNo
GO

