SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherReadX].[TEacherLastPayslip]
AS
	Select
	tpsID
	,tpsPayroll
	, tpsPeriodStart
	, tpsPeriodEnd
	, tpsSalaryPoint
	, tpsPosition
	, tpsPayPoint
	FROM
	(Select
			tpsID
	,tpsPayroll
	, tpsPeriodStart
	, tpsPeriodEnd
	, tpsSalaryPoint
	, tpsPosition
	, tpsPayPoint
		, row_number() OVER (Partition BY tpsPayroll ORDER BY tpsPeriodEnd DESC) row
	FROM TeacherPayslips
	) sub
	WHERE row = 1
GO

