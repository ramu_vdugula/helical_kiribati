SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2019
-- Description:	Warehouse - Enrolment data by District
--
-- This is a simple consoilidation of warehouse.enrol to group by Nation
-- ( ie SurveyYear, ClassLevel Age, GenderCode but NOT by school or district)
--
-- The family of related objects:
-- Base data:
--		warehouse.Enrol
-- Consolidations:
--		warehouse.tableEnrol
--		warehouse.EnrolDistrict
--		warehouse.EnrolNation

-- Consolitations including population: (these do not break down by class level)
--		warehouse.enrolPopDistrict
--		warehouse.EnrolPopNation

-- 'Report' versions ie denormalised by Gender
--		warehouse.EnrolR
--		warehouse.EnrolDistrictR
--		warehouse.EnrolNationR
--		warehouse.enrolPopDistrictR
--		warehouse.EnrolPopNationR
-- =============================================
CREATE VIEW
[warehouse].[EnrolNation]
AS
Select SurveyYear
, ClassLevel
, Age
, GenderCode

, sum(Enrol) Enrol
, sum(Rep) Rep
, sum(Trin) Trin
, sum(Trout) Trout
, sum(Boarders) Boarders
, sum(Disab) Disab
, sum(Dropout) Dropout
, sum(PSA) PSA

from warehouse.tableEnrol E
GROUP BY
SurveyYear
, ClassLevel
, Age
, GenderCode
GO

