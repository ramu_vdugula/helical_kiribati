SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVActiveSchools]
AS
SELECT schoolLifeYears.svyYear, schoolLifeYears.schClosed, Schools.schNo AS [School No], Schools.schName AS [School Name], Schools.schType AS SchoolTypeCode, TRSchoolTypes.stDescription AS SchoolType, Schools.iCode AS [Island Code], Islands.iName AS Island, Islands.iGroup AS [District Code], Districts.dName AS District, Schools.schElectN AS [Electorate N Code], lkpElectorateL.codeDescription AS [Electorate N], DimensionAuthority.*, DimensionLanguage.*
FROM (Districts INNER JOIN (Islands INNER JOIN (lkpElectorateL RIGHT JOIN (lkpElectorateN RIGHT JOIN (((schoolLifeYears INNER JOIN Schools ON schoolLifeYears.schNo = Schools.schNo) LEFT JOIN DimensionAuthority ON Schools.schAuth = DimensionAuthority.AuthorityCode) LEFT JOIN DimensionLanguage ON Schools.schLang = DimensionLanguage.LanguageCode) ON lkpElectorateN.codeCode = Schools.schElectN) ON lkpElectorateL.codeCode = Schools.schElectL) ON Islands.iCode = Schools.iCode) ON Districts.dID = Islands.iGroup) LEFT JOIN TRSchoolTypes ON Schools.schType = TRSchoolTypes.stCode;
GO
GRANT SELECT ON [dbo].[PIVActiveSchools] TO [public] AS [dbo]
GO

