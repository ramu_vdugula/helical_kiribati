SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create VIEW [dbo].[vtblParentContributions]
AS
SELECT     ptID,ssID,ptCode,ptCol term,ptRow contTypeCode,ptLevel,ptM,ptF
FROM       dbo.PupilTables
WHERE     dbo.PupilTables.ptCode BETWEEN 'PCONT' And 'PCONTZZZZ'
GO

