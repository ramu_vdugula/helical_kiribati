SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRWorkITemType] AS
SELECT codeCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN codeDescription
    WHEN 1 THEN codeDescriptionL1
    WHEN 2 THEN codeDescriptionL2
 END,codeDescription) AS codeDescription

, witUseBuilding
, witBuildingType
, witCreateBuilding
,[witEstUnitCost]
,[witEstUOM]
,[witEstFormula]
,[witEstIM]
, witGroup
FROM dbo.lkpWorkITemType
GO
GRANT SELECT ON [dbo].[TRWorkITemType] TO [public] AS [dbo]
GO

