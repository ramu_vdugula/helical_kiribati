SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paTeacherLatestAssessmentRpt]
WITH VIEW_METADATA
AS
SELECT TI.tID
, TI.tFullName
, TI.tSurname
, TI.tGiven
, TI.tDoB
, TI.tSex
, TI.tPayroll

, A.paID
, A.paSchNo
, A.paDate
, A.paConductedBy
, A.paYear
, A.pafrmCode
, A.pEditUser
, A.pEditDateTime
, A.paScore

, S.schName
, I.iName
, D.dID
, D.dName
, AUTH.*
, schType

from TeacherIdentity TI
LEFT JOIN
	(
		select  ROW_NUMBER() over (PARTITION BY tID ORDER BY paDate DESC, paID DESC) seq
		, *
		from paAssessment
	 ) A
		ON A.tID = TI.tID
		AND A.seq = 1
	LEFT JOIN Schools S
		ON A.paSchNo = S.schNo
	LEFT JOIN DimensionAuthority AUTH
			ON AUTH.AuthorityCode = S.schAuth
	LEFT JOIN Islands I
		ON S.iCode = I.iCode
	LEFT JOIN Districts D
		ON I.iGroup = D.dID
GO

