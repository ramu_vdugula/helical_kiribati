SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRAuthorityType]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription,
codeGroup
FROM         dbo.lkpAuthorityType
GO
GRANT SELECT ON [dbo].[TRAuthorityType] TO [public] AS [dbo]
GO

