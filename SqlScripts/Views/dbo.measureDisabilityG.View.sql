SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[measureDisabilityG]
AS
SELECT schNo
, LifeYear SurveyYear
, Estimate
, SurveyDimensionssID SurveyDimensionID
, ptLevel ClassLevel
, GenderCode
, disCode
, case when GenderCode = 'M' then ptM
		when GenderCode = 'F' then ptF
	end Disab
from dbo.tfnESTIMATE_BestSurveyEnrolments() B
	INNER JOIN vtblDisabilities R
		ON B.bestssID = R.ssID
	CROSS JOIN dimensionGender G
GO

