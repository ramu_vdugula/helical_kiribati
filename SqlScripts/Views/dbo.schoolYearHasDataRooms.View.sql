SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[schoolYearHasDataRooms]
AS
SELECT     DISTINCT S.schNo, S.svyYear,S.ssID,R.rmType, R.rmQualityCode,  Q.ssqLevel

FROM         dbo.SchoolSurvey AS S
			INNER JOIN Rooms R
			on S.SSID = R.ssID
			LEFT OUTER JOIN
             dbo.tfnQualityIssues('Rooms', null) AS Q
				ON R.ssID = Q.ssID and R.rmQualityCode = Q.ssqSubitem
WHERE     (q.ssqLevel is null or q.ssqLevel <2)
GO

