SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRSubjects]
AS
SELECT     subjCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN subjName
	WHEN 1 THEN subjNameL1
	WHEN 2 THEN subjNameL2

END, subjName) AS subjName,subjSort,ifID

FROM         dbo.lkpSubjects
GO
GRANT SELECT ON [dbo].[TRSubjects] TO [public] AS [dbo]
GO

