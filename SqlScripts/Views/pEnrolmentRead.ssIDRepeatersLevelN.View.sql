SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pEnrolmentRead].[ssIDRepeatersLevelN]
as
SELECT     Repeaters.ssID,
ptLevel AS LevelCode,
lvlYear as YearOfEd,
SUM(ptM) AS repM,
SUM(ptF) AS repF,
SUM(isnull(ptM,0) + isnull(ptF,0)) AS Rep,
sum(case when ptAge = svyPSAge + L.lvlYear - 1 then ptM else null end) nRepM,
sum(case when ptAge = svyPSAge + L.lvlYear - 1 then ptF else null end) nRepF,
sum(case when ptAge = svyPSAge + L.lvlYear - 1
		then isnull(ptM,0) + isnull(ptF,0) else null end
	) nRep,
svyPSAge + L.lvlYear - 1 OfficialAge

FROM   pEnrolmentRead.vtblRepeaters Repeaters
INNER JOIN lkpLevels L
ON Repeaters.ptLevel = L.codecode
INNER JOIN SchoolSurvey SS
ON SS.ssID = Repeaters.ssID
LEFT JOIN Survey S
ON S.svyYear = SS.svyYEar
GROUP BY Repeaters.ssID, ptLevel, lvlYear, svyPSAge
GO

