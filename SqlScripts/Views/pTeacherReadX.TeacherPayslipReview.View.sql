SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherReadX].[TeacherPayslipReview]
AS

Select tPayroll, tFullName, tDOB, tSex, LastSurvey, LastPayslip, LastAppt
, tYearStarted, tYearFinished, tDatePSAppointed, tDatePSClosed, TI.tID
, pCreateDateTime, pCreateUser
FROM TEacherIdentity TI
LEFT JOIN (Select TS.tID, max(svyYear) LastSurvey
				from TEacherSurvey TS
				INNER JOIN SchoolSurvey SS ON TS.ssID = SS.ssID
				GROUP BY tID
			) LS
		ON TI.tID = LS.tID
LEFT JOIN
	( Select tpsPayroll, max(tpsPEriodEnd) LastPaySlip
		FROM TEacherPayslips
		GROUP BY tpsPayroll
	) LastPayslip
	ON TI.tPayroll = LastPayslip.tpsPayroll
LEFT JOIN (SELECT tID, max(taDate) LastAppt
			from TeacherAppointment
			GROUP BY tID) LA
	ON TI.tID = LA.tID

WHERE
tPayroll is not null
and tDatePSClosed is null
GO

