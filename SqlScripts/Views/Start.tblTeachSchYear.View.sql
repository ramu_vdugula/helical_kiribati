SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Start].[tblTeachSchYear]
AS
Select
'T' + replace(right('00000000000000000000' + convert(nvarchar(12), tID),12),'-','9') MoE_PF
, svyYear TeachingYear
, schNo	   SchoolID
, null		[User]
, null		Entered
FRom TeacherSurvey TS
	INNER JOIN SchoolSurvey SS
		ON TS.ssID = SS.ssID
GO

