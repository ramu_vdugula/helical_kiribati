SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsRoomsClass]
AS
Select
ssID,
R.rmType Code,
lkpRoomTypes.codeDescription AS [Type],
R.rmID,
R.rmYear AS YearBuilt,
R.rmNo AS RoomNo,
R.rmSize AS RoomSize,
case when (rmSize > 0 ) then 1 else 0 end AS SizeSupplied,
R.rmLevel AS ClassLevel,
R.rmMaterials Material,
R.rmMaterialRoof MaterialRoof,
R.rmMaterialFloor MaterialFloor,
R.rmCondition AS Condition,
1 AS NumRooms,
case when rmYear is null then null else P.ptName end AS YearBracket,
case when [rmShareType] Is Null then 0 else 1 end AS SharedRoom,
R.rmShareType AS ShareRoomTypeCode
FROM Rooms R
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType
	LEFT JOIN [Partitions] P
--- 14 11 2009 SRVU0017 schools with rmYear is null were counted for every partition
---- 25 10 2013 same problem solved differently
	ON P.ptSet='YearBuilt'
	AND R.rmYear between ptMin and ptMax
GO

