SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		BRian Lewis
-- Create date: 1 12 2013
-- Description:	Somali (Puntland) fee totals - note that we went from collecting the total to the charge per head in 2013
-- =============================================
CREATE VIEW [dbo].[PIVColsFeesSO]
AS

Select SS.ssID
, EE.Enrol
, FM.FeePerMonth
, NP.NonPay
, Enrol - isnull(NonPay,0) Paying
, case when SS.svyYEar >= 2013 then
	(Enrol - isnull(NonPay,0) ) * convert(float,FeePerMonth)
	else FeePerMonth
	end EstPaidPerMonth
From SchoolSurvey SS
LEFT JOIN
( Select ssID
		,ptSum  FeePerMonth
	from PupilTables
	WHERE ptCode = 'FEES'
	AND ptCol = 'SHL_M'
) FM
	ON  SS.ssID = FM.ssID
LEFT JOIN
( Select ssID
		,ptSum  NonPay
	from PupilTables
	WHERE ptCode = 'FEES'
	AND ptCol = 'NONPAY'
) NP
	ON  SS.ssID = NP.ssID
LEFT JOIN
	(Select ssID
		, sum(enSum) Enrol
	  FROM Enrollments
	  GROUP BY ssID
	  ) EE
	  ON SS.ssID = EE.ssID
GO

