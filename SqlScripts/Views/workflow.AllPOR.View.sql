SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [workflow].[AllPOR]
as


SELECT
	POR.*
	, Schools.schName
	, TeacherIdentity.tRegister
	, TeacherIdentity.tPayroll
	, TeacherIdentity.tProvident
	, TeacherIdentity.tNamePrefix
	, TeacherIdentity.tGiven
	, TeacherIdentity.tMiddleNames
	, TeacherIdentity.tSurname
	, TeacherIdentity.tNameSuffix
	, TeacherIdentity.tFullName
	, FlowActions.wfqID
	, porStatus.statusDesc
	, Actions.actName
	, Actions.actRole
FROM
	POR
	INNER JOIN workflow.Flows Flows
		ON POR.flowID = Flows.flowID
	INNER JOIN workflow.FlowActions FlowActions
		on POR.flowID = FlowActions.flowID
		AND POR.porStatus = FlowActions.wfqStatus
	INNER JOIN workflow.Actions Actions
		ON FlowActions.actID = Actions.actID
	INNER JOIN workflow.porStatus porStatus
		ON POR.porStatus = porStatus.statusCode
	INNER JOIN Schools
		ON POR.schNo = Schools.schNo
	LEFT JOIN TeacherIdentity
		ON POR.tID = TeacherIdentity.tID
GO

