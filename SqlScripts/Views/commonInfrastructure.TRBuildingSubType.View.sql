SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [commonInfrastructure].[TRBuildingSubType]
AS
Select bsubCode
	, bdlgCode
, isnull(CASE dbo.userLanguage()
	WHEN 0 THEN bsubDescription
	WHEN 1 THEN bsubDescriptionL1
	WHEN 2 THEN bsubDescriptionL2

END,bsubDescription) AS bsubDescription
from lkpBuildingSubType;
GO

