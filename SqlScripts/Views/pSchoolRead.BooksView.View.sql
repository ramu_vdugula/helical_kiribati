SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[BooksView]
WITH VIEW_METADATA
AS
Select B.*
, isnull(pubName, bkPublisher) Publisher
, isnull(subjName, bkSubject) Subject
, isnull(TG.codeDescription, bkTG) TG
from Books B
	LEFT JOIN TRSubjects S
		ON B.bkSubject = S.subjCode
	LEFT JOIN Publishers P
		ON B.bkPublisher = P.pubCode
	LEFT JOIN TRBookTG TG
		ON B.bkTG = TG.codeCode
GO

