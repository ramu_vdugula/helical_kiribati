SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRGrantRules] AS
SELECT grCode,
isnull(CASE dbo.userLanguage()
    WHEN 0 THEN grName
    WHEN 1 THEN grNameL1
    WHEN 2 THEN grNameL2
 END,grName) AS grName
 FROM dbo.GrantRules
GO
GRANT SELECT ON [dbo].[TRGrantRules] TO [public] AS [dbo]
GO

