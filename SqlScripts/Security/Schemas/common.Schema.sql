CREATE SCHEMA [common]
GO
GRANT EXECUTE ON SCHEMA::[common] TO [public] AS [dbo]
GO
GRANT SELECT ON SCHEMA::[common] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON SCHEMA::[common] TO [public] AS [dbo]
GO

