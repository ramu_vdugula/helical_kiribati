SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Partitions](
	[ptID] [int] IDENTITY(1,1) NOT NULL,
	[ptSet] [nvarchar](50) NULL,
	[ptName] [nvarchar](50) NULL,
	[ptMin] [int] NULL,
	[ptMax] [float] NULL,
 CONSTRAINT [aaaaaPartitions1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Partitions] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Partitions] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Partitions] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Partitions] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Partitions] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Partitions] ADD  CONSTRAINT [DF__Partition__ptMin__10216507]  DEFAULT ((0)) FOR [ptMin]
GO
ALTER TABLE [dbo].[Partitions] ADD  CONSTRAINT [DF__Partition__ptMax__11158940]  DEFAULT ((0)) FOR [ptMax]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Partiotions are set of number ranges used to divide up numeric fields for reporting. YearBuilt (for buildings) and TeacherAge are examples. Maintained through lookups.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Partitions'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Reporting' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Partitions'
GO

