SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleGrades](
	[rgCode] [nvarchar](20) NOT NULL,
	[rgDescription] [nvarchar](50) NOT NULL,
	[rgDescriptionL1] [nvarchar](50) NULL,
	[rgDescriptionL2] [nvarchar](50) NULL,
	[roleCode] [nvarchar](50) NOT NULL,
	[rgSalaryLevelMin] [nvarchar](5) NULL,
	[rgSalaryLevelMax] [nvarchar](5) NULL,
	[rgSalaryPointMedian] [nvarchar](10) NULL,
	[rgSort] [int] NULL,
	[rgSalaryLevelRange]  AS (case when [rgSalaryLevelMin]=[rgSalaryLevelMax] then [rgSalaryLevelMin] else (isnull([rgSalaryLevelMin],'')+'-')+isnull([rgSalaryLevelMax],'') end),
 CONSTRAINT [PK_RoleGrades] PRIMARY KEY CLUSTERED 
(
	[rgCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[RoleGrades] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[RoleGrades] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[RoleGrades] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[RoleGrades] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[RoleGrades] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[RoleGrades]  WITH CHECK ADD  CONSTRAINT [FK_RoleGrades_lkpTeacherRole] FOREIGN KEY([roleCode])
REFERENCES [dbo].[lkpTeacherRole] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[RoleGrades] CHECK CONSTRAINT [FK_RoleGrades_lkpTeacherRole]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher Rols are divided into role grades, representing different pay levels for that role. Table defines a minimum and maximum Salary Level applicable to the RoleGRade. 
Teacher Role is a foreign key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RoleGrades'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RoleGrades'
GO

