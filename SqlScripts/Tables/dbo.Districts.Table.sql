SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Districts](
	[dID] [nvarchar](5) NOT NULL,
	[dName] [nvarchar](50) NULL,
	[censusCode] [nvarchar](2) NULL,
	[examCode] [nvarchar](2) NULL,
	[oldCode] [nvarchar](2) NULL,
	[dShort] [nvarchar](5) NULL,
	[dgisID] [int] NULL,
	[dMap] [image] NULL,
	[dPayrollID] [nvarchar](20) NULL,
 CONSTRAINT [aaaaaDistricts1_PK] PRIMARY KEY NONCLUSTERED 
(
	[dID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Districts] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Districts] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Districts] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Districts] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Districts] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identifier for the District as used in Payroll system' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts', @level2type=N'COLUMN',@level2name=N'dPayrollID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'District is the principal geographic classifier of the school. 
District is determined by Island, iCode is on the School record.

District may be renamed using vocabulary, most usually to Province.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'linked table/bound' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Maintained through system Lookups' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'School Related Data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'School Classification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Districts'
GO

