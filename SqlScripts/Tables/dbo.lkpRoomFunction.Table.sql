SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpRoomFunction](
	[rfcnCode] [nvarchar](10) NOT NULL,
	[rfcnDesc] [nvarchar](50) NULL,
	[rfcnNotes] [ntext] NULL,
	[rfcnDescL1] [nvarchar](50) NULL,
	[rfcnDescL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpRoomFunction1_PK] PRIMARY KEY NONCLUSTERED 
(
	[rfcnCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpRoomFunction] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpRoomFunction] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpRoomFunction] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpRoomFunction] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpRoomFunction] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Function of rooms. These are system defined and correspond to the explicit fields on Building and BuildingReview for room counts. This mapping back to room type enabes reconciliation of the room counts on Buildings, with room counts in the survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomFunction'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpRoomFunction'
GO

