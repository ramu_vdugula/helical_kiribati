SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[EdLevelAlt2ER](
	[SurveyYear] [int] NULL,
	[edLevelCode] [nvarchar](50) NULL,
	[enrolM] [int] NULL,
	[enrolF] [int] NULL,
	[enrol] [int] NULL,
	[repM] [int] NULL,
	[repF] [int] NULL,
	[rep] [int] NULL,
	[intakeM] [int] NULL,
	[intakeF] [int] NULL,
	[intake] [int] NULL,
	[nEnrolM] [int] NULL,
	[nEnrolF] [int] NULL,
	[nEnrol] [int] NULL,
	[nRepM] [int] NULL,
	[nRepF] [int] NULL,
	[nRep] [int] NULL,
	[nIntakeM] [int] NULL,
	[nIntakeF] [int] NULL,
	[nIntake] [int] NULL,
	[popM] [int] NULL,
	[popF] [int] NULL,
	[pop] [int] NULL,
	[firstYear] [int] NULL,
	[lastYear] [int] NULL,
	[numYears] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male enrolment' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'enrolM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female enrolment' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'enrolF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total enrolment' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'enrol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male repeaters' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'repM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female repeaters' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'repF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total repeaters' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'rep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male intake into the education level (enrolment in start year - repeaters)' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'intakeM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female intake' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'intakeF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total intake' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'intake'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male "net" enrolment (enrolmenat at official age)' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nEnrolM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female net enrolment' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nEnrolF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total net enrolment' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nEnrol'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male net repeaters (repeaters of official age)' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nRepM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female net repeaters' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nRepF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total net repeaters' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nRep'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male net intake (intake of official start age)' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nIntakeM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female net intake' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nIntakeF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total net intake' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'nIntake'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'male population of official age range' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'popM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'female population of official age range' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'popF'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total population of official age range' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'pop'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'first year of education for the education level' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'firstYear'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'last year of education for the education level' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'lastYear'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'total number of years in the education level' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'EdLevelAlt2ER', @level2type=N'COLUMN',@level2name=N'numYears'
GO

