SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PupilTables](
	[ptID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[ptCode] [nvarchar](20) NULL,
	[ptAge] [int] NULL,
	[ptLevel] [nvarchar](10) NULL,
	[ptPage] [nvarchar](50) NULL,
	[ptRow] [nvarchar](50) NULL,
	[ptCol] [nvarchar](50) NULL,
	[ptM] [int] NULL,
	[ptF] [int] NULL,
	[ptSum]  AS (case when [ptM] IS NULL AND [ptF] IS NULL then NULL else isnull([ptM],(0))+isnull([ptF],(0)) end),
	[ptTableDef] [nvarchar](20) NULL,
	[ptTable]  AS (isnull([ptTableDef],[ptCode])) PERSISTED,
 CONSTRAINT [PK_PupilTables] PRIMARY KEY NONCLUSTERED 
(
	[ptID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[PupilTables] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[PupilTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[PupilTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PupilTables] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PupilTables] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_PupilTables_ssID_Code] ON [dbo].[PupilTables]
(
	[ssID] ASC,
	[ptCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PupilTables]  WITH CHECK ADD  CONSTRAINT [PupilTables_FK00] FOREIGN KEY([ptCode])
REFERENCES [dbo].[metaPupilTableDefs] ([tdefCode])
GO
ALTER TABLE [dbo].[PupilTables] CHECK CONSTRAINT [PupilTables_FK00]
GO
ALTER TABLE [dbo].[PupilTables]  WITH CHECK ADD  CONSTRAINT [PupilTables_FK01] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PupilTables] CHECK CONSTRAINT [PupilTables_FK01]
GO
ALTER TABLE [dbo].[PupilTables]  WITH CHECK ADD  CONSTRAINT [PupilTables_FK02] FOREIGN KEY([ptLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[PupilTables] CHECK CONSTRAINT [PupilTables_FK02]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'General purpose table for storing tables in metadata format. The ptCode indicates the type of data item, which, depending on the characteristics of that type, may be split by the row fieldd, column field, age and/or class level.
MetaPupilTableDefs defi9nes the cahractersitcis of each table type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PupilTables'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PupilTables'
GO

