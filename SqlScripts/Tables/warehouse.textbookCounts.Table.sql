SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[textbookCounts](
	[SchNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Resource] [nvarchar](50) NULL,
	[Number] [int] NULL,
	[Condition] [smallint] NULL,
	[ClassLevel] [nvarchar](50) NULL,
	[Estimate] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Aggregates of teatbooks by class level and school.


' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'textbookCounts'
GO

