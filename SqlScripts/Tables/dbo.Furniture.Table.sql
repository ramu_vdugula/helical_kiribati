SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Furniture](
	[fID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[fCode] [nvarchar](50) NULL,
	[fLevel] [nvarchar](10) NULL,
	[fNum] [int] NULL,
	[rmID] [int] NULL,
	[fCond] [nvarchar](1) NULL,
 CONSTRAINT [aaaaaFurniture1_PK] PRIMARY KEY NONCLUSTERED 
(
	[fID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Furniture] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Furniture] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Furniture] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Furniture] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Furniture] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_Furniture_SSID] ON [dbo].[Furniture]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Furniture] ADD  CONSTRAINT [DF__Furniture__ssID__1881A0DE]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Furniture] ADD  CONSTRAINT [DF__Furniture__fNum__1975C517]  DEFAULT ((0)) FOR [fNum]
GO
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [Furniture_FK00] FOREIGN KEY([fLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [Furniture_FK00]
GO
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [Furniture_FK01] FOREIGN KEY([fCode])
REFERENCES [dbo].[lkpFurnitureTypes] ([codeCode])
GO
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [Furniture_FK01]
GO
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [Furniture_FK02] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [Furniture_FK02]
GO
ALTER TABLE [dbo].[Furniture]  WITH CHECK ADD  CONSTRAINT [Furniture_FK03] FOREIGN KEY([rmID])
REFERENCES [dbo].[Rooms] ([rmID])
GO
ALTER TABLE [dbo].[Furniture] CHECK CONSTRAINT [Furniture_FK03]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Furniture recorded at a school. Related to SchoolSurvey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Furniture'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Resources' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Furniture'
GO

