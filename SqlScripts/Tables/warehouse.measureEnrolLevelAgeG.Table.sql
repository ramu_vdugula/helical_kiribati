SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measureEnrolLevelAgeG](
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[ClassLevel] [nvarchar](10) NULL,
	[Age] [smallint] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[Enrol] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'National level enrolment, by class level age and gender.

A small data set that can produce an enrolment grid ( by class , age, gender) at national level.

Data is disaggregated by Estimate/Actual, so percentage of estimate can be calculated from this.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'measureEnrolLevelAgeG'
GO

