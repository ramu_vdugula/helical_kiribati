SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EFTableBands](
	[efbID] [int] IDENTITY(1,1) NOT NULL,
	[efTable] [nvarchar](10) NOT NULL,
	[efbMin] [int] NOT NULL,
	[efbMax] [int] NOT NULL,
 CONSTRAINT [EFTableBands_PK] PRIMARY KEY NONCLUSTERED 
(
	[efbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EFTableBands] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[EFTableBands] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTableBands] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EFTableBands] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTableBands] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EFTableBands] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [EFTablesEFTableBands] ON [dbo].[EFTableBands]
(
	[efTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[EFTableBands]  WITH CHECK ADD  CONSTRAINT [EFTablesEFTableBands] FOREIGN KEY([efTable])
REFERENCES [dbo].[EFTables] ([efTable])
GO
ALTER TABLE [dbo].[EFTableBands] CHECK CONSTRAINT [EFTablesEFTableBands]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Bands are a partition of enrolments. The bands are specific to the particular Establishment formula Table.  Each band has a set of related Allocations in EFAllocations.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableBands'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableBands'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTableBands'
GO

