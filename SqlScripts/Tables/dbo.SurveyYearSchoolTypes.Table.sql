SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearSchoolTypes](
	[svyYear] [int] NOT NULL,
	[stCode] [nvarchar](10) NOT NULL,
	[ytForm] [nvarchar](50) NULL,
	[ytTeacherForm] [nvarchar](50) NULL,
	[ytAgeMin] [smallint] NULL,
	[ytAgeMax] [smallint] NULL,
	[ytConfig] [ntext] NULL,
	[ytLayout] [nvarchar](20) NULL,
	[ytDynamic] [int] NOT NULL,
	[ytEForm] [nvarchar](100) NULL,
 CONSTRAINT [aaaaaSurveyYearSchoolTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[svyYear] ASC,
	[stCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearSchoolTypes] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearSchoolTypes] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearSchoolTypes] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearSchoolTypes] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearSchoolTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[SurveyYearSchoolTypes] ADD  CONSTRAINT [DF_SurveyYearSchoolTypes_ytDynamic]  DEFAULT ((0)) FOR [ytDynamic]
GO
ALTER TABLE [dbo].[SurveyYearSchoolTypes]  WITH CHECK ADD  CONSTRAINT [FK_SurveyYearSchoolTypes_metaFormLayouts] FOREIGN KEY([ytLayout])
REFERENCES [dbo].[metaFormLayouts] ([formName])
GO
ALTER TABLE [dbo].[SurveyYearSchoolTypes] CHECK CONSTRAINT [FK_SurveyYearSchoolTypes_metaFormLayouts]
GO
ALTER TABLE [dbo].[SurveyYearSchoolTypes]  WITH CHECK ADD  CONSTRAINT [SurveyYearSchoolTypes_FK00] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearSchoolTypes] CHECK CONSTRAINT [SurveyYearSchoolTypes_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Minimum age for enrolment grids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearSchoolTypes', @level2type=N'COLUMN',@level2name=N'ytAgeMin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maximum age for enrolment grids' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearSchoolTypes', @level2type=N'COLUMN',@level2name=N'ytAgeMax'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Name of the EForm to use for this school type' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearSchoolTypes', @level2type=N'COLUMN',@level2name=N'ytEForm'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table that determines settings for school type in a survey, incuding:
- the survey form to use;
- the teacher form to use;
- grid age ranges.
The configuration of the survey form - hiding or showing pages - is also stored in XML on this record.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearSchoolTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearSchoolTypes'
GO

