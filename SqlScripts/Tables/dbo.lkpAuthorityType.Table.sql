SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpAuthorityType](
	[codeCode] [nvarchar](1) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeGroup] [nvarchar](1) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[atOrgUnitNumber] [int] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpAuthorityType1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpAuthorityType] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpAuthorityType] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAuthorityType] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpAuthorityType] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpAuthorityType] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpAuthorityType] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpAuthorityType]  WITH CHECK ADD  CONSTRAINT [lkpAuthorityType_FK00] FOREIGN KEY([codeGroup])
REFERENCES [dbo].[lkpAuthorityGovt] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpAuthorityType] CHECK CONSTRAINT [lkpAuthorityType_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ORganisation Unit Number in Aurion' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityType', @level2type=N'COLUMN',@level2name=N'atOrgUnitNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Classification of Authorities. Middle tier of authority reporting. Authority Gov is foreign key, while the authority type is foreign key in turn on Authorities.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityType'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Authority Hierarchy' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityType'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Authorities' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpAuthorityType'
GO

