SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaSchoolTypeLevelMap](
	[tlmID] [int] IDENTITY(1,1) NOT NULL,
	[stCode] [nvarchar](10) NULL,
	[tlmLevel] [nvarchar](10) NULL,
	[tlmOffset] [int] NULL,
 CONSTRAINT [aaaaametaSchoolTypeLevelMap1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tlmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaSchoolTypeLevelMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaSchoolTypeLevelMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeLevelMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaSchoolTypeLevelMap] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaSchoolTypeLevelMap] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaSchoolTypeLevelMap] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaSchoolTypeLevelMap] ADD  CONSTRAINT [DF__metaSchoo__tlmOf__11D4A34F]  DEFAULT ((0)) FOR [tlmOffset]
GO
ALTER TABLE [dbo].[metaSchoolTypeLevelMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeLevelMap_FK00] FOREIGN KEY([tlmLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[metaSchoolTypeLevelMap] CHECK CONSTRAINT [metaSchoolTypeLevelMap_FK00]
GO
ALTER TABLE [dbo].[metaSchoolTypeLevelMap]  WITH CHECK ADD  CONSTRAINT [metaSchoolTypeLevelMap_FK01] FOREIGN KEY([stCode])
REFERENCES [dbo].[SchoolTypes] ([stCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[metaSchoolTypeLevelMap] CHECK CONSTRAINT [metaSchoolTypeLevelMap_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Class levels associated with each school type.  A class level can , and often is, taught in more than one school; e.g. Primary/CHS Junior Sec/Combined Sec. This table therefreo has foreign keys for school type and Class Level.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeLevelMap'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaSchoolTypeLevelMap'
GO

