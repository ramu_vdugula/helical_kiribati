SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ECETransition](
	[ecetID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[ecetSeq] [int] NULL,
	[ecetSchNo] [nvarchar](50) NULL,
	[ecetM3] [int] NULL,
	[ecetF3] [int] NULL,
	[ecetM4] [int] NULL,
	[ecetF4] [int] NULL,
	[ecetM5] [int] NULL,
	[ecetF5] [int] NULL,
	[ecetM6] [int] NULL,
	[ecetF6] [int] NULL,
	[ecetM7] [int] NULL,
	[ecetF7] [int] NULL,
 CONSTRAINT [aaaaaECETransition1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ecetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ECETransition] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ECETransition] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ECETransition] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ECETransition] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ECETransition] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransit__ssID__6AEFE058]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetS__6BE40491]  DEFAULT ((0)) FOR [ecetSeq]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetM__6CD828CA]  DEFAULT ((0)) FOR [ecetM3]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetF__6DCC4D03]  DEFAULT ((0)) FOR [ecetF3]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetM__6EC0713C]  DEFAULT ((0)) FOR [ecetM4]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetF__6FB49575]  DEFAULT ((0)) FOR [ecetF4]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetM__70A8B9AE]  DEFAULT ((0)) FOR [ecetM5]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetF__719CDDE7]  DEFAULT ((0)) FOR [ecetF5]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetM__72910220]  DEFAULT ((0)) FOR [ecetM6]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetF__73852659]  DEFAULT ((0)) FOR [ecetF6]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetM__74794A92]  DEFAULT ((0)) FOR [ecetM7]
GO
ALTER TABLE [dbo].[ECETransition] ADD  CONSTRAINT [DF__ECETransi__ecetF__756D6ECB]  DEFAULT ((0)) FOR [ecetF7]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Pupil moving from ECE to primary. Part of ECE surveys.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ECETransition'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ECETransition'
GO

