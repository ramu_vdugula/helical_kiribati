SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EFTables](
	[efTable] [nvarchar](10) NOT NULL,
	[efGroup] [nvarchar](50) NULL,
 CONSTRAINT [EFTables_PK] PRIMARY KEY NONCLUSTERED 
(
	[efTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EFTables] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[EFTables] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTables] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EFTables] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[EFTables] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EFTables] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Establishment Formula Tables. Related tables are EFTableLevels - class levels in the Table, EFTableBands - partition of enrolments.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTables'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Enrolment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTables'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Establishment Formula' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EFTables'
GO

