SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpInfrastructureSize](
	[szCode] [nvarchar](10) NOT NULL,
	[szDesc] [nvarchar](50) NULL,
	[szDescL1] [nvarchar](50) NULL,
	[szDescL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaalkpInfrastructureSize1_PK] PRIMARY KEY NONCLUSTERED 
(
	[szCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpInfrastructureSize] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpInfrastructureSize] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInfrastructureSize] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpInfrastructureSize] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpInfrastructureSize] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpInfrastructureSize] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parition of enrolments for for NIS. Together with Class, determines the NIS Standards that will apply to the school' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInfrastructureSize'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpInfrastructureSize'
GO

