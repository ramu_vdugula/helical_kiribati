SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpPrimaryTransitions](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSeq] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpPrimaryTransitions1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpPrimaryTransitions] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpPrimaryTransitions] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpPrimaryTransitions] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpPrimaryTransitions] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpPrimaryTransitions] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpPrimaryTransitions] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpPrimaryTransitions] ADD  CONSTRAINT [DF__lkpPrimar__codeS__2EA5EC27]  DEFAULT ((0)) FOR [codeSeq]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Different transitions possible after primary school. Appears in PupilTables' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpPrimaryTransitions'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolments' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpPrimaryTransitions'
GO

