SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpToiletTypes](
	[ttypName] [nvarchar](30) NOT NULL,
	[ttypSystem] [bit] NULL,
	[ttypSort] [int] NULL,
	[ttypNameL1] [nvarchar](25) NULL,
	[ttypNameL2] [nvarchar](25) NULL,
	[ttypGroup] [nvarchar](25) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpToiletTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ttypName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpToiletTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpToiletTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpToiletTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpToiletTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpToiletTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpToiletTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpToiletTypes] ADD  CONSTRAINT [DF__lkpToilet__ttypS__32E0915F]  DEFAULT ((0)) FOR [ttypSystem]
GO
ALTER TABLE [dbo].[lkpToiletTypes] ADD  CONSTRAINT [DF__lkpToilet__ttypS__33D4B598]  DEFAULT ((0)) FOR [ttypSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of toilets. Foreign key on Toilets' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpToiletTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpToiletTypes'
GO

