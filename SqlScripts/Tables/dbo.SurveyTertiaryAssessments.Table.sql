SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyTertiaryAssessments](
	[staID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NOT NULL,
	[staDuration] [nvarchar](50) NULL,
	[staDescription] [nvarchar](255) NOT NULL,
	[staAssessor] [nvarchar](50) NULL,
 CONSTRAINT [SurveyTertiaryAssessments_PK] PRIMARY KEY NONCLUSTERED 
(
	[staID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyTertiaryAssessments] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyTertiaryAssessments] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyTertiaryAssessments] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyTertiaryAssessments] TO [pSchoolWrite] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [ssID] ON [dbo].[SurveyTertiaryAssessments]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyTertiaryAssessments]  WITH CHECK ADD  CONSTRAINT [FK_SurveyTertiaryAssessments_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyTertiaryAssessments] CHECK CONSTRAINT [FK_SurveyTertiaryAssessments_SchoolSurvey]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Assessments of Teriary Instituitions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyTertiaryAssessments'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyTertiaryAssessments'
GO

