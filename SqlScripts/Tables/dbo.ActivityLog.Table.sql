SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ActivityLog](
	[ActivityID] [int] IDENTITY(1,1) NOT NULL,
	[ActivityTime] [datetime] NOT NULL,
	[ActivityLogin] [nvarchar](50) NOT NULL,
	[ActivityTask] [nvarchar](100) NOT NULL,
	[ActivityReference] [int] NULL,
	[ActivityDate]  AS (dateadd(day,datediff(day,(0),[ActivityTime]),(0))) PERSISTED,
 CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED 
(
	[ActivityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ActivityLog] TO [pAdminReadX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ActivityLog] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key activities, such as log in, log out, open a survey, and save enrolments are logged to this table. This table is not attached into the User Interface.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Audit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ActivityLog'
GO

