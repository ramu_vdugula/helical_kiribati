SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TermStructure](
	[tmID] [int] IDENTITY(1,1) NOT NULL,
	[tmYear] [int] NULL,
	[tmNo] [int] NULL,
	[tmWeek] [int] NULL,
	[tmDate] [datetime] NULL,
	[tmDays] [int] NULL,
	[tmPay] [bit] NULL,
	[tmComment] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaTermStructure1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TermStructure] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TermStructure] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TermStructure] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[TermStructure] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TermStructure] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[TermStructure] ADD  CONSTRAINT [DF__TermStruc__tmYea__7D78A4E7]  DEFAULT ((0)) FOR [tmYear]
GO
ALTER TABLE [dbo].[TermStructure] ADD  CONSTRAINT [DF__TermStruct__tmNo__7E6CC920]  DEFAULT ((0)) FOR [tmNo]
GO
ALTER TABLE [dbo].[TermStructure] ADD  CONSTRAINT [DF__TermStruc__tmWee__7F60ED59]  DEFAULT ((0)) FOR [tmWeek]
GO
ALTER TABLE [dbo].[TermStructure] ADD  CONSTRAINT [DF__TermStruc__tmDay__00551192]  DEFAULT ((0)) FOR [tmDays]
GO
ALTER TABLE [dbo].[TermStructure] ADD  CONSTRAINT [DF__TermStruc__tmPay__014935CB]  DEFAULT ((0)) FOR [tmPay]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The structure of the school year. One record for each week of term, showing the number of school days in that week. TeacherAttendance has foreign key in tmID.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TermStructure'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Bound form, linked table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TermStructure'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'Set up Term structure using frmTermStructureEntry. Access in UI from Term Dates button on Teacher Attendence Entry (frmTeacherAttendance)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TermStructure'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers (Attendance)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TermStructure'
GO

