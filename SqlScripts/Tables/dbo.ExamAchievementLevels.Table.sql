SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamAchievementLevels](
	[exID] [int] NOT NULL,
	[exalVal] [int] NOT NULL,
	[exalDescription] [nvarchar](200) NOT NULL
) ON [PRIMARY]
GO

