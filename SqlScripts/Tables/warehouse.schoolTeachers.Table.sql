SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[schoolTeachers](
	[SurveyYear] [int] NOT NULL,
	[SchNo] [nvarchar](50) NULL,
	[Role] [nvarchar](50) NULL,
	[GenderCode] [nvarchar](1) NULL,
	[NumTeachers] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Survey year' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers', @level2type=N'COLUMN',@level2name=N'SurveyYear'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'School Code' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers', @level2type=N'COLUMN',@level2name=N'SchNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Role of teacher' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers', @level2type=N'COLUMN',@level2name=N'Role'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Gender code' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers', @level2type=N'COLUMN',@level2name=N'GenderCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of teachers in this group' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers', @level2type=N'COLUMN',@level2name=N'NumTeachers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'High level totals of teachers by school.

this is provided where this total teacher number is required alongside other contexts, and for auditing.

For reporting on teachers, use warehouse.SchoolTeacherCount and its derived views.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeachers'
GO

