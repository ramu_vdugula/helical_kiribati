SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pTeacherRead].[TeacherAppointmentList]
	-- Add the parameters for the stored procedure here
	@Snapshot int = 0,	-- 0 means no snapshot 1 = snapshot at current date,2 = snapshot at specified date 3 = last appointment ( even if future)
	@SchoolNo nvarchar(50) ,
	@TeacherID int,
	@StartDate datetime = null,	-- minimum ofr appt date
	@EndDate datetime = null,		-- maximum for appt date
	@SnapshotDate datetime = null,	-- snapshot date if @snapshot = 2
	@ApptType nvarchar(20) = null,
	@ApptRole nvarchar(20) = null,	-- appointment role
	@SalaryLevel nvarchar(10) = null -- salary level code

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if @Snapshot = 0
	begin
	-- return all the teacher appointments that match the specifed criteria
		Select App.*
		, Schools.schName
		, Schools.schAuth
		, R.codeDescription RoleDesc
		, TI.tPayroll
		, TI.tNamePrefix
		, TI.tGiven
		, TI.tMiddleNames
		, TI.tSurname
		, TI.tNameSuffix
		, TI.tFullName
		From
		(Select
			TA.taID
			, TA.tID
			, TA.taType
			, TA.taDate
			, TA.taEndDate
			, TA.estpNo
			, TA.spCode
			, E.estpTitle
			, isnull(TA.SchNo,E.SchNo) schNo
			, isnull(TA.tarole,RG.roleCode) taRole
			, RG.rgCode
		from TeacherAppointment TA
			LEFT JOIN Establishment E ON E.estpNo = TA.estpNo
			LEFT JOIN RoleGrades RG on E.estpRoleGRade = RG.rgCode
		) App
		INNER JOIN Teacheridentity TI on TI.tID = App.tID
		LEFT JOIN Schools ON Schools.schNo = App.SchNo
		LEFT JOIN TRTeacherRole R ON R.codeCode = App.taRole
		WHERE
			 (App.schNo = @SchoolNo or @SchoolNo is null)
			AND (APP.tID = @TeacherID or @teacherID is null)

		return
	end

if @Snapshot = 1
	select @Snapshotdate = getdate()
if @Snapshot = 3
	select @snapshotdate = null

-- find the most recent appointment for each teacher prior to or on the snapshot date
-- of these filter only those that meet the remaining criteria

    -- Insert statements for procedure here
	-- @Snapshot, @SchoolNo


	Select App.*
		, Schools.schName
		, Schools.schAuth
		, R.codeDescription RoleDesc
		, TI.tPayroll
		, TI.tNamePrefix
		, TI.tGiven
		, TI.tMiddleNames
		, TI.tSurname
		, TI.tNameSuffix
		, TI.tFullName
	from
	(
		Select
			TA.taID
			, TA.tID
			, TA.taType
			, TA.taDate
			, TA.taEndDate
			, TA.estpNo
			, TA.spCode
			, isnull(TA.SchNo,E.SchNo) schNo
			, isnull(TA.tarole,RG.roleCode) taRole
			, estpTitle
			, RG.rgCode
			, row_number() OVER(PARTITION by tID ORDER BY taDate DESC) N
		from TeacherAppointment TA
			LEFT JOIN Establishment E ON E.estpNo = TA.estpNo
			LEFT JOIN RoleGrades RG on E.estpRoleGRade = RG.rgCode
		WHERE taDate <= @SnapshotDate
	) App
	INNER JOIN Teacheridentity TI on TI.tID = App.tID
	LEFT JOIN Schools ON Schools.schNo = App.SchNo
	LEFT JOIN TRTeacherRole R ON R.codeCode = App.taRole
		WHERE
			App.N =1
			AND (App.schNo = @SchoolNo or @SchoolNo is null)
			AND (APP.tID = @TeacherID or @teacherID is null)


END
GO

