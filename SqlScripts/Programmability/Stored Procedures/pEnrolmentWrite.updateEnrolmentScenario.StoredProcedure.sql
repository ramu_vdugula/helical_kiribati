SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pEnrolmentWrite].[updateEnrolmentScenario]
	-- Add the parameters for the stored procedure here
	@grid xml
	, @ReplaceAll int = 1			-- if this parameter is 1, the whole scenario is replaced
	, @PreserveLocks int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values
/* these attributes on the enrolment Scenario
	ID
	name
	description
	parameters
	source
	split
	genderSplit
	levelSplit
	ageSplit

	yearStart
	yearEnd
*/
declare @ID int
-- temp table holds the school/year projections
declare @Projections table
(
	ProjYEar int
	, District nvarchar(10)
	, SchoolNo nvarchar(50)
	, Locked bit
)

-- first , set the default for ahdling the locks

if (@PReservelocks = -1)
	-- user did not supply, so preserve if loading all, overwrite if loading partial
	Select @PreserveLocks = @ReplaceAll


Select @id = ID   from OPENXML (@idoc, '/EnrolmentScenario',2)
		WITH (
				id int '@ID'
				, [name] nvarchar(50)
				, [description] text '@description'
				, parameters text '@parameters'
				, source text '@source'
				, split nvarchar(1) '@split'
				, genderSplit nvarchar(1) '@genderSplit'
				, levelSplit  nvarchar(1) '@levelSplit'
				, ageSplit  nvarchar(1) '@ageSplit'
				, yearStart int '@yearStart'
				, yearEnd int '@yearEnd'
				, definition xml 'Definition'

			)

-- get all the projection headers
INSERT INTO @projections
SELECT
	X.ProjYear
	, X.District
	, X.SchoolNo
	, isnull(X.Locked,0)
 from OPENXML (@idoc, '/EnrolmentScenario/Projections/Projection',2)
		WITH (
				ProjYear int '@projectionYear'
				, District nvarchar(10) '@district'
				, SchoolNo nvarchar(50) '@schoolNo'
				, Locked bit '@locked'
			) X


begin try
	begin transaction
	if  not (@ID is null)

		-- if id is not null, replace that projection
		-- update any other values that are supplied here
	BEGIN
			print 'not null'
			print @ID
			UPDATE EnrolmentScenario
			SET escnName = isnull([name], escnName)
			, escnDescription = isnull([description], escnDEscription)
			, escnParams = isnull([definition], escnParams) -- don;t update the params if not supplied
			,	escnGenderSplit =isnull(genderSplit, escnGenderSplit)
			,	escnLevelSplit = isnull(levelSplit, escnLevelSplit)
			,	escnAgeSplit =isnull(ageSplit, escnAgeSplit)
			,	escnSource = isnull([source],escnSource)
			, escnYearStart = isnull(yearStart,escnYearStart)
			, escnYearEnd = isnull(yearEnd,escnYearEnd)
			from EnrolmentScenario ES
			INNER JOIN
			(Select *  from OPENXML (@idoc, '/EnrolmentScenario',2)
				WITH (
						id int '@ID'
						, [name] nvarchar(50) '@name'
						, [description] text '@description'
						, definition xml 'Definition'
						, source text '@source'
						, split nvarchar(1) '@split'
						, genderSplit nvarchar(1) '@genderSplit'
						, levelSplit  nvarchar(1) '@levelSplit'
						, ageSplit  nvarchar(1) '@ageSplit'
						, yearStart int '@yearStart'
						, yearEnd int '@yearEnd'

					)
				) theXML on ES.escnID = theXML.id
			if @@Rowcount = 0
				select @ID = null

			-- this logic is designed to allow updating of one school/year at a time
			if (@ReplaceAll = 1)
				DELETE from EnrolmentProjection
				WHERE escnID = @ID
					and (epLocked = 0	or @PreserveLocks = 0)		-- preserve any that are locked

			else begin
				-- just delete any school/year combination in the input

					DELETE from EnrolmentProjection
					FROM
						EnrolmentProjection
						INNER JOIN
						 @Projections X
						ON EnrolmentProjection.escnID = @ID
							AND EnrolmentProjection.epYear = X.ProjYear
							AND isnull(EnrolmentProjection.dID,'') = isnull(X.District,'')
							AND isnull(EnrolmentProjection.schNo,'') = isnull(X.SchoolNo,'')

						WHERE
							(epLocked = 0	or @PreserveLocks = 0)
					print 'projection delete'
					print @@rowcount

			end
	end

	IF (@ID is null)
		BEGIN
		-- we have to add a new scenario record
			INSERT INTO EnrolmentScenario
			(
				escnName
			,	escnDescription
			,	escnParams
			,	escnSplit
			,	escnGenderSplit
			,	escnLevelSplit
			,	escnAgeSplit
			,	escnSource
			,   escnYearStart
			,	escnYearEnd
			)
			Select * from OPENXML (@idoc, '/EnrolmentScenario',2)
				WITH (
						[name] nvarchar(50) '@name'
						, [description] text '@description'
						, definition xml 'Definition'
						, split nvarchar(1) '@split'
						, genderSplit nvarchar(1) '@genderSplit'
						, levelSplit  nvarchar(1) '@levelSplit'
						, ageSplit  nvarchar(1) '@ageSplit'
						, source text '@source'
						, yearStart int '@yearStart'
						, yearEnd int '@yearEnd'
					)
			Select @ID = @@IDENTITY

		END

-- insert the projection headers
		INSERT INTO enrolmentProjection(escnID, epYear, dID, schNo, epLocked)
		Select @ID
			, X.*
		FROM @Projections X
			LEFT JOIN EnrolmentProjection
			ON EnrolmentProjection.escnID = @ID
				AND EnrolmentProjection.epYear = X.ProjYear
				AND isnull(EnrolmentProjection.dID,'') = isnull(X.District,'')
				AND isnull(EnrolmentProjection.schNo,'') = isnull(X.SchoolNo,'')

		WHERE
			epId is null -- don;t try to insert anything not deleted (ie locked)


		INSERT INTO enrolmentProjectionData(epID, epdAge, epdLevel, epdM, epdF, epdU)
		Select  EP.epID
		, X.Age, X.classLevel, X.M, X.F ,X.U from
		EnrolmentProjection EP,
		OPENXML (@idoc, '/EnrolmentScenario/Projections/Projection/row/col/data',2)
		WITH (
				M int '@M'
				, F int '@F'
				, U int '@U'
				, District nvarchar(10) '../../../@district'
				, SchoolNo nvarchar(50) '../../../@schoolNo'
				, ProjYear int '../../../@projectionYear'
				, Age int '../../@age'
				, ClassLevel nvarchar(10) '../@classLevel'
			) X
		WHERE EP.escnID = @ID
			and (isnull(EP.dID,'') = isnull(X.District ,''))
			and (isnull(EP.schNo,'') = isnull(X.SchoolNo ,''))
			and EP.epYear = X.ProjYear
end try
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

-- return the recordset so we can get the Id if necessary
Select * from EnrolmentScenario WHERE escnID = @ID
return


END
GO

