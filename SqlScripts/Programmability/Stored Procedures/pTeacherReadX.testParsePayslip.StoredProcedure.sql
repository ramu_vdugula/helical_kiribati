SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pTeacherReadX].[testParsePayslip]
(
	-- Add the parameters for the function here
	@Payslip nvarchar(max)
)

AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @xmlPS xml


	declare @rows TABLE
	(
		ID int IDENTITY
		, rowData nvarchar(200)
	)


declare @layout TABLE
	(
		FieldName sysname
		, rowTag nvarchar(50)
		, startChar int
		, lenField int
		, txtValue nvarchar(255)
		, sorter int
	)

INSERT INTO @layout
	VALUES ('tpsPeriodStart'
			,'| Pay Period: %'
			,15
			, 11
			, null
			, 0
			)


INSERT INTO @layout
	VALUES ('tpsPeriodEnd'
			,'| Pay Period: %'
			,30
			, 11
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsPayDate'
			,'| Pay Period: %'
			,65
			, 11
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsGross'
			,'|  Gross %'
			,25
			, 13
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsTitle'
			,'|  Job Title: %'
			,15
			, 30
			, null
			, 0
			)


INSERT INTO @layout
	VALUES ('fullName'
			,'|       Name: %'
			,15
			, 50
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsSalaryPoint'
			,'|  Job Title: %'
			,51
			, 9
			, null
			, 0
			)


INSERT INTO @layout
	VALUES ('tpsPosition'
			,'|  Job Title: %'
			,64
			, 15
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsPayroll'
			,'|Employee No: %'
			,15
			, 30
			, null
			, 0
			)

INSERT INTO @layout
	VALUES ('tpsPayPtCode'
			,'| Pay Point: %'
			,15
			, 3
			, null
			, 0
			)


INSERT INTO @layout
	VALUES ('tpsPayPtDesc'
			,'| Pay Point: %'
			,19
			, 30
			, null
			, 0
			)

	Select @XMLPS = '<ps><row>' + replace(replace(@Payslip,'&','_'), char(13) + char(10), '</row><row>') + '</row></ps>'


	INSERT INTO @Rows
	(rowData)
	Select cast( r.query('./text()') as nvarchar(400))
	from
	@xmlps.nodes('/ps/row') as F(r)


	declare @ThisPayStart int
	declare @ThisPayEnd int

-- all the pay slip data
	declare @PeriodStart datetime
	declare @PeriodEnd datetime

	declare @PayrollNo nvarchar(20)
	declare @PositionNo nvarchar(20)

	declare @Tag nvarchar(100)
	declare @Pos bigint
	Select @Tag = ' This Pay                      Multiplier      Rate  Hours/Units       Amount'

	Select @ThisPayStart = ID+2
	from @rows
	WHERE rowData like @Tag

	Select @Tag = ' ----%'

	Select top 1 @ThisPayEnd = ID - 3
	from @rows
	WHERE rowData like @Tag
	AND ID > @ThisPayStart


		Select @xmlPS =
		(
		Select  rtrim(substring(rowData, 4,12)) [@pc]
		, cast( substring(rowData, 70,10) as money) [@amt]

		from @rows
		WHERE ID between @ThisPayStart and @ThisPayEnd
		FOR XML PATH('Item'), root( 'ThisPay')
		)

UPDATE @layout
SET txtValue = substring(rowData,startChar,lenField)
FROM @Layout L
INNer JOIN @rows R
	ON R.rowData like L.rowTag


select * from @layout
-- get the period to and from -

Select L.*
, N.*
from
(
Select
	max(cast((case FieldName when 'tpsPeriodStart' then txtValue end) as datetime)) tpsPeriodStart
	, max(cast((case FieldName when 'tpsPeriodEnd' then txtValue end) as datetime)) tpsPeriodEnd
	, max(cast((case FieldName when 'tpsPayDate' then txtValue end) as datetime)) tpsPayDate
	, max(cast((case FieldName when 'tpsGross' then txtValue end) as money)) tpsGross

	, max(case FieldName when 'tpsTitle' then txtValue end) tpsTitle
	, max(case FieldName when 'tpsPosition' then txtValue end) tpsPosition
	, max(case FieldName when 'tpsPayroll' then txtValue end) tpsPayroll

	, max(case FieldName when 'tpsPayPtCode' then txtValue end) tpsPayPtCode
	, max(case FieldName when 'tpsPayPtDesc' then txtValue end) tpsPayPtDesc

	, max(case FieldName when 'tpsSalaryPoint' then txtValue end) tpsSalaryPoint

	, max(case FieldName when 'fullName' then txtValue end) fullName

from @Layout
) L
CROSS APPLY dbo.fnParseName(fullName) N


declare @xmlPos xml

		Select @xmlPos =
		(
		Select rtrim(substring(rowData,15,30)) [@title]
		from @rows
		WHERE rowData like '%Job Title%'
		FOR XML PATH('Position')
		)


		Select @xmlPS =
		(
		Select @XMLPS
		, @xmlPos
		FOR XML PATH('Payslip')
		)

END
GO

