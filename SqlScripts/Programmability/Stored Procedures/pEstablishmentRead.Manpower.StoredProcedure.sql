SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2009
-- Description:	Manpower report of appointments against Positions
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[Manpower]
	-- Add the parameters for the stored procedure here
	  @asAtDate datetime = null
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(10) = null
	, @RoleGrade nvarchar(10) = null
	, @Role nvarchar(10) = null
	, @Sector nvarchar(10) = null
	, @Unfilled bit = 0
	, @PayslipExceptions bit = 0
	, @SurveyExceptions bit = 0
	, @SalaryPointExceptions bit = 0
	, @PositionFlag nvarchar(10) = null
	, @PositionFlagOp bit = 1


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if (@AsAtDate is null)
	Select @AsAtDate = common.Today()
-- Find the last census date before the AsAtDate

CREATE TABLE #wf		-- for workforce
(
	estpNo nvarchar(20)
	, schNo nvarchar(50) NULL
	, schAuth nvarchar(10) NULL
	, SchName nvarchar(50) NULL
	, schType nvarchar(10) NULL
	, RoleGrade nvarchar(10) NULL
	, secCode nvarchar(10) NULL
	, ApptID int NULL
	, tID int NULL
	, PayslipConfirm int NULL
	, LastTeacherInfo int NULL
	, tpsID int
	, tpsPayPoint nvarchar(10) NULL
	, tpsSalaryPoint nvarchar(50) NULL
	, tpsGross money
	, surveySchNo nvarchar(50) NULL
	, surveyRole nvarchar(10) NULL
	, SurveyException int null
	, SalaryPointConfirm int NULL
	, Underpay int NULL
	, Overpay int NULL
	, ApptAtSurveyDate int NULL
	, NumExtensions INT
	, IsExtension int
	, PayAllowanceList nvarchar(400)
)

declare @LastSurvey int
declare @LastSurveyDate datetime

Select @LastSurvey = max(svyYear)
		, @LastSurveyDate = max(svyCensusDate)
from Survey
WHERE svyCensusDate <= @AsAtDate

-- 21 4 2010 we can optimise by finding the dates of the payrun up front.

declare @PayPeriodStart datetime
Select @PayPeriodStart = max(tpsPeriodStart)
from TeacherPayslips
WHERE tpsPeriodStart <= @AsAtDate

declare @AsAtYear int
select @AsAtYear = year(@AsAtDate)


-- the strategy here is to make the temporary table in order to expedite creating the totals
-- as a second recordset
-- if a table is required for filtering, then the fields from that table
-- are written into the temp table
-- That table does not need to be read again in the output query (the values are surced from #wf)
-- If a table is not required for filtering, it is omitted in the construction of the temp
-- table, and joined back in in the construction of the output.


INSERT INTO #wf
Select E.estpNo
	, E.schNo
	, case estpScope
			when 'A' then estpAuth
			else Schools.schAuth
		end schAuth
	, Schools.SchName
	, Schools.schType
	, E.RoleGrade			-- keep this cos it comes from the appointment if supernu
	, Roles.secCode
	, E.taID		-- use from E , removed left join to TEacherAppointments
	, E.tID

	, case when Slips.tpsPosition = E.estpNo then 1
			when Slips.tpsPayroll is null then null		-- use tpsPayroll instead of tpsID becuase it is mandatory, and becuase it ptimises index use
			else 0	-- there is a payslip but it don;t match
		end PayslipConfirm
	, LastTeacherInfo
	, slips.tpsID
	, slips.tpsPayPoint
	, slips.tpsSalaryPoint
	, slips.tpsGross
	, TS.schNo surveySchNo
	, TS.tchRole surveyRole
	, case when TS.schNo <> E.schNo then 1
			when
					-- or the teacher has not appeared anywhere, but the school survey is in
					 (TS.tID is null and lastTeacherInfo = @LastSurvey and TI.tID is not null) then 1
			else 0
	  end SurveyException
	, case when SalPt.salLevel  between RoleGrades.rgSalaryLevelMin and RoleGRades.rgSalaryLevelMax then 1
		end SalaryPointConfirm
	, case when SalPt.salLevel < RoleGrades.rgSalaryLevelMin then 1
			else 0
		end UnderPay
	, case when SalPt.salLevel > RoleGrades.rgSalaryLevelMax then 1
			else 0
		end OverPay
	, case when ( @LastSurveyDate between E.taDate and isnull(E.taEndDate,'2999-12-31')) then 1
			else 0
		end ApptAtSurveyDate
	, Exts.NumExtensions
	, Schools.IsExtension
	, pSchoolRead.fnSchoolPayAllowances(E.schNo, @AsAtYear)

from
	---Establishment E
	-- 29 04 2010
	-- supernumeraries - positions of Authority Scope add the indirection
	-- that we only know the rolegrade and the school from the appointment
	-- so we'll do a little preprocessing to get these core fields related to the position on the
	-- AsAtDate so ths subquery is now E
	(
		Select E.estpNo
		, E.estpScope
		, case estpScope
				when 'A' then TA.schNo else E.schNo end schNo
		, case estpScope
				when 'A' then isnull(TA.taRoleGrade,E.estpRoleGrade)	-- allow for a role on the A position
						 else E.estpRoleGrade
						 end RoleGrade
		, estpAuth
		, estpActiveDate
		, estpClosedDate
		, estpFlag
		, taDate
		, taEndDate
		, taID
		, tID

		from Establishment E
			LEFT JOIN TeacherAppointment TA
				ON E.estpNo = TA.estpNo
				AND TA.taDate <= @AsAtDate
				AND (TA.taendDate >= @AsATDate or TA.taEndDate is null)
	) E
	LEFT JOIN	-- becuase Authority scope has no school
		-- does it make a slight optimisation pushing these criteria to the top,
		-- instead of at the end?
		(Select Schools.schNo, schName, schType, schAuth
				, case when schParent is null then 0 else 1 end IsExtension
				, max(svyYear) lastTeacherInfo
			from Schools
				LEFT JOIN
					(Select schNo, svyYear
							from SchoolSurvey
								INNER JOIN TeacherSurvey
									ON SchoolSurvey.ssID = TeacherSurvey.ssID
					) u1
					ON Schools.schNo = u1.schNo
			GROUP BY Schools.schNo, schName, schType, schAuth, schParent

		) Schools
		ON E.schNo = Schools.schNo	-- so we are picking up the correct school for the supernumerary
	LEFT JOIN RoleGrades -- becuase authority scope has no rolegrade
		ON RoleGrades.rgCode = E.RoleGrade
	LEFT JOIN lkpTeacherRole Roles
		ON RoleGRades.roleCode = Roles.codeCode
	LEFT JOIN
		(Select schParent, count(schNo) NumExtensions
		 FROM Schools
		 GROUP BY schParent
		) Exts
		ON Exts.schParent = E.schNo
	LEFT JOIN TEacherIdentity TI
		ON TI.tID = E.tID
	LEFT JOIN
			( Select *
				from TeacherPaySlips
				WHERE tpsPeriodStart = @PayPeriodStart
			) Slips
		   ON Slips.tpsPayroll = TI.tPayroll
		-- ON Slips.tID = TI.tID

	LEFT JOIN (Select tID, SchoolSurvey.SchNo , SchoolSurvey.svyYear, TeacherSurvey.tchRole
				-- 12 05 2010 subtle bug here if there is a duplicate survey for the teacher in the year
				-- the subquery returned multiple rows distorting the position count ROW = 1 eliminates that
				, row_number() OVER (PARTITION BY tID, svyYear ORDER BY tchsID) ROW
					from TeacherSurvey RIGHT JOIN SchoolSurvey
						ON TeacherSurvey.ssID = SchoolSurvey.ssID
				WHERE SvyYear = @LastSurvey) TS
		ON TS.tID = TI.tID
		AND TS.ROW = 1
	----LEFT JOIN (Select schNo, max(svyYear) lastTeacherInfo
	----				from TeacherSurvey INNER JOIN SchoolSurvey
	----					ON TeacherSurvey.ssID = SchoolSurvey.ssID
	----			GROUP BY schNo ) LastTeacherInfoYear

	----	ON LastTeacherInfoYear.schNo = E.schNo

	LEFT JOIN lkpSalaryPoints SalPt
		ON SalPt.spCode = slips.tpsSalaryPoint

	WHERE
		(E.schNo = @SchoolNo or @SchoolNo is null)
		AND (
			(@Authority is null)
			or (SchAuth = @Authority and estpScope is null)
			or (estpAuth = @Authority and estpScope = 'A')
		)
		AND (schType = @SchoolType or @SchoolType is null)

		-- condiotions on authority and school type moved into the FROM
		AND (E.RoleGrade = @RoleGrade OR @roleGrade is null)
		AND (RoleGrades.roleCode = @Role or @Role is null)
		AND (Roles.secCode = @Sector or @Sector is null)
		AND (E.estpActiveDate <= @asAtDate)
		AND (E.estpClosedDate >= @AsAtDate or E.estpClosedDate is null)
		AND ((@PayslipExceptions = 1
				AND
				(( Slips.tpsPosition <> E.estpNo)
				OR (Slips.tpsID is null and TI.tID is not null))		-- there is no payslip....
			  )
			 OR
				(@Unfilled = 1 AND E.tID is null)
			 OR
				(@Unfilled = 0 and 	@PayslipExceptions = 0)
			)
		AND (@SurveyExceptions = 0
				-- a survey exception is when
				-- the teacher has appeared some place else
				OR (@LastSurveyDate between E.taDate and isnull(E.taEndDate,'2999-12-31')
					AND
						(
						TS.schNo <> E.schNo
						-- or the teacher has not appeared anywhere, but the school survey is in
						OR (TS.tID is null and lastTeacherInfo = @LastSurvey and TI.tID is not null)
						)
					)
			)
		AND ( @SalaryPointExceptions = 0
				-- salary point is outside the range for the position
				OR SalPt.salLevel  < RoleGrades.rgSalaryLevelMin
				OR SalPt.salLevel > RoleGRades.rgSalaryLevelMax
			)
		AND ( @PositionFlag is null
				OR (@positionFlagOp = 1 and E.estpFlag = @PositionFlag)
				OR (@positionFlagOp = 0 and ( E.estpFlag <> @PositionFlag or E.estpFlag is null))
			)


---- Main output query -------
Select
	  #wf.[estpNo]
     ,  #wf.schNo
	, #wf.RoleGrade estpRoleGrade
	, #wf.schAuth
	, #wf.SchName
	, #wf.schType
	 , E.estpScope
     , E.[estpActiveDate]
      ,E.[estpClosedDate]
      ,E.[estpClosedReason]
      ,E.[estpTitle]
      ,[estpDescription]
      ,E.[estpSubject]
      ,E.[estpFlag]
	  , case when E.estpNote is null then 0 else 1 end PosHasNote

      , Auth.AuthName
	, STyp.stDescription
	, STyp.stSort
	, TI.tID
	, TI.tNAmePrefix
	, TI.tGiven
	, TI.tMiddleNames
	, TI.tSurname
	, TI.tNameSuffix
	, TI.tShortName
	, TI.tFullName
	, TI.tLongName
	, TI.tPayroll
	, TI.tDOB
	, TI.tSex
	, #wf.ApptID
	, Appts.spCode
	, Appts.taDate
	, Appts.taType
	, Appts.taFlag
	, case when taNote is null then 0 else 1 end ApptHasNote
	, RoleGrades.RoleCode
	, RoleGrades.rgSalaryLevelMin
	, RoleGrades.rgSalaryLevelMax
	, RoleGrades.rgSalaryLevelMin + '-' + RoleGrades.rgSalaryLevelMax RoleSalaryLevelRange
	, RoleGrades.rgSort
	, #wf.secCode
	, SEC.secSort
	, #wf.tpsPayPoint
	, #wf.tpsSalaryPoint
	, PPC.payptDesc
	, #wf.surveySchNo
	, #wf.surveyRole
	, #wf.lastTeacherInfo
	, @LastSurvey	lastSurveyYear
	, case when lastTeacherInfo is null then null
			when lastTeacherInfo = @LastSurvey then 1
			else 0
		end TeacherInfoReturned
	, #wf.PayslipConfirm
	, #wf.SalaryPointConfirm
	, #wf.UnderPay
	, #wf.OverPay
	, #wf.ApptAtSurveyDate
	, #wf.SurveyException
	, #wf.NumExtensions
	, #wf.IsExtension
	, case when #wf.IsExtension = 1 then 'x'
		when #wf.NumExtensions > 0 then 'P'
		else null end ExtensionFlag
	, #wf.PayAllowanceList
	, row_number() OVER (ORDER BY E.schNo, secSort, rgSort, rgSalaryLevelMax DESC, rgSalaryLevelMin DESC, E.estpNo) StdReportingSort
	from #wf
	INNER JOIN 	Establishment E
		ON #wf.estpNo = E.estpNo

	LEFT JOIN RoleGrades ON RoleGrades.rgCode = #wf.RoleGrade
	--INNER JOIN lkpTeacherRole Roles ON RoleGRades.roleCode = Roles.codeCode
	LEFT JOIN EducationSectors SEC ON Sec.secCode = #wf.secCode
	LEFT JOIN TRAuthorities Auth ON Auth.authCode = #wf.schAuth
	LEFT JOIN TRSchoolTypes STyp ON #wf.schType = Styp.stCode
	LEFT JOIN TeacherAppointment Appts
		ON Appts.taID = #wf.ApptID
	LEFT JOIN TEacherIdentity TI
		ON TI.tID = #wf.tID

	----LEFT JOIN lkpSalaryPoints SalPt
	----	ON SalPt.spCode = #wf.tpsSalaryPoint
	LEFT JOIN TRPayPointCodes PPC
		ON #wf.tpsPayPoint = PPC.payptCode
ORDER BY #wf.schNo, #wf.SchAuth, secSort, rgSort, rgSalaryLevelMax DESC, rgSalaryLevelMin DESC, E.estpNo

--summary
Select
	count(estpNo) NumPositions
	, count(DISTINCT schNo) NumSchools
	, count(DISTINCT case when tID is null then schNo else null end) NumSchoolsUnfilled
	, count(tID) NumAppts
	, sum(PayslipConfirm) NumPayslipConfirm
	, sum(SurveyException) NumSurveyException
	, sum(SalaryPointConfirm) NumSalaryPointConfirm
	, sum(Underpay) NumUnderpay
	, sum(Overpay) NumOverpay
	, sum(ApptAtSurveyDate) NumApptAtSurveyDate
	, sum(tpsGross) TotalPayslipGross
	, count(tpsID) NumPayslips

from #wf

DROP TABLE #wf

-- finally the payrun summary
if (common.fnIs_MemberEx('pTeacherReadX') = 1) begin
	exec pTeacherReadX.PayrunSummary @AsAtDate
end

END
GO

