SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Underlying data for calculation of efa12
-- =============================================
CREATE PROCEDURE [dbo].[sp_EFA12Data]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table
exec sp_efacore 1
return


--- UNUSED


Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

-- current years enrolments by level and gender


-----------------------------------------------------------
-- Enrolments
-- 3 groups- This Year, NextYear, Next Year Next Level
-----------------------------------------------------------

-- enrolments this year
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	EL.LevelCode,
	EL.EnrolM,
	EL.enrolF,
	0 AS EnrolNYM,
	0 AS EnrolNYF,
	0 AS EnrolNYNextLevelM,
	0 AS EnrolNYNextLevelF,
	0 as RepM,
	0 as RepF,
	0 AS RepNYM,
	0 AS RepNYF,
	0 AS RepNYNextLevelM,
	0 AS RepNYNextLevelF,
	1 as Rectype
INTO #data
FROM #ebse EBSE
inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID

-- next year's enrolment at the same level
INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	EL.LevelCode,
	0 as EnrolM,
	0 as enrolF,
	EL.enrolM AS EnrolNYM,
	EL.EnrolF AS EnrolNYF,
	0 AS EnrolNYNextLevelM,
	0 AS EnrolNYNextLevelF,
	0 as RepM,
	0 as RepF,
	0 AS RepNYM,
	0 AS RepNYF,
	0 AS RepNYNextLevelM,
	0 AS RepNYNextLevelF,
	1 as Rectype
FROM #ebse EBSE
inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID


-- enrolments next year next level
INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	L2.codeCode as LevelCode,
	0 as EnrolM,
	0 as enrolF,
	0 AS EnrolNYM,
	0 AS EnrolNYF,
	EL.enrolM AS EnrolNYNextLevelM,
	EL.EnrolF AS EnrolNYNextLevelF,
	0 as RepM,
	0 as RepF,
	0 AS RepNYM,
	0 AS RepNYF,
	0 AS RepNYNextLevelM,
	0 AS RepNYNextLevelF,
	1 as Rectype
FROM #ebse EBSE
	inner join pEnrolmentRead.ssidEnrolmentLevel EL
		on EBSE.bestssID = El.ssID
	inner join lkpLevels L1
		on L1.codeCode = EL.LevelCode,
-- find the 'next level' based on lvlYear, the year of education
-- default path levels is a way to get a unique level
-- needs to be considered how this would work in a school type not using the default path level levels

	LISTDefaultPathLevels LDPL
		inner join lkpLevels L2
		on L2.codeCode = LDPL.levelCode
WHERE L2.lvlYear = L1.lvlYear-1

-----------------------------------------------
-- Repeaters
-- this year, next year, next year next level
-----------------------------------------------

INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	RL.ptLevel,
	0 as EnrolM,
	0 as enrolF,
	0 AS EnrolNYM,
	0 AS EnrolNYF,
	0 AS EnrolNYNextLevelM,
	0 AS EnrolNYNextLevelF,
	RL.repM as RepM,
	RL.repF as RepF,
	0 AS RepNYM,
	0 AS RepNYF,
	0 AS RepNYNextLevelM,
	0 AS RepNYNextLevelF,
	3 as Rectype
FROM #ebse EBSE
inner join ssidRepeatersLevel RL
on EBSE.bestssID = RL.ssID

INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	RL.ptLevel,
	0 as EnrolM,
	0 as enrolF,
	0 AS EnrolNYM,
	0 AS EnrolNYF,
	0 AS EnrolNYNextLevelM,
	0 AS EnrolNYNextLevelF,
	0 as RepM,
	0 as RepF,
	RL.repM AS RepNYM,
	RL.repF AS RepNYF,
	0 AS RepNYNextLevelM,
	0 AS RepNYNextLevelF,
	3 as Rectype
FROM #ebse EBSE
inner join ssidRepeatersLevel RL
on EBSE.bestssID = RL.ssID

-- Repeaters next year next level
INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	L2.codeCode as LevelCode,
	0 as EnrolM,
	0 as enrolF,
	0 AS EnrolNYM,
	0 AS EnrolNYF,
	0 AS EnrolNYNextLevelM,
	0 AS EnrolNYNextLevelF,
	0 as RepM,
	0 as RepF,
	0 AS RepNYM,
	0 AS RepNYF,
	RepM AS RepNYNextLevelM,
	RepF AS RepNYNextLevelF,
	1 as Rectype
FROM #ebse EBSE
	inner join ssidRepeatersLevel RL
		on EBSE.bestssID = RL.ssID
	inner join lkpLevels L1
		on L1.codeCode = RL.ptLevel,
-- find the 'next level' based on lvlYear, the year of education
-- default path levels is a way to get a unique level
-- needs to be considered how this would work in a school type not using the default path level levels

	LISTDefaultPathLevels LDPL
		inner join lkpLevels L2
		on L2.codeCode = LDPL.levelCode
WHERE L2.lvlYear = L1.lvlYear-1

-- now return the sum across these
Select
	D.schNo,
	D.svyYear [Survey Year],
	LevelCode,
	sum(enrolM) as EnrolM,
	sum(enrolF) as enrolF,
	sum(EnrolNYM) AS EnrolNYM,
	sum(EnrolNYF) AS EnrolNYF,
	sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
	sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
	sum(RepM) AS RepM,
	sum(RepF)  AS RepF,
	sum(RepNYM) AS RepNYM,
	sum(RepNYF)  AS RepNYF,
	sum(RepNYNextLevelM)  AS RepNYNextLevelM,
	sum(RepNYNextLevelF)  AS RepNYNextLevelF,
	E.Estimate,
	E.bestYear [Year of Data],
	E.offset [Age of Data],
	E.bestssqLevel [Data Quality Level],
	E.actualssqLevel [Survey Year Quality Level],
	surveyDimensionssID
from #data D
	INNER JOIN #ebse  E
		on D.schNo = E.SchNo
		and D.svyYear = E.LifeYear
group by D.schNo, D.svyYear, D.LevelCode,
Estimate, bestyear, offset, bestssqlevel, actualssqLevel, surveydimensionssID


DROP TABLE #ebse
DROP TABLE #data
END
GO
GRANT EXECUTE ON [dbo].[sp_EFA12Data] TO [pSchoolRead] AS [dbo]
GO

