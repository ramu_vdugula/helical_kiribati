SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Underlying data for calculation of efa12
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVRepeatersData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table

Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()
-- current years enrolments by level and gender
-----------------------------------------------------------
-- Enrolments
-----------------------------------------------------------
-- enrolments this year
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	E.enLevel LevelCode,
	E.enAge Age,
	E.enM EnrolM,
	E.enF enrolF,
	0 as RepM,
	0 as RepF
INTO #data
FROM #ebse EBSE
inner join Enrollments E
on EBSE.bestssID = E.ssID

-- Repeaters next year next level
INSERT INTO #data
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	R.ptLevel,
	R.ptAge,
	0 as EnrolM,
	0 as enrolF,
	isnull(R.ptM,0) as RepM,
	isnull(R.ptF,0) as RepF
FROM #ebse EBSE
inner join vtblRepeaters R
on EBSE.bestssID = R.ssID


-- now return the sum across these
Select
	D.schNo,
	D.svyYear [Survey Year],
	LevelCode,
	Age,
	Sum(enrolM) AS enrolM,
	Sum(enrolF) AS enrolF,
	Sum(repM) AS repM,
	Sum(repF) AS repF,
	E.Estimate,
	E.Offset [Age of Data],
	E.bestYear [Year of Data],
	E.actualssqLevel [Survey Year Quality Level],
	E.bestssqLevel [Data Quality Level],
	SurveyDimensionssID
	from #data D
	INNER JOIN #ebse  E
		on D.schNo = E.SchNo
		and D.svyYear = E.LifeYear
group by D.schNo, D.svyYear, D.LevelCode,D.Age,
E.Estimate, E.Offset, E.bestYear,E.actualssqLevel, E.bestssqLevel,
surveydimensionssID

DROP TABLE #ebse
DROP TABLE #data
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVRepeatersData] TO [pSchoolRead] AS [dbo]
GO

