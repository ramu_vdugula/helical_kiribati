SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVToiletsSanitationOrig]
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Select * into
#ebst
from dbo.tfnESTIMATE_BestSurveyToilets() E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)

if (@DataColumns is null) begin
	Select @DataColumns = case when paramText in ('SLEMIS','CSEMIS','PLEMIS') then 'SOMALIA'
							else null end
	from SysParams
	WHERE paramName = 'APP_NAME'
end

if (@DataColumns is null) begin
	if (@Group = 'SANITATION')
		EXEC dbo.tmpPIVColsSanitation
	else
		EXEC dbo.tmpPIVColsToilets
end

if (@DataColumns = 'SOMALIA') begin
	print 'SOMALIA'
	if (@Group = 'SANITATION')
		EXEC dbo.tmpPIVColsSanitationSO
	else
		EXEC dbo.tmpPIVColsToiletsSO

end


SELECT
EBT.LifeYear AS [Survey Year],
EBT.Estimate,
EBT.bestYear AS [Year of Data],
EBT.Offset AS [Age of Data],
EBT.bestssqLevel AS [Data Quality Level],
EBT.ActualssqLevel AS [Survey Year Quality Level],
EBT.surveyDimensionssID,
Toilets.*
INTO #TmpPivToilets
FROM #ebst AS EBT
	INNER JOIN ##tmpPivColsToilets Toilets
		ON EBT.bestssID = Toilets.ssID


drop table ##tmpPIVColsToilets

if (@DimensionColumns is null) begin
	SELECT T.*
	FROM #tmpPivToilets T
end


if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCore DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'CORESUMM') begin
	SELECT

	sum([Number]) Number,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      -- groupings specific to this dataset
      , [Type]
      , UsedBy

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      , [Type]
      , UsedBy

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank


	SELECT
	sum([Number]) Number,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
-- specific to this dataset
      , [Type]
      , UsedBy
      -- from DimensionRank
		, sum(DRANK.[School Enrol]) [School Enrol]
		, DRANK.[District Decile],
		DRANK.[District Quartile],

		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

-- specific to this dataset
      , [Type]
      , UsedBy

-- from DimensionRank

	, DRANK.[District Decile],
	DRANK.[District Quartile],
	--DRANK.[Rank],			don;t use rank when grouping
	DRANK.[Decile],
	DRANK.[Quartile]

END

drop table #ebst
drop table #tmpPIVToilets


END
GO

