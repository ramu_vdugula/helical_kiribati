SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 1 2008
-- Description:	update primary classes from xml
-- =============================================
-- THIS ROUTINE will be aboned after 2009 as we move to the full Classes format entry.
CREATE PROCEDURE [pEnrolmentWrite].[updatePrimaryClasses]
	-- Add the parameters for the stored procedure here
	@SurveyID int ,
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- legacy name
	exec pEnrolmentWrite.UpdateClasses @SurveyID, @xml

	return


	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

-- delete existing
-- the xml may pass in classType, minSeq and maxSeq
-- defining the range of items in scope. this should be used for deleting
--- the preexisitng - ie we only delete what is being replaced.

	declare @classType nvarchar(1)
	declare @minSeq int
	declare @maxSeq int

	select @classType = classType,
			@minSeq = minSeq,
			@maxSeq = maxSeq
	FROM
		OPENXML (@idoc, '/Classes',2)
		WITH (
				classType nvarchar(1) '@classType',
				minSeq int '@minSeq',
				maxSeq int '@maxSeq'
				)


if (@classType) is null
	begin
		DELETE from PrimaryClassTeacher WHERE pcId in
		(select pcID from PrimaryClass WHERE ssID = @surveyID)

		DELETE from PrimaryClass WHERE ssID = @surveyID			-- cascade does the rest
	end
else
	-- as passed in from classes form
	begin
		DELETE from PrimaryClassTeacher WHERE pcId in
		(select pcID from PrimaryClass
			WHERE ssID = @surveyID
				AND pcCat = @classType
				AND pcSeq between @minSeq and @maxSeq)

		DELETE from PrimaryClass WHERE
			ssID = @surveyID			-- cascade does the rest
				AND pcCat = @classType
				AND pcSeq between @minSeq and @maxSeq
	end

   INSERT INTO PrimaryClass(ssID, pcSeq, pcCat)

	Select @SurveyID, seq, classType
		from OPENXML (@idoc, '/Classes/Class',2)
		WITH (
				seq int '@seq',
				classType nvarchar(1) '../@classType'
				)


-- insert the level info

	INSERT INTO PrimaryClassLevel
	(
		pcID, pclLevel, pcLNum
	)
	Select pcID, levelCode, enrol
	FROM
	PrimaryClass,
	OPENXML (@idoc, '/Classes/Class/Levels/Level',2)
		WITH (
				seq int '../../@seq',
				levelCode nvarchar(10) '@Tag',
				enrol int '@Enrol'
				) PCL
	WHERE PrimaryClass.pcSeq = PCL.seq
	AND ssID = @surveyID

	select * from primaryclass where ssID = @surveyID
	select pcl.* from primaryclass
	inner join primaryclasslevel pcl on primaryclass.pcID = pcl.pcID
	where ssID = @surveyID

-- insert the teacher info
-- the challengeis to find the tchsId created when the teacher with this tag was loaded
-- tag here is the primary key tsmisID from the SMIS system

	INSERT INTO PrimaryClassTeacher
	(pcID , tchsID, pctSeq)
	SELECT pcID, tchsID, TeacherSeq
	FROM PrimaryClass PC, TeacherSurvey TS,
	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				smisID nvarchar(10) '@Tag',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq'
				) XMLT
	WHERE PC.pcSeq = xmlt.seq
	AND xmlt.smisID = TS.tchsmisID
	AND TS.ssID = @surveyID
	AND PC.ssID = @surveyID

-- pineapples itself supplies the tid, not the smisid .
-- since these are mutually exclusive , we can do the inserts
-- based on tid too

	INSERT INTO PrimaryClassTeacher
	(pcID , tchsID, pctSeq)
	SELECT pcID, tchsID, TeacherSeq
	FROM PrimaryClass PC, TeacherSurvey TS,
	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				smisID nvarchar(10) '@Tag',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq'
				) XMLT
	WHERE PC.pcSeq = xmlt.seq
	AND xmlt.tID = TS.tID
	AND TS.ssID = @surveyID
	AND PC.ssID = @surveyID


-- if called from a survey form, and we already have category and sequence that we do not want to change, exit here
if not (@classType is null)
	return 0


-- this section is for the data arriving from SMIS
-- it classifies according to whether the school is S, J, or C
-- and renumbers in ascending sequence
-- this part is not used when this sp is called from a survey form
-- there are too many issues with shifting items betwen the various tabs on the form
UPDATE Primaryclass
	SET pcCat = 'C'
WHERE ssID = @SurveyID
AND pcID in (Select pcID from PrimaryClassLevel GROUP BY pcID having count(pcID) > 1)

UPDATE Primaryclass
	SET pcCat = 'J'
WHERE ssID = @SurveyID
AND pcCat is null
AND pcID in (Select pcID from PrimaryClassTeacher GROUP BY pcID having count(pcID) > 1)

-- single classes
UPDATE PrimaryClass
	SET pcCat = 'S'
WHERE ssID = @SurveyID
AND pcCat is null

update PrimaryClass SET pcSeq = null WHERE ssId = @surveyID

declare @i int
select @i = 0

declare @pcID int
select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null

	end
END
GO

