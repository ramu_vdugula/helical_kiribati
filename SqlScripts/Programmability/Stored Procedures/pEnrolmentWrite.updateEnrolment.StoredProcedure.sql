SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pEnrolmentWrite].[updateEnrolment]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@grid xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @grdtbl table(	Age int,
							EnLevel nvarchar(20) collate database_default,
							M int,
							F int)
	-- this is the set of column tags for the grid
	declare @colSet table( colLevel nvarchar(20) collate database_default
							)
	-- this is the set of row tags for the grid
	declare @rowSet table( rowAge nvarchar(20)  collate database_default
							)

	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- populate the table of grid values
	INSERT @grdtbl
	Select * from OPENXML (@idoc, '/Grid/values/row/col/data',2)
		WITH (
				Age int '../../@Tag',
				EnLevel nvarchar(20) '../@Tag',
				M int '@M',
				F int '@F')
-- get the complete set of columns
	INSERT @colSet
	Select colLevel from OPENXML(@idoc, '/Grid/colSet/col',2)
		WITH (
				colLevel nvarchar(20) '@Tag'
			)

-- get the complete set of rows
	INSERT @rowSet
	Select rowAge from OPENXML(@idoc, '/Grid/rowSet/row',2)
		WITH (
				rowAge int '@Tag'
			)
-- 13 12 2007 this supports pineapples @school, which will send grids without colSet
-- or rowSet, assuming that all data will be supplied in one grid
declare @rowRange int;
select @rowRange = count(*) from @rowSet


declare @colRange int;
select @colRange = count(*) from @colSet

-- delete
-- if column and rowset specified, restrict the delete to that range
-- otherwise delete all

-- transaction control


begin transaction

begin try

	DELETE from Enrollments
	from Enrollments
	WHERE ssID = @SurveyID
		AND (
			enAge = any (Select rowAge from @rowSet)
			or @rowRange = 0
			)
		AND (
			enLevel = any (Select colLevel from @colSet)
			or @colRange = 0
			)

	-- insert from the grid

	INSERT INTO Enrollments(ssID, enAge, enLevel, enOrigin, enM, enF)
	SELECT @surveyID, Age, EnLevel,
		null,
	-- values
	G.M, G.F

	FROM @grdtbl G
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

exec LogActivity 'updateEnrolment', @SurveyID
return


END
GO

