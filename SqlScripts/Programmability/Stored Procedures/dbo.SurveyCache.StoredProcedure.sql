SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SurveyCache]
	-- Add the parameters for the stored procedure here
	@SurveyID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- return multiple resultsets to populate the survey form cache
	Select * from Enrollments WHERE ssID = @SurveyID

	Select * from PupilTables WHERE ssID = @SurveyID

	Select * from Resources WHERE ssID = @SurveyID

	Select * from SchoolSurveyQuality WHERE ssID = @SurveyID

	Select * from TRMetaPupilTableDefs

	Select * from TRMetaResourceDefs


END
GO
GRANT EXECUTE ON [dbo].[SurveyCache] TO [pSchoolRead] AS [dbo]
GO

