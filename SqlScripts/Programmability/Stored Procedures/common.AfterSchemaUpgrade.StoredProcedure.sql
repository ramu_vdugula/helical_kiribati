SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 3 2010
-- Description:	Tasks to run every time a database schema upgrade is performed
-- =============================================
CREATE PROCEDURE [common].[AfterSchemaUpgrade]
	-- Add the parameters for the stored procedure here
with execute as 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- some pineapples specifics


 declare @output table
 (
	msg nvarchar(400)
 )
-- we should  rebuild the listxtab in case it got updated
	INSERT INTO @output
	VALUES('*** START ***')

   exec dbo.MakeListXTb
 	INSERT INTO @output
	VALUES('List XTab rebuilt')

 	-- this code will re-enable all disabled triggers

 	INSERT INTO @Output
 	select 'Trigger ' + [name] + ' is disabled' from sys.triggers
	WHERE is_disabled = 1
	INSERT INTO @output
	exec common.EnableAllTriggers

  	INSERT INTO @Output
 	select 'Trigger ' + [name] + ' is still disabled after repair' from sys.triggers
	WHERE is_disabled = 1

  	-- computed columns is something that Delta doesn't quite get right
	if not exists(Select * from sys.computed_columns WHERE [name] = 'enSum') begin
		if exists (Select * from INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = 'Enrollments'
								AND COLUMN_NAME = 'enSum'
								AND TABLE_SCHEMA = 'dbo') begin
				ALTER TABLE dbo.Enrollments
					DROP COLUMN enSum
				INSERT INTO @output
				VALUES('Noncomputed column enSum was dropped')
		end

		ALTER TABLE dbo.Enrollments
			ADD enSum as (case when enM is null and enF is null then null
							else isnull([enM],(0))+isnull([enF],(0))
							end)
		INSERT INTO @output
		VALUES('Computed column enSum was added')

	end

	-- popSum
	if not exists(Select * from sys.computed_columns WHERE [name] = 'popSum') begin
		if exists (Select * from INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = 'Population'
								AND COLUMN_NAME = 'popSum'
								AND TABLE_SCHEMA = 'dbo') begin
				ALTER TABLE dbo.[Population]
					DROP COLUMN popSum
				INSERT INTO @output
				VALUES('Noncomputed column popSum was dropped')
		end

		ALTER TABLE dbo.[Population]
			ADD popSum as (case when popM is null and popF is null then null
							else isnull([popM],0)+isnull([popF],0)
							end)
		INSERT INTO @output
		VALUES('Computed column popSum was added')
	end

	-- enrolmentProjectionData
	if not exists(Select * from sys.computed_columns WHERE [name] = 'epdSum') begin
		if exists (Select * from INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = 'EnrolmentProjectionData'
								AND COLUMN_NAME = 'epdSum'
								AND TABLE_SCHEMA = 'dbo') begin
			ALTER TABLE dbo.EnrolmentProjectionData
				DROP COLUMN epdSum
				INSERT INTO @output
				VALUES('Noncomputed column epdSum was dropped')
		end

		ALTER TABLE dbo.EnrolmentProjectionData
			ADD epdSum as (case when epdM is null and epdF is null and epdU is null then null
							else isnull(epdM,0)+isnull(epdF,0)+isnull(epdU,0)
							end)
		INSERT INTO @output
		VALUES('Computed column epdSum was added')
	end
	-- PupilTables
	if not exists(Select * from sys.computed_columns WHERE [name] = 'ptSum') begin
		if exists (Select * from INFORMATION_SCHEMA.COLUMNS
						WHERE TABLE_NAME = 'PupilTables'
								AND COLUMN_NAME = 'ptSum'
								AND TABLE_SCHEMA = 'dbo') begin
			ALTER TABLE dbo.PupilTables
				DROP COLUMN ptSum
				INSERT INTO @output
				VALUES('Noncomputed column ptSum was dropped')
		end

		ALTER TABLE dbo.PupilTables
			ADD ptSum as (case when ptM is null and ptF is null  then null
							else isnull(ptM,0)+isnull(ptF,0)
							end)
		INSERT INTO @output
		VALUES('Computed column ptSum was added')
	end
	INSERT INTO @output
	exec common.DataIntegrity
 	INSERT INTO @output
	VALUES('*** END ***')


-- this part simply rebuilds all views and sps
    -- refresh all the big views in this project
 declare @ViewName nvarchar(255);
 declare @view nvarchar(500)
 declare @schema nvarchar(500)

--CREATE CURSOR TO READ THE DATABASE VIEWS IN ORDER TO VALIDATE
	Declare @CrsrView Cursor
	Set @CrsrView = Cursor For
		SELECT TABLE_SCHEMA, TABLE_NAME
			FROM INFORMATION_SCHEMA.VIEWS
			 ORDER BY TABLE_SCHEMA, TABLE_NAME


	declare @SomethingToDo int = 1
	declare @savedErrorcount int = 0
	declare @ThisErrorcount int = 0
-- fetch the first record in the Cursor

	while (@SomethingToDo = 1) begin
		Open @CrsrView
		Fetch Next From @CrsrView Into @schema, @view
		SELECT @ThisErrorCount = 0
		 While (@@FETCH_STATUS = 0) begin --WHILE THE CURSER STILL INCLUDE RECORDS
			--refresh the view
			 SET @ViewName = quotename(@schema) + '.' + quotename(@view);
			 PRINT 'Refreshing View... ' + @ViewName;
			 BEGIN TRY
				begin transaction
				EXEC sp_refreshview @viewname;
				commit
			 END TRY
			 BEGIN CATCH
				rollback
				print 'Error on ' + @viewName + ' ' + ERROR_MESSAGE()
				SELECT @ThisErrorCount = @ThisErrorCount + 1
			 END CATCH

			-- fetch the next record in the Cursor
			 Fetch Next From @CrsrView Into @schema, @view

		 END

		 Close @CrsrView
		 Select @SomethingtoDo = 0
		 if (@ThisErrorCount <> @SavedErrorCount AND @ThisErrorcount > 0) begin
			Select @SavedErrorCount = @ThisErrorCount
			Select @SomethingToDo = 1
		end

	end

	DEALLOCATE @CrsrView


-- stored procs now
	Set @CrsrView = Cursor For
		SELECT SPECIFIC_SCHEMA, SPECIFIC_NAME
			FROM INFORMATION_SCHEMA.ROUTINES
			 WHERE ROUTINE_TYPE = 'PROCEDURE'
			 ORDER BY SPECIFIC_SCHEMA, SPECIFIC_NAME


	Select @SomethingToDo = 1
	select @savedErrorcount  = 0
	select @ThisErrorcount  = 0
-- fetch the first record in the Cursor

	while (@SomethingToDo = 1) begin
		Open @CrsrView
		Fetch Next From @CrsrView Into @schema, @view
		SELECT @ThisErrorCount = 0
		 While (@@FETCH_STATUS = 0) begin --WHILE THE CURSER STILL INCLUDE RECORDS
			--refresh the view
			 SET @ViewName = quotename(@schema) + '.' + quotename(@view);
			 PRINT 'Refreshing proc... ' + @ViewName;
			 BEGIN TRY
				EXEC sp_recompile @viewname;
			 END TRY
			 BEGIN CATCH
				print 'Error on ' + @viewName
				SELECT @ThisErrorCount = @ThisErrorCount + 1
			 END CATCH

			-- fetch the next record in the Cursor
			 Fetch Next From @CrsrView Into @schema, @view

		 END

		 Close @CrsrView
		 Select @SomethingtoDo = 0
		 if (@ThisErrorCount <> @SavedErrorCount AND @ThisErrorcount > 0) begin
			Select @SavedErrorCount = @ThisErrorCount
			Select @SomethingToDo = 1
		end

	end
	DEALLOCATE @CrsrView

	SELECT * from @output
END
GO

