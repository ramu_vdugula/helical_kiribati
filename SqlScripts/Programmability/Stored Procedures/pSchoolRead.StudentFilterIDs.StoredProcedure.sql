SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Student filter
--              TODO - Add ability to filter by currently enrolled school
-- =============================================
CREATE PROCEDURE [pSchoolRead].[StudentFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@StudentID uniqueidentifier = null,
	@StudentCardID nvarchar(20) = null,
    @StudentGiven nvarchar(50) = null,
    @StudentFamilyName  nvarchar(50) = null,
    @StudentDoB  date = null,
	@StudentGender nvarchar(1) = null,
	@StudentEthnicity nvarchar(200) = null
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
	selectedStuID nvarchar(50)
	, recNo int IDENTITY PRIMARY KEY
	)

	INSERT INTO @keys
	(selectedStuID)
	SELECT stuID
	FROM Student_ S
	WHERE
	(stuID = @StudentID or @StudentID is null)
	AND (S.stuCardID = @StudentCardID or @StudentCardID is null)
	AND (S.stuGiven = @StudentGiven or @StudentGiven is null)
	AND (S.stuFamilyName = @StudentFamilyName or @StudentFamilyName is null)
	AND (S.stuDoB = @StudentDoB or @StudentDoB is null)
	AND (S.stuGender = @StudentGender or @StudentGender is null)
	AND (S.stuEthnicity = @StudentEthnicity or @StudentEthnicity is null)

	ORDER BY
		CASE @sortColumn			-- strings
			WHEN 'StudentID' then stuID
			WHEN 'StudentCardID' then stuCardID
			WHEN 'StudentGiven' then stuGiven
			WHEN 'StudentFamilyName' then stuFamilyName
			WHEN 'StudentEthnicity' then stuEthnicity
			WHEN 'StudentGender' then stuGender
			ELSE null
			END,
		CASE @sortColumn -- dates
			WHEN 'StudentDoB' then stuDoB
			ELSE null
		END,

		stuID


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedStuID
		, RecNo
		FROM
		(
			Select selectedStuID
			, @NumMatches - RecNo + 1 RecNo
			FROM @Keys
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)
				)
		ORDER BY RecNo
	end
	else begin

		SELECT selectedStuID
		, RecNo
		FROM @Keys
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
END
GO

