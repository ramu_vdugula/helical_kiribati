SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- 6 11 2012 added collate to temp tables
-- 02 01 2015 added Primary key to @keysAll, changed handling of sort to order by
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolFilterIDs]
	-- Add the parameters for the stored procedure here

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@SchoolNo   nvarchar(50) = null,
	@SchoolName nvarchar(50) = null,
    @fSearchAlias int = 0, -- 1 = search 2 = search for exclusion
    @AliasSource nvarchar(50) = null,
    @AliasNot int = 0,
    @SchoolType nvarchar(10) = null,
	@SchoolClass nvarchar(10) = null,
	@District nvarchar(10) = null,
    @Island nvarchar(10) = null,
	@ElectorateN nvarchar(10) = null,
	@ElectorateL nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@Language nvarchar(10) = null,
	@InfrastructureClass nvarchar(10) = null,
	@InfrastructureSize nvarchar(10) = null,
	@ShipService nvarchar(50) = null,
	@BankService nvarchar(50) = null,
	@PostService nvarchar(50) = null,
	@AirService nvarchar(50) = null,
	@Clinicservice nvarchar(50) = null,
	@HospitalService nvarchar(50) = null,
	@SearchIsExtension int = null,
	@PArentSchool nvarchar(50) = null,
	@SEarchHasExtension int = null,
	@ExtensionReferenceYear int = null,	-- look on school year history - if null, use the Active Year
	@YearEstablished int = null,
	@RegistrationStatus nvarchar(10) = null,	-- see case for allowed values
	@SearchReg int = 0,				--search for the school name text in the reg no too
	@CreatedAfter datetime = null,
	@EditedAfter datetime = null,
	@IncludeClosed int = 0,
	@ListName nvarchar(20) = null,
	@ListValue nvarchar(10)= null,
	@AuthorityGovt nvarchar(1) = null,
	@xmlFilter xml = null


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	SET FMTONLY OFF;	-- becuase Entity Framework 4.0 is a typical Microsoft 2 foot deep pool!


if (@xmlFilter is not null) begin

declare @idoc int
EXEC sp_xml_preparedocument @idoc OUTPUT, @xmlFilter

	Select
	@SchoolNo = isnull(@SchoolNo,SchoolNo)
	, @SchoolName = isnull(@SchoolName,SchoolName)
	, @fSearchAlias = isnull(@fSearchAlias,SearchAlias)
	--, @AliasNot = isnull(@AliasNot,AliasNot)
	, @SchoolType = isnull(@SchoolType,SchoolType)
	, @SchoolClass = isnull(@SchoolClass,SchoolClass)
	, @District = isnull(@District,District)
	, @Island = isnull(@Island,Island)
	, @ElectorateN = isnull(@ElectorateN,ElectorateN)
	, @ElectorateL = isnull(@ElectorateL,ElectorateL)
	, @Authority = isnull(@Authority,Authority)
	, @Language = isnull(@Language,Language)
	, @InfrastructureClass = isnull(@InfrastructureClass,InfrastructureClass)
	, @InfrastructureSize = isnull(@InfrastructureSize,InfrastructureSize)
	, @ShipService = isnull(@ShipService,NearestShipping)
	, @BankService = isnull(@BankService,NearestBank)
	, @PostService = isnull(@PostService,NearestPO)
	, @AirService = isnull(@AirService,NearestAirstrip)
	, @ClinicService = isnull(@ClinicService,NearestClinic)
	, @HospitalService = isnull(@HospitalService,NearestHospital)
	, @SearchIsExtension = isnull(@SearchIsExtension,SearchIsExtension)
	, @ParentSchool = isnull(@ParentSchool,ParentSchool)
	, @SearchHasExtension = isnull(@SearchHasExtension,SearchHasExtension)
	, @ExtensionReferenceYear = isnull(@ExtensionReferenceYear,ExtensionReferenceYear)
	, @YearEstablished = isnull(@YearEstablished,YearEstablished)
	, @RegistrationStatus = isnull(@RegistrationStatus,RegistrationStatus)
	, @SearchReg = isnull(@SearchReg,SearchRegNo)
	, @CreatedAfter = isnull(@CreatedAfter,CreatedAfter)
	, @EditedAfter = isnull(@EditedAfter,EditedAfter)
	, @IncludeClosed = isnull(@IncludeClosed,IncludeClosed)
	, @ListName = isnull(@ListName, ListName)
	, @ListValue = isnull(@ListValue, ListValue)
	, @AuthorityGovt = isnull(@AuthorityGovt, AuthorityGovt)
	FROM OPENXML(@idoc,'//Filter',2)
	WITH
	(
	SchoolNo    nvarchar(50)     '@SchoolNo'
	,SchoolName    nvarchar(100)     '@SchoolName'
	,SearchAlias    int     '@SearchAlias'
	,AliasNot    nvarchar(50)     '@AliasNot'
	,SchoolType    nvarchar(10)     '@SchoolType'
	,SchoolClass    nvarchar(10)     '@SchoolClass'
	,District    nvarchar(10)     '@District'
	,Island    nvarchar(10)     '@Island'
	,ElectorateN    nvarchar(10)     '@ElectorateN'
	,ElectorateL    nvarchar(10)     '@ElectorateL'
	,Authority    nvarchar(10)     '@Authority'
	,Language    nvarchar(10)     '@Language'
	,InfrastructureClass    nvarchar(10)     '@InfrastructureClass'
	,InfrastructureSize    nvarchar(10)     '@InfrastructureSize'
	,NearestShipping    nvarchar(10)     '@NearestShipping'
	,NearestBank    nvarchar(50)     '@NearestBank'
	,NearestPO    nvarchar(50)     '@NearestPO'
	,NearestAirstrip    nvarchar(50)     '@NearestAirstrip'
	,NearestClinic    nvarchar(50)     '@NearestClinic'
	,NearestHospital    nvarchar(50)     '@NearestHospital'
	,SearchIsExtension    int				'@SearchIsExtension'
	,ParentSchool    nvarchar(50)			'@ParentSchool'
	,SearchHasExtension    int				'@SearchHasExtension'
	,ExtensionReferenceYear    int			'@ExtensionReferenceYear'
	,YearEstablished    int					'@YearEstablished'
	,RegistrationStatus    nvarchar(50)     '@RegistrationStatus'
	,SearchRegNo    int						'@SearchRegNo'
	,CreatedAfter    datetime				'@CreatedAfter'
	,EditedAfter    datetime				'@EditedAfter'
	,IncludeClosed    int					'@IncludeClosed'
	,ListName		nvarchar(20)			'@ListName'
	,ListValue		nvarchar(10)			'@ListValue'
	,AuthorityGovt nvarchar(1)				'@AuthorityGovt'
	)


end
	DECLARE @keysAll TABLE
	(
	selectedSchNo nvarchar(50)
	, recNo int IDENTITY PRIMARY KEY
	)


	declare @sql nvarchar(2000)
	declare @Params nvarchar(500)
	declare @TablesToUse int
	select @TablesToUse = 0

-- rules/cases for alias search
-- no name specified, search alias flag on no action (return any school, or any school with any alias)
-- name specified, alias flag on - match schname, or any alias
-- name specified alias flag on, specific item - match name or alias in the specific list
-- no name specified, alias flag on, specific item - any item in specific alias list
-- name specified, alias flag on, specific item, NOT : any school with name like name' not incuded in the specified alias list
-- no name specified alias flag on, specific item, NOT :any school not in the specific alias list
declare @AliasMode int
select @AliasMode = 0
declare @fForceAlias int
select @fForceAlias = 0


declare @UseSurvey int
select @UseSurvey = 0


DECLARE @alias TABLE
(
	schNo nvarchar(50) COLLATE DATABASE_DEFAULT
)

DECLARE @survey TABLE
(
	schNo nvarchar(50) COLLATE DATABASE_DEFAULT
)

Declare @authGov TABLE
(
	schNo nvarchar(50) COLLATE DATABASE_DEFAULT
)


-- make sure nulls are treated as defaults where null is not valid
-- This is to support Entity Framework 4.0
SELECT @IncludeClosed = isnull(@IncludeClosed,0)
Select @fSearchAlias = isnull(@FSearchAlias,0)

if (@fSearchAlias = 1)
	select @AliasMode = 4
if (@fSearchAlias = 2)
	Select @AliasMode = 8
if (@SchoolName is not null)
	Select @AliasMode =@AliasMode + 2
if (@AliasSource is not null)
	select @AliasMode = @AliasMode +1


if (@AliasMode = 9 or @AliasMode = 11)
	-- name is blank, not in specified list
	-- SELECTED school MUST be in #ALIAS
BEGIN
	insert into @alias
	select schNo
	From Schools WHERE
		schNo not in (Select schNo from SchoolAlias WHERE saSrc = @AliasSource)
	select @fForceAlias = 1
END

if @AliasMode = 5
	-- search , in specific alias list, no name
	--
BEGIN

	insert into @alias
	select schNo
	From SchoolAlias
	WHERE saSrc = @AliasSource
	select @fForceAlias = 1
END

-- allow web client to use enduser * and ? conventions for name
if (@schoolName is not null)
	Select @schoolName = replace(replace(@SchoolName,'*','%'),'?','_')


if ((@ShipService is not null) or
	(@BankService is not null) or
	(@PostService is not null) or
	(@AirService is not null) or
	(@ClinicService is not null) or
	(@InfrastructureClass is not null) or
	(@InfrastructureSize is not null))		BEGIN

			INSERT INTO @survey
			Select S.schNo
			from Schools S
				inner join SchoolSurvey SS on S.schNo = ss.SchNo
			WHERE
				SS.svyYear = (Select max(svyYear) from SchoolSurvey ss2 where ss2.schno = S.schNo)
				AND (ssSvcAir = @AirService or @AirService is null)
				AND (ssSvcPost = @PostService or @PostService is null)
				AND (ssSvcShip = @ShipService or @ShipService is null)
				AND (ssSvcBank = @BankService or @BankService is null)
				AND (ssSvcClinic = @ClinicService or @ClinicService is null)
				AND (ssSvcHospital = @HospitalService or @HospitalService is null)
				AND (ssISClass = @InfrastructureClass or @InfrastructureClass is null)
				AND (ssISSize = @InfrastructureClass or @InfrastructureClass is null)

			select @UseSurvey = 1
end

if (@AuthorityGovt is not null) BEGIN
	INSERT INTO @authGov
	Select schno from Schools S
		INNER JOIN DimensionAuthority A
			ON S.schAuth = A.AuthorityCode
	WHERE A.AuthorityGroupCode = @AuthorityGovt
END

INSERT INTO @keysAll
(
	selectedSchNo
)
SELECT
s.schNo
from Schools S
	LEFT OUTER JOIN Islands I
		on S.iCode = I.iCode
	LEFT OUTER JOIN Districts D
		on D.dID = I.iGroup
	LEFT OUTER JOIN lkpElectorateN N
		ON S.schElectN = N.codeCode
	LEFT OUTER JOIN lkpElectorateL L
		ON S.schElectL = L.codeCode
	LEFT OUTER JOIN (Select syParent, count(schNo) NumExtensions from SchoolYearHistory
						WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
						GROUP BY syParent) Extensions
		ON S.schNo = Extensions.syParent
	LEFT OUTER JOIN (Select schNo PSchNo, syParent
						FROM SchoolYearHistory
						WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
					) Parent
					ON S.schNo = Parent.PschNo
WHERE
	(S.schNo = @SchoolNo or @SchoolNo is null)
	AND
		(


			(@SchoolName is null or
				(schName like @SchoolName + '%' or
					(@fSearchAlias = 1 and
						S.SchNo in
							(Select schNo from SchoolAlias WHERE saAlias like (@SchoolName + '%')
								AND (saSrc=@AliasSource OR @AliasSource is null))
					) or
					(@SearchReg = 1 and
						S.SchReg = @SchoolName
					)


				)
			)
		)

	AND (schType = @schoolType or @SchoolType is null)
	AND (SchClass = @SchoolClass or @SchoolClass is null)
	AND (S.iCode = @Island or @Island is null)
	AND (I.iGroup = @District or @District is null)
	AND (schElectN = @ElectorateN or @ElectorateN is null)
	AND (schElectL = @ElectorateL or @ElectorateL is null)
	AND (schAuth = @Authority or @Authority is null)
	AND (schLang = @Language or @Language is null)

	AND (schEst = @YearEstablished or @YearEstablished is null)
	AND (schClosed = 0 or schClosed > year(getdate()) or @IncludeClosed = 1)		-- fixed 26 6 2010

	AND (schNo = any (Select schNo from @survey)
			OR @UseSurvey = 0)
	AND (schNo = any (Select schNo from @alias)
			OR @fForceAlias = 0)
	-- support search on AuthorityGovt
	AND (schNo = any (Select schNo from @authGov)
			OR @AuthorityGovt is null)


	----AND ( ( schNo in (Select schParent from Schools)
	----		AND @SearchHasExtension = 1)
	----	OR (schNo <> any (Select schParent from Schools)
	----		AND @SearchHasExtension = 0)
	----	OR @SearchHasExtension is null
	----	)

	AND (( Extensions.syParent is not null
			AND @SearchHasExtension = 1)
		OR (Extensions.syParent is null
			AND @SearchHasExtension = 0)
		OR @SearchHasExtension is null
		)

	AND ( (Parent.syParent is not null
			AND @SearchIsExtension = 1)
		OR (Parent.syParent is  null
			AND @SearchIsExtension = 0)
		OR @SearchIsExtension is null
		)

	AND (
			Parent.syParent =@ParentSchool

		OR @ParentSchool is null
		)

	AND ( @RegistrationStatus is null or
			case @RegistrationStatus
				when 'NULL' then
					case when schRegStatus is null then 1 end
				when 'ANY' then
					case when schRegStatus is not null then 1 end
				when 'PXX' then
					case when schRegStatus in ('PX','X') then 1 end
				when 'PRR' then
					case when schRegStatus in ('PR','R') then 1 end
				when 'P' then
					case when schRegStatus in ('PX','PR') then 1 end
				when 'RX' then
					case when schRegStatus in ('X','R') then 1 end
				else
					case when schRegStatus =@RegistrationStatus then 1 end
			end = 1
		)
	AND ( S.pEditDateTime >= @EditedAfter or @EditedAfter is null)
	AND ( S.pCreateDateTime >= @CreatedAfter or @CreatedAfter is null)
	-- support custom lists 19 11 2015
	AND ( @ListName is null or
			( @ListValue is not null and schNo in (Select schNo from ListSchools
									WHERE lstName = @ListName And lstValue = @ListValue)
				or schNo in (Select schNo from ListSchools WHERE lstName = @ListName)
			)
		)

	ORDER BY
			case @SortColumn
				when 'schName' then schName
				when 'iName' then I.iName
				when 'dName' then D.dName
				when 'schVillage' then schVillage
				when 'schAuth' then schAuth
				when 'schType' then schType
				else schNo
			end

	OPTION(RECOMPILE)


SELECT @NumMatches = @@ROWCOUNT

-- now return the page, sorted in the right sequence and direction

If @SortDir = 1 begin


		SELECT selectedschNo
		, RecNo
		FROM
		(
			Select selectedschNo
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin


		SELECT selectedSchNo
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end

END
GO

