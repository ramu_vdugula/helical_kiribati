SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[setupSchema]
	-- Add the parameters for the stored procedure here
	@schemaName sysname ,
	@Owner sysname = 'dbo'
AS
BEGIN


	if exists(select s.name from sys.schemas S where s.name = @schemaName)
		print 'Schema ' + @schemaName + ' already exists'
	else begin
		begin try
			declare @sql nvarchar(200)
			select @sql = 'CREATE SCHEMA ' + @schemaName + ' AUTHORIZATION ' + @Owner
			exec sp_executesql @sql
			print 'Schema ' + @schemaName + ' created'

		end try
		begin catch
			print 'Error creating schema ' + @schemaName + ': ' + ERROR_MESSAGE()
		end catch
	end
	select @sql = 'GRANT VIEW DEFINITION ON Schema::' + @schemaName + ' TO PUBLIC'
	begin try
			exec sp_executesql @sql
	end try
	begin catch
			print 'could not grant View definition on  ' + @schemaName + ' to public'
	end catch


END
GO

