SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for pupil classrom ratio
-- =============================================
CREATE PROCEDURE [dbo].[sp_INDPupilClassroomRatioData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- need the cahced enrolment classroom estimate

SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyRoomType('CLASS')


-- create the shape of the temp data
create table #data
(
LifeYear int,
schNo nvarchar(50),
[Room Data Year] int,
[Room Estimate] int,
[Age of Room Data] int,
RoomsurveyDimensionssID int,

[Room Data Quality Level] int,
[Enrol Estimate] int,
[Enrol Data Year] int,
[Age of Enrol Data] int,
[Enrol Data Quality Level] int,
surveyDimensionssID int,
Sector nvarchar(10),
Enrol int,
NumRooms int,
TotSize int,
minSize int,
[MaxSize] int,
SizeSupplied int,
sizeOK int,
RoomDataSupplied int
)


-- insert the enrolment data
INSERT INTO #data
(
   LifeYear
, SchNo
, surveyDimensionssID
, Enrol
, [Enrol Estimate]
, [Enrol Data Year]
, [Age of Enrol Data]
, [Enrol Data Quality Level]

)
SELECT
	E.LifeYear,
	E.schNo,
	E.surveyDimensionssID,
	SS.ssEnrol
, Estimate
, bestYear
, Offset
, bestssqLevel
FROM #ebse E
	INNER JOIN SchoolSurvey SS
		ON E.bestssID = ss.ssID

-- classrooms
INSERT INTO #data
(
LifeYear, schNo,
NumRooms,
TotSize,
minSize,
[MaxSize],
SizeSupplied,
sizeOK,
RoomDataSupplied
,[Room Data Year]
, [Room Estimate]
, [Age of Room Data]
, [Room Data Quality Level]
, RoomSurveyDimensionssID
)
Select
E.LifeYear, E.schNo,
NumRooms,
TotSize,
minSize,
[MaxSize],
SizeSupplied,
sizeOK,
1 as RoomDataSupplied
, bestYear
, Estimate
, offSet
, bestssqLevel
, surveyDimensionssID

FROM #ebr E
	INNER JOIN ssIDClassrooms C
		ON E.bestssID = C.ssID


Select LifeYear,
schNo,
min([Room Estimate]) [Room Estimate],		-- all values are the same except for NULL
min([Room Data Year]) [Room Data Year],
min([Age of Room Data]) [Age of Room Data],
min([Room Data Quality Level]) [Room Data Quality Level],

min([Enrol Estimate]) [Enrol Estimate],		-- all values are the same except for NULL
min([Enrol Data Year]) [Enrol Data Year],
min([Age of Enrol Data]) [Age of Enrol Data],
min([Enrol Data Quality Level]) [Enrol Data Quality Level],

case min(SurveydimensionssID)
	when null then min(RoomSurveyDimensionssID)
	else min(SurveydimensionssID)
end surveydimensionssID,

sum(NumRooms) NumRooms,
sum(TotSize) TotSize,
sum(minSize) minSize,
sum([MaxSize]) [MaxSize],
min(SizeSupplied) SizeSupplied,			-- only one roww

sum(Enrol) Enrol

from #data D
GROUP BY LifeYear,schNo

drop table #data
drop table #ebse
drop table #ebr
END
GO
GRANT EXECUTE ON [dbo].[sp_INDPupilClassroomRatioData] TO [pSchoolRead] AS [dbo]
GO

