SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pInfrastructureAdmin].[ImportTeacherHouses]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


create table #ebs
(
	schNo nvarchar(50),
	LifeYear int,
	resName nvarchar(50),
	ActualssID int,
	bestssID int,
	bestYear int,
	Offset int,
	bestssqLevel int,
	ActualssqLevel int,
	SurveyDimensionssID int,
	Estimate smallint,
	RowNo int
)


create table #th
(
	schNo nvarchar(50),
	[Survey Year] int,
	surveyDimensionssID int,
	HousingType nvarchar(50),
	Materials nvarchar(50),
	Condition nvarchar(1),
	NumHouses int,
	MaritalStatus nvarchar(1),
	HouseProvided int,
	HouseProvidedYN nvarchar(1),
	HouseStatus nvarchar(20),
	NumTeachers int,
	PartnerIsTeacher int,
	PartnerIsTeacherYN nvarchar(1),
	NumHouseRequired int,
	HouseEstimate int,
	AgeOfHousingData int,
	TeacherEstimate int,
	AgeOfTeacherData int
)


		insert into #ebs
		select * from
		(
		select *
			, ROW_NUMBER() OVER(PARTITION BY schno, LifeYear ORDER BY ABS(Offset)) RowNo
		from ESTIMATE_BestSurveyResourceCategory EBSRC
		WHERE				(EBSRC.resName like 'Staff Housing%'
						or EBSRC.resName = 'Teacher Housing')
			AND LifeYEar in (select (max(svyYear))  from Survey)
		) sub
		WHERE 	RowNo = 1
		SELECT *
		INTO #ebst
		from ESTIMATE_BestSurveyTeachers
	-- get the teacher housing records to put in the table
	--
	INSERT INTO #th
	( schNo
		, [Survey Year]
		, surveyDimensionssID
		, HousingType
		, Materials
		, Condition
		, NumHouses
		, MaritalStatus
		, HouseProvided
		, HouseProvidedYN
		, HouseStatus
		, NumTeachers
		, PartnerIsTeacher
		, NumHouseRequired
		, HouseEstimate
		, AgeOfHousingData
		, TeacherEstimate
		, AgeOfTeacherData
	)
	SELECT #ebs.schNo, #ebs.LifeYear
		, isnull(#ebst.surveyDimensionssID, #ebs.surveyDimensionssID)
		, HousingType
		, Materials
		, HousingCondition
		, resNumber
		, null, null, null, null
		, null, null, null
		, #ebs.Estimate, #ebs.Offset, #ebst.Estimate, #ebst.Offset
	FROM #ebs
		INNER JOIN vtblTeacherHousing
		ON #ebs.bestssID = vtblTeacherHousing.ssID
		LEFT JOIN #ebst
			ON #ebs.schNo = #ebst.schNo
			AND #ebs.LifeYear = #ebst.LifeYear
	WHERE resNumber is not null
	--INSERT INTO Buildings
	--( schNo
	--, bldgCode
	--, bldgSubType
	--, bldgTitle
	--)
	INSERT INTO Buildings
	(
		schNo
		, bldgCode
		, bldgSubType
		, bldgMaterials
		, bldgTitle
	)
	Select
		schNo
		, mresBuildingType
		, mresBuildingSubType
		--, num
		, case Materials WHEN 'Permanent' then 'P' when 'Traditional' then 'T' end MatCode
		--, ROW_NUMBER() OVER (PARTITION BY schNo ORDER BY mresBuildingSubType, num) RoomNo
		, 'Teacher House ' + cast(ROW_NUMBER() OVER (PARTITION BY schNo ORDER BY mresBuildingSubType, num)  as nvarchar(3)) Title
	from #th
		INNER JOIN metaResourceDefs RD
			on #th.HousingType = RD.mresName
		INNER JOIN metaNumbers
			on metaNumbers.num between 1 and #th.NumHouses
	ORDER BY schNo, mresBuildingSubType, num
	END
GO

