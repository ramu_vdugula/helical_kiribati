SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 04 2010
-- Description:	Apply 5 part names to TeacherIdentity table
-- =============================================
CREATE PROCEDURE [pTeacherOps].[Apply5PartName]
	-- Add the parameters for the stored procedure here
	@TeacherID int = null,
	@SurveysToo int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET ANSI_NULLS ON;
	SET ANSI_WARNINGS OFF;

begin try

	begin transaction

	UPDATE TeacherIdentity
	SET tNamePrefix = P.Honorific
		, tGiven = P.FirstName
		, tMiddleNames = P.MiddleName
		, tSurname = P.LastName
		, tNameSuffix = P.Suffix
	FROM TeacherIdentity
		CROSS APPLY dbo.fnParseName(tGiven + ' ' + tSurname) P
	WHERE tNamePrefix is null and tMiddleNames is null and tNameSuffix is null
		and charindex(' ',tGiven+tSurname) > 0
		and (tID = @TeacherID or @TeacherID is null)

	if (@SurveysToo = 1) begin
		UPDATE TeacherSurvey
		SET tchNamePrefix = P.Honorific
			, tchFirstname = P.FirstName
			, tchMiddleNames = P.MiddleName
			, tchFamilyName = P.LastName
			, tchNameSuffix = P.Suffix
		FROM TeacherSurvey
			CROSS APPLY dbo.fnParseName(tchFirstname + ' ' + tchFamilyName) P
		WHERE tchNamePrefix is null and tchMiddleNames is null and tchNameSuffix is null
			and charindex(' ',tchFirstname+tchFamilyName) > 0
			and (tID = @TeacherID or @TeacherID is null)
	end

	commit transaction

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

END
GO

