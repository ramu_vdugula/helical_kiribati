SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 02 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[xmlDisabilityVillage]
	-- Add the parameters for the stored procedure here
	@schNo nvarchar(50) = '',
	@SurveyYear int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


declare @xmlResult xml

set @xmlResult =
(
Select
1 as Tag
, Null as parent
, disVillage [NonAttender!1!village]
, disage [NonAttender!1!age]
, disGender [NonAttender!1!gender]
, disCode [NonAttender!1!disabilityCode]

from

	DisabilityVillage DV
		inner join SchoolSurvey S
			on DV.ssId = S.ssID

WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar
ORDER BY

	disID

for xml explicit
)

Select @xmlResult =
(
Select @xmlResult
for XML PAth('DisabilityNonAttenders')
)

select @xmlResult

END
GO
GRANT EXECUTE ON [dbo].[xmlDisabilityVillage] TO [pEnrolmentReadX] AS [dbo]
GO

