SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 1 2008
-- Description:	update primary classes from xml
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[updateClasses]
	-- Add the parameters for the stored procedure here
	@SurveyID int ,
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

-- delete existing
-- the xml may pass in classType, minSeq and maxSeq
-- defining the range of items in scope. this should be used for deleting
--- the preexisitng - ie we only delete what is being replaced.

	declare @classType nvarchar(1)
	declare @minSeq int
	declare @maxSeq int

	select @classType = classType,
			@minSeq = minSeq,
			@maxSeq = maxSeq
	FROM
		OPENXML (@idoc, '/Classes',2)
		WITH (
				classType nvarchar(1) '@classType',
				minSeq int '@minSeq',
				maxSeq int '@maxSeq'
				)

-- start a transaction here, before the deletes

begin transaction

-- try block to prepare for rollback

begin try

	if (@classType) is null
		-- not passed by VEMIS @ school
		-- also not used by Classes form 2010 format
		begin
			DELETE from ClassTeacher WHERE pcId in
			(select pcID from Classes WHERE ssID = @surveyID)

			DELETE from Classes WHERE ssID = @surveyID			-- cascade does the rest
		end
	else
		-- as passed in from classes form 2007-2009
		begin
			DELETE from ClassTeacher WHERE pcId in
			(select pcID from Classes
				WHERE ssID = @surveyID
					AND pcCat = @classType
					AND pcSeq between @minSeq and @maxSeq)

			DELETE from Classes WHERE
				ssID = @surveyID			-- cascade does the rest
					AND pcCat = @classType
					AND pcSeq between @minSeq and @maxSeq
		end

	   INSERT INTO Classes(ssID, pcSeq, pcCat
			, pcHrsWeek, pcLang, subjCode, shiftCode, rmID)	-- new fields for 2010
			-- 5 11 2011 added shift for puntland

		Select @SurveyID, seq, classType
				, weeklyHours, langCode, subjCode, shiftCode, roomID
			from OPENXML (@idoc, '/Classes/Class',2)
			WITH (
					seq int '@seq'
					, classType nvarchar(1) '../@classType'
					, weeklyHours float '@weeklyHours'
					, langCode nvarchar(5) '@langCode'
					, subjCode nvarchar(10) '@subjectCode'
					, shiftCode nvarchar(10) '@shiftCode'
					, roomID int '@roomID'
					)


	-- insert the level info

		INSERT INTO ClassLevel
		(
			pcID, pclLevel, pclNum, pclM, pclF
		)
		Select pcID, levelCode, enrol
				, enrolM, enrolF
		FROM
		Classes,
		OPENXML (@idoc, '/Classes/Class/Levels/Level',2)
			WITH (
					seq int '../../@seq',
					levelCode nvarchar(10) '@levelCode',
					enrol int '@enrol'
					, enrolM int '@enrolM'
					, enrolF int '@enrolF'
					) PCL
		WHERE Classes.pcSeq = PCL.seq
		AND ssID = @surveyID


-- insert the teacher info
-- the challengeis to find the tchsId created when the teacher with this tag was loaded
-- tag here is the primary key tsmisID from the SMIS system
-- as of 2009 we save the tID on this record, not the tchsID
-- 2 formats for now...

-- Pineapples@School sends the Tag, the Id from its own table


	INSERT INTO ClassTeacher
	(pcID , tchsID, pctSeq)
	SELECT pcID, TS.tchsID, TeacherSeq
	FROM Classes PC, TeacherSurvey TS,
	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				smisID nvarchar(10) '@Tag',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq'
				) XMLT
	WHERE PC.pcSeq = xmlt.seq
	AND xmlt.smisID = TS.tchsmisID
	AND TS.ssID = @surveyID
	AND PC.ssID = @surveyID

-- 6 7 2013	for somalia
UPDATE TeacherSurvey
	SEt tchSubjectMajor = Major
	,  tchSubjectMinor = Minor
FROM TeacherSurvey
INNER JOIN
	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				smisID nvarchar(10) '@Tag',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq',
				Major nvarchar(10) '@Major',
				Minor nvarchar(10) '@Minor'
				) XMLT
	ON TeacherSurvey.tId = XMLT.tID
	AND TeacherSurvey.ssID = @SurveyID
WHERE major is not null or minor is not null

------------ pineapples Classes Format sends ID on the xml, which is a real tchsID
------------ pineapples itself supplies the tid, not the smisid .
------------ since these are mutually exclusive , we can do the inserts
------------ based on tid too
----------	INSERT INTO ClassTeacher
----------	(pcID , tID, pctSeq)
----------	SELECT pcID, xmlt.ID, TeacherSeq
----------	FROM Classes PC,
----------	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
----------		WITH (
----------				seq int '../../@seq',
----------				ID int '@tID',
----------				TeacherSeq int '@TeacherSeq'
----------				) XMLT
----------	WHERE PC.pcSeq = xmlt.seq
----------	AND PC.ssID = @surveyID

select * from
OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq'
				) XMLT


	INSERT INTO ClassTeacher
	(pcID , tchsID, tID, pctSeq)
	SELECT pcID, null,  xmlt.tID, TeacherSeq
	FROM Classes PC,
	OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
		WITH (
				seq int '../../@seq',
				tID int '@tID',
				TeacherSeq int '@TeacherSeq'
				) XMLT
	WHERE PC.pcSeq = xmlt.seq
	AND PC.ssID = @surveyID
print 'after insert'
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


-- if called from a survey form, and we already have category and sequence that we do not want to change, exit here
if not (@classType is null)
	return 0


-- this section is for the data arriving from SMIS
-- it classifies according to whether the school is S, J, or C
-- and renumbers in ascending sequence
-- this part is not used when this sp is called from a survey form
-- there are too many issues with shifting items betwen the various tabs on the form
UPDATE Classes
	SET pcCat = 'C'
WHERE ssID = @SurveyID
AND pcID in (Select pcID from ClassLevel GROUP BY pcID having count(pcID) > 1)

UPDATE Classes
	SET pcCat = 'J'
WHERE ssID = @SurveyID
AND pcCat is null
AND pcID in (Select pcID from PrimaryClassTeacher GROUP BY pcID having count(pcID) > 1)

-- single classes
UPDATE Classes
	SET pcCat = 'S'
WHERE ssID = @SurveyID
AND pcCat is null

update Classes SET pcSeq = null WHERE ssId = @surveyID

declare @i int
select @i = 0

declare @pcID int
select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE Classes
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from Classes WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from Classes WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE Classes
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from Classes WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from Classes WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE Classes
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from Classes WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null

	end
END
GO

