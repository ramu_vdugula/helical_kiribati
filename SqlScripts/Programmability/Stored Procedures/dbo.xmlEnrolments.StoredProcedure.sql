SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-02-07
-- Description:	Read Enrolments as XML
-- =============================================
CREATE PROCEDURE [dbo].[xmlEnrolments]
	-- Add the parameters for the stored procedure here

	@SchNo nvarchar(50) ,
	@SurveyYear int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


Select E.*
into #e
from Enrollments E
		inner join SchoolSurvey S
			on E.ssId = S.ssID
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar

declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, TableName as [Grid!1!Tag]
, null as [values!2!ID!Hide]
, rowTag as [row!3!Tag]
, colTag as [col!4!Tag]
, null as [data!5!ID!Hide]
, M as [data!5!M]
, F as [data!5!F]

from

(Select
	1 as Tag
	, null as Parent
	, 'Enrolment' as TableName
	, null as rowTag
	, null as colTag
	, null as M
	, null as F

UNION ALL
Select
	2 as Tag
	, 1 as Parent
	, 'Enrolment' as TableName
	, null as rowTag
	, null as colTag
	, null as M
	, null as F

UNION ALL
Select DISTINCT
	3 as Tag
	, 2 as Parent
	, 'Enrolment' as TableName
	, enAge as rowTag
	, null as colTag
	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	4 as Tag
	, 3 as Parent
	, 'Enrolment' as TableName
	, enAge as rowTag
	, enLevel as colTag
	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	5 as Tag
	, 4 as Parent
	, 'Enrolment' as TableName
	, enAge as rowTag
	, enLevel as colTag
	, enM as M
	, enF as F
from #e
) u
ORDER BY


 [Grid!1!Tag]
, [values!2!ID!Hide]
, [row!3!Tag]
, [col!4!Tag]

for xml explicit
)

select @xmlResult
drop table #e
END
GO
GRANT EXECUTE ON [dbo].[xmlEnrolments] TO [pEnrolmentRead] AS [dbo]
GO

