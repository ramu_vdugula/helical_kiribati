SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 12 2007
-- Description:	Identify the version of the SQL Schema
-- =============================================
CREATE PROCEDURE [dbo].[getSchemaInfo]
	@Version nvarchar(10)= null OUTPUT,
	@VersionDate datetime = null OUTPUT
AS

BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @ver nvarchar(10)
declare @date datetime

select @ver = '2011114A'
select @date = '2011-11-14'

Select @Version = @ver,
@VersionDate = @date

Select
@Version as SchemaVersion,
@VersionDate as SchemaDate

END
GO
GRANT EXECUTE ON [dbo].[getSchemaInfo] TO [public] AS [dbo]
GO

