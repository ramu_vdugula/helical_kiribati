SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 04 2010
-- Description:	Summary of pay run, esp regarding how many Payroll Numbers are identified, how many teachers have appointments, and how many have the correct appointment
-- =============================================
CREATE PROCEDURE [pTeacherReadX].[PayrunSummary]
	-- Add the parameters for the stored procedure here
	@AsAtDate smalldatetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- find the applicable pay period for the date in question
declare @PayPeriodStart datetime

SELECT @PayPeriodStart = max(tpsPeriodStart)
FROM TeacherPayslips
WHERE tpsPeriodStart <= @AsAtDate

declare @numDups int	-- the number of payroll numbers in the payslip file that are dulicates on TeacherIdentity

SELECT @numDups = count(DupPayroll)
FROM
(
Select tpsPayroll DupPayroll
from TeacherPayslips
	LEFT JOIN TeacherIdentity TI
		ON TeacherPayslips.tpsPayroll = TI.tPayroll

WHERE tpsPeriodStart = @PayPeriodStart
GROUP BY tpsPayroll
HAVING count(tID) > 1
) subDups

-- totals across the Macthed

declare @NumPayslips int
declare @PayPeriodEnd datetime
declare @GrossPay money

declare @PayslipsMatched int
declare @GrossMatched money
declare @PayslipsNotMatched int
declare @GrossNotMatched money

declare @PayslipsAppointed int
declare @GrossAppointed money
declare @PayslipsNotAppointed int
declare @GrossNotAppointed money

SELECT @NumPayslips = count(tpsPayroll)
, @PayPeriodEnd = min(tpsPeriodEnd)
, @GrossPay = sum(tpsGross)
, @PayslipsMatched = count(tPayroll)
, @PayslipsNotMatched = sum(case when tPayroll is null then 1 else 0 end)
, @GrossMatched = sum(case when tPayroll is not null then tpsGross else null end)
, @GrossNotMatched = sum(case when tPayroll is null then tpsGross else null end)
From TeacherPayslips  TP
LEFT JOIN (Select DISTINCT tPayroll from TeacherIdentity) TI
ON TP.tpsPayroll = TI.tPayroll
WHERE tpsPeriodStart = @PayPeriodStart

-- now get get the totals for appointed/not appointed

SELECT
  @PayslipsAppointed = count(tPayroll)
, @PayslipsNotAppointed = sum(case when tPayroll is null then 1 else 0 end)
, @GrossAppointed = sum(case when tPayroll is not null then tpsGross else null end)
, @GrossNotAppointed = sum(case when tPayroll is null then tpsGross else null end)
From TeacherPayslips  TP
LEFT JOIN (
			Select DISTINCT tPayroll
			from TeacherIdentity TI2
			INNER JOIN TeacherAppointment TA
				ON TI2.tID = TA.tID
				AND TA.taDate <=@AsAtDate
				AND (TA.TAEndDate is null or TA.taEndDate >= @AsAtDate)
			) TI
			ON TP.tpsPayroll = TI.tPayroll
WHERE tpsPeriodStart = @PayPeriodStart

SELECT  @NumPayslips NumPayslips		-- DISTINCT, becuase if there is a duplicate payroll no, we'llget 2 copies of the paysluip
	, @NumDups NumDuplicateMatches
	, @PayPEriodStart PayPeriodStart

	, @PayPeriodEnd PayPeriodEnd	-- min saves a "GROUP BY"
	, @GrossPay GrossPay

	, @PayslipsNotMatched PayslipsNotMatched
	, @PayslipsNotAppointed  PayslipsNotAppointed
	, @GrossNotMatched GrossNotMatched
	, @GrossNotAppointed GrossNotAppointed

	, @PayslipsMatched PayslipsMatched
	, @PayslipsAppointed PayslipsAppointed
	, @GrossMatched GrossMatched
	, @GrossAppointed GrossAppointed


--SELECT  count(DISTINCT tpsID) NumPayslips		-- DISTINCT, becuase if there is a duplicate payroll no, we'llget 2 copies of the paysluip
--	, @NumDups NumDuplicateMatches
--	, @PayPEriodStart PayPeriodStart

--	, min(tpsPeriodEnd) PayPeriodEnd	-- min saves a "GROUP BY"
--	, sum(tpsGross) GrossPay

--	, sum(case when TI.tID is null then 1 else 0 end) PayslipsNotMatched
--	, sum(case when TI.tID is not null and TA.taID is null then 1 else 0 end)  PayslipsNotAppointed
--	, sum(case when TI.tID is null then tpsGross else 0 end) GrossNotMatched
--	, sum(case when TI.tID is not null and TA.taID is null then tpsGross else 0 end) GrossNotAppointed

--	, count(DISTINCT case when TI.tID is not null then tpsPayroll else null end) PayslipsMatched
--	, sum(case when TA.taID is not null then 1 else 0 end) PayslipsAppointed
--	, sum(case when TI.tID is not null then tpsGross else 0 end) GrossMatched
--	, sum(case when TA.taID is not null then tpsGross else 0 end) GrossAppointed

--from TeacherPayslips
--	LEFT JOIN TeacherIdentity TI
--		ON TeacherPayslips.tpsPayroll = TI.tPayroll
--	LEFT JOIN TeacherAppointment TA
--		ON TI.tID = TA.tID
--		AND TA.taDate <=@AsAtDate
--		AND (TA.TAEndDate is null or TA.taEndDate >= @AsAtDate)
--WHERE tpsPeriodStart = @PayPeriodStart


END
GO

