SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[updateGrid]
	-- Add the parameters for the stored procedure here
	@surveyID int,
	@tableDef nvarchar(10),
	@page nvarchar(50),
	@grid xml
AS
BEGIN

/*

	 as of 7 10 2009, the table codes map to data Codes in metaPupilTableDefs, so
	that more than one type of table can write to the same ptCode in Pupil Tables
	For legacy applications, this will work the same (since there will be in metaPupilTableDEfs a generic record for each ptCode)

-- and as of 31 8 2011 , the table def code is written as well, so we know which tabledef
-- generated the data. makes it possible to track back the right
-- table formation in the Survey SQL

*/

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @grdtbl table(
							Page nvarchar(50) ,
							YearLevel nvarchar(10) ,
							Row nvarchar(20),
							Col nvarchar(20),
							seq int,
							M int,
							F int)
	-- this is the set of column tags for the grid
	declare @colSet table( col nvarchar(20)
							)
	-- this is the set of row tags for the grid
	declare @rowSet table( row nvarchar(20)
							)

	declare @codeType nvarchar(10)			-- the ptCode assocaited to this table
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @grid

-- if the table code is not supplied as an argument, get it from the xml
-- or if it uses the default, it will come from e.g. pineapples@school it eill be TableCode
-- ie there is a default layout definition for each data code
	if (@tableDef is null)
		Select @tableDef = isnull(TableDef, TableCode)
			from OPENXML (@idoc, '/Grid',2)
			WITH (
				TableDef nvarchar(20) '@TableDef'
				,TableCode nvarchar(20) '@TableCode'
				)


-- populate the table of grid values

	INSERT @grdtbl
	Select * from OPENXML (@idoc, '/Grid/values/row/col/data',2)
		WITH (

				Page nvarchar(50) '../../../@Tag',
				YearLevel nvarchar(10) '../../../@yearLevel',
				Row nvarchar(20) '../../@Tag',
				Col nvarchar(20) '../@Tag',
				seq int '../../@seq',
				M int '@M',
				F int '@F')
-- get the complete set of columns
	INSERT @colSet
	Select col from OPENXML(@idoc, '/Grid/colSet/col',2)
		WITH (
				col nvarchar(20) '@Tag'
			)

-- get the complete set of rows
	INSERT @rowSet
	Select row from OPENXML(@idoc, '/Grid/rowSet/row',2)
		WITH (
				row nvarchar(20) '@Tag'
			)


/*
	-- update the records that match
	UPDATE PupilTables
		set ptM = G.M,
			ptF = G.F
	from PupilTables P inner join @grdtbl G
		on P.ptRow = G.Row and P.ptCol = G.Col

	WHERE P.ssID = @surveyID
			and P.ptCode = @codeType

	INSERT INTO PupilTables(ssID, ptCode, ptRow, ptCol, ptM, ptF)
	SELECT @surveyID, @codeType, G.Row, G.Col, G.M,G.F
	FROM @grdtbl G left join (Select * from PupilTables
	WHERE ssID = @surveyID and ptCode = @codeType) P
	on G.Row = p.ptRow and G.col = p.ptCol
	WHERE ptID is null
    -- Insert statements for procedure here
	-- SELECT @grid
*/
declare @rowSplit int; --0 = ptAge, 1 = ptLevel, 2= ptRow
declare @colSplit int; --0 = ptAge, 1 = ptLevel, 2= ptCol

select @rowSplit =
	case
		when tdefRows is null then -1			-- to support pupiltable formats
		when tdefRows like 'AGE%' then 0
		when tdefRows like 'LEVEL%' then 1
		else 2
	end,
	@colSplit =
	case
		when tdefCols is null then 1		-- level is the default for columns
		when tdefCols like 'AGE%' then 0
		when tdefCols like 'LEVEL%' then 1
		else 2
	end,
	@codetype = tdefDataCode
	from MetaPupilTableDefs
	WHERE tdefCode = @tableDef		-- this is the table definition


declare @ReplaceRows int
declare @ReplaceCols int

Select @ReplaceRows = count(*) from @rowSet
Select @ReplaceCols = count(*) from @colSet


-- start the transaction

begin transaction

begin try

	if @ReplaceRows = 0
			IF @ReplaceCols = 0
				-- delete all rows and columns
				BEGIN
					DELETE from PupilTables
					WHERE ssID = @SurveyID
						--AND ptCode = @codeType
						AND ptTable = @TableDef
						AND (ptPage = @page or (@page is null))
				END

			ELSE
				BEGIN
				-- delete all rows, specified columns
					DELETE from PupilTables
					FROM PupilTables,  @colSet
					WHERE ssID = @SurveyID
						--AND ptCode = @codeType
						AND ptTable = @TableDef
						AND (ptPage = @page or (@page is null))
						AND (( @colsplit = 0 and ptAge = col) OR
							 (@colsplit = 1 and ptLevel = col)  OR
							 (@colsplit = 2 and ptCol = Col)
							)
				END
	else
			IF @ReplaceCols = 0
				BEGIN
					-- delete all columns, specifed rows
					DELETE from PupilTables
					FROM PupilTables, @rowSet
					WHERE ssID = @SurveyID
						--AND ptCode = @codeType
						AND ptTable = @TableDef
						AND (ptPage = @page or (@page is null))
						AND (	(@rowsplit = -1) OR
								( @rowsplit = 0 and ptAge = row) OR
							 (@rowsplit = 1 and ptLevel = row)  OR
							 (@rowsplit = 2 and ptRow = row)
							)
				END
			ELSE
				BEGIN
					-- delete only specified rows and columns
					DELETE from PupilTables
					FROM PupilTables, @rowSet, @colSet
					WHERE ssID = @SurveyID
						--AND ptCode = @codeType
						AND ptTable = @TableDef
						AND (ptPage = @page or (@page is null))
						AND (	(@rowsplit = -1) OR
								( @rowsplit = 0 and ptAge = row) OR
							 (@rowsplit = 1 and ptLevel = row)  OR
							 (@rowsplit = 2 and ptRow = row)
							)
						AND (( @colsplit = 0 and ptAge = col) OR
							 (@colsplit = 1 and ptLevel = col)  OR
							 (@colsplit = 2 and ptCol = Col)
							)
				END
	------Select count(*) from @RowSet
	------select @rowsplit
	------select * from @grdtbl

	INSERT INTO PupilTables(ssID, ptCode, ptPage, ptLevel, ptAge, ptRow, ptCol, ptM, ptF, ptTableDef)
	SELECT @surveyID,
	@codeType,
	case	when @page is null then
				case
					when G.Page is null then cast(seq as nchar(10))
					else  G.Page
				end
			else @page
	end,
	-- for level, test the plit values
	case
		when @rowsplit = 1 then G.Row
		when @colsplit = 1 then G.Col
		else YearLevel
	end,

	-- for age
	case
		when @rowsplit = 0 then G.Row
		when @colsplit = 0 then G.Col
		else null				-- to support tertiary with yearLevel tag on the values node
	end,
	--row
	case
		when @rowsplit = 2 then G.Row
		else null
	end,
	--col
	case
		when @colsplit = 2 then G.Col
		else null
	end,
	-- values
	G.M, G.F
	, @TableDef
	FROM @grdtbl G
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit
if @@trancount > 0
	commit transaction

return


END
GO
GRANT EXECUTE ON [dbo].[updateGrid] TO [pEnrolmentWrite] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[updateGrid] TO [pFinanceWrite] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[updateGrid] TO [pSchoolWrite] AS [dbo]
GO

