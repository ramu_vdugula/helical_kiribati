SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 11 2007
-- Description:	Core data for calculation of EFA 11. Used by EFA11 query.
-- =============================================
-- -1 = full details including dimensions
--  0 = usual return of data
--  1 = national summary
CREATE PROCEDURE [dbo].[sp_EFA11DataAppt]
	@Consolidation int = 0	,
	@SendAsXML int = 0,			-- only used when consolidation = 1
	@xmlout xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

if (@Consolidation = 0) begin
	Select TLA.*
	, 1 NumTeachers
	, TLA.Certified * TLA.Qualified CertandQual
	, DSS.*
	FROM pTeacherRead.TeacherListByAppointment TLA
	INNER JOIN #ebse E
		ON TLA.schNo = E.schNo
		AND TLA.svyYEar = E.LifeYear
	INNER JOIN DimensionSchoolSurveyNoYear DSS
		ON E.bestssID = DSS.[Survey ID]
end

	If (@Consolidation = 1) begin

		DECLARE @tmp TABLE
		(
			svyYear int
			, sector		nvarchar(20)
			, numTeachers        int null
			, FemaleTeachers	int null
			, MaleTeachers		int NULL
			, certified			int NULL
			, certifiedF	int NULL
			, certifiedM	int NULL
			, qualified     int NULL
			, qualifiedF	int NULL
			, qualifiedM	int NULL
			, enrol			int

		)

		INSERT INTO @tmp
		Select TLA.svyYear
		-- best school type for the year, based on available returns
		, E.bestSchType
		-- number of teachers
		, count(*)
		, sum(case when Gender = 'F' then 1 else null end)
		, sum(case when Gender = 'M' then 1 else null end)
		--- certified teachers
		, sum(Certified)
		, sum(case when Gender = 'F' then Certified else null end)
		, sum(case when Gender = 'M' then Certified else null end)
		--- qualified teachers
		, sum(Qualified)
		, sum(case when Gender = 'F' then Qualified else null end)
		, sum(case when Gender = 'M' then Qualified else null end)
		, sum(case when rn = 1 then bestEnrol else null end) enrol

		FROM
		(Select *
		, row_number() OVER (Partition by svyYear, schNo ORDER BY tID) RN
		FROM
		pTeacherREad.TeacherListByAppointment) TLA
		INNER JOIN #ebse E
			ON TLA.svyYEar = E.LifeYear
			AND TLA.schNo = E.schNo
		GROUP BY TLA.svyYear, E.bestSchType


		if @SendAsXML <> 0 begin
			declare @xml xml
			SELECT @xml =
			(
			Select [svyYear] [@year]
				, sector		[@sectorCode]
				, Enrol
				, MaleTeachers		TeachersM
				, FemaleTeachers	TeachersF
				, numTeachers		Teachers
				, qualifiedM		QualM
				, qualifiedF		QualF
				, qualified			Qual
				, CertifiedM		CertM
				, CertifiedF		CertF
				, Certified			[Cert]

				, case when isnull(MaleTeachers,0) = 0 then null
					else cast(CertifiedM as decimal(8,4)) / MaleTeachers
					end CertPercM
				, case when isnull(FemaleTeachers,0) = 0 then null
					else cast(CertifiedF as decimal(8,4)) / FemaleTeachers
					end CertPercF
				, case when isnull(numTeachers,0) = 0 then null
					else	cast(Certified as decimal(8,4)) / numTeachers
				  end CertPerc


				, case when isnull(MaleTeachers,0) = 0 then null
					else cast(QualifiedM as decimal(8,4)) / MaleTeachers
					end QualPercM
				, case when isnull(FemaleTeachers,0) = 0 then null
					else cast(QualifiedF as decimal(8,4)) / FemaleTeachers
					end QualPercF

				, case when isnull(numTeachers,0) = 0 then null
					else cast(Qualified as decimal(8,4)) / numTeachers
				  end  QualPerc

				, case when isnull(numTeachers,0) = 0 then null
					else cast(Enrol as decimal(12,4)) / numTeachers
				  end  PTR
				, case when isnull(Certified,0) = 0 then null
						else cast(Enrol as decimal(12,4)) / Certified
				  end CertPTR
				, case when isnull(Qualified,0) = 0 then null
						else cast(Enrol as decimal(12,4)) / Qualified
				  end QualPTR
			from @tmp T

			FOR XML PATH('TeacherQC')
			)
			SELECT @xmlOut =
			(
				SELECT @xml
				FOR XML PATH('TeacherQCs')
			)

			if @SendAsXML = 1
				SELECT @xmlOut
		end
	end
end
GO

