SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		brian lewis
-- Create date: 17 12 2007
-- Description:	for NIS, school survey for the best year for room data
-- =============================================
CREATE PROCEDURE [dbo].[sp_schoolYearBestSurveyAnyRooms]
	-- Add the parameters for the stored procedure here
	@SurveyYear int,
	@SchoolNo nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select *
INTO #ebsr
FROM dbo.tfnESTIMATE_BestSurveyanyRooms(@SurveyYear, @SchoolNo)
    -- Insert statements for procedure here

Select E.LifeYear,
E.schNo [School No],
E.Estimate,
E.bestssID,
E.bestYear,
S.*
from #ebsr E
INNER JOIN SchoolSurvey S
	ON E.surveyDimensionssID = s.ssID
WHERE (E.LifeYear = @SurveyYear or @SurveyYear is null)
	AND (E.schNo = @SchoolNo or @SchoolNo is null)

END
GO
GRANT EXECUTE ON [dbo].[sp_schoolYearBestSurveyAnyRooms] TO [pSchoolRead] AS [dbo]
GO

