SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 5 2013
-- Description:	REad estimated enrolments by school filter
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[EstimateEnrolmentFilter]
	-- Add the parameters for the stored procedure here
	@xmlFilter xml = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @keys TABLE
(
	schNo nvarchar(50)
	, recno int
)

DECLARE @NumMatches int

if (@xmlFilter is not null) begin
	INSERT INTO @keys
	EXEC pSchoolRead.SchoolFilterIDs @NumMatches OUT, 0,1 ,@xmlFilter = @xmlFilter
end

Select *
FROM pEnrolmentRead.EstimateEnrolment
WHERE (@xmlFilter is null
		OR
		schNo in (Select schNo from @keys)
	   )

END
GO

