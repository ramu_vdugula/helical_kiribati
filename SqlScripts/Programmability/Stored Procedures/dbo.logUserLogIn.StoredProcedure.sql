SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[logUserLogIn]
	-- Add the parameters for the stored procedure here
	@WindowsUserName nvarchar(50),
	@MachineName nvarchar(50),
	@IPAddress nvarchar(20),
	@Context nvarchar(2000) = null			-- for backward compatibility
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if not exists(Select uLogin from UserInfo WHERE ULogin = @WindowsUserName)
		INSERT INTO USERInfo
			(uLogin)
		VALUES
			(@WindowsUserName)


	Update UserInfo
		SET uActive = 1,
		uLoginMachine = @MachineName,
		uLoginIP = @IPAddress,
		uLogintime = getdate(),
		uContext = @Context
	WHERE ulogin = @WindowsUserName

	if not exists(Select computerName from ComputerInfo WHERE computerName = @MachineName)
		INSERT INTO ComputerInfo
			(computerName, computerContext, computerDateStamp)
		VALUES
			(@MachineName, @Context, getdate())

	UPDATE ComputerInfo
		SET computerContext = @context
		, computerDateStamp = getdate()

	WHERE computerName = @MachineName

	exec LogActivity 'Login', null
END
GO
GRANT EXECUTE ON [dbo].[logUserLogIn] TO [public] AS [dbo]
GO

