SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 10 2012
-- Description:	This function returns a dropdown list of province names for a pdfform
-- in CENODPF items text format.
-- The selected value is the code, so is amed for
-- validated province fields like HomeProvince
-- =============================================
CREATE FUNCTION [cenopdf].[DisabilityTypeItems]
()

RETURNS nvarchar(max)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @result nvarchar(max)

declare @dt nvarchar(max)  = '(Select...)|00|*' + char(13) + char(10)


Select @dt = @dt + codeDescription + '|' + codeCode + '|'  + char(13) + char(10)
FRom lkpDisabilities		-- provinces omits 'do not use'
ORDER BY codeSeq

RETURN @dt
END
GO

