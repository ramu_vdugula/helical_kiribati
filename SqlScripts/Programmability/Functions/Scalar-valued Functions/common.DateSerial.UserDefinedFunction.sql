SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 06 2009
-- Description:	DateSerial equivalent
-- =============================================
CREATE FUNCTION [common].[DateSerial]
(
	-- Add the parameters for the function here
	@year int
	, @month int
	, @day int
)
RETURNS datetime
AS
BEGIN

	RETURN dateadd(dd,@day -1,dateadd(mm,@month -1,DAteADD(yyyy,@year-year(0),0)))

END
GO

