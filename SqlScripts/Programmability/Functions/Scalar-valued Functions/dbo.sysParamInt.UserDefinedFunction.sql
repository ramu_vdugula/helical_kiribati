SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[sysParamInt]
(
	-- Add the parameters for the function here
	@parmName nvarchar(30)
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = paramInt from sysParams
		WHERE paramName = @parmName

	-- Return the result of the function
	RETURN @Result

END
GO
GRANT EXECUTE ON [dbo].[sysParamInt] TO [public] AS [dbo]
GO

