SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Function version of this
-- =============================================
CREATE FUNCTION [pSchoolRead].[tfnSchoolDataSetEnrolment]
(
	-- Add the parameters for the function here
	@Year int
	, @SchoolNo nvarchar(50)
	, @ClassLevel nvarchar(10)
	, @Gender nvarchar(1)
)
RETURNS
@schoolData TABLE
(
	-- Add the column definitions for the TABLE variable here
	schNo nvarchar(50)
	, dataValue float
	, Estimate int
	, Quality int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set

if @ClassLevel is null and @Gender is null 	begin
	INSERT INTO @schoolData
	Select schNo, bestEnrol, Estimate, ActualssqLevel
	FROM dbo.tfnESTIMATE_BestSurveyEnrolments()
	WHERE lifeYear = @Year
		AND (schNo = @SchoolNo or @SchoolNo is null)

	RETURN
end


INSERT INTO @schoolData
Select schNo,
	sum(case when @Gender is null then enSum
		when @Gender = 'M' then enM
		when @Gender = 'F' then enF
	end
	)
	, Estimate, ActualssqLevel
FROM dbo.tfnESTIMATE_BestSurveyEnrolments() EE
	INNER JOIN Enrollments E
		ON Ee.bestssID = E.ssID
WHERE lifeYear = @Year
		AND (schNo = @SchoolNo or @SchoolNo is null)
AND (enLevel = @ClassLevel or @ClassLevel is null)
GROUP BY schNo, Estimate, actualssqLevel

RETURN
END
GO

