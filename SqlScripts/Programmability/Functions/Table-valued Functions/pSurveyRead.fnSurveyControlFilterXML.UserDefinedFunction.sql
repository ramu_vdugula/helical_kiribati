SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 05 2010
-- Description:	takes an xm representation of the survey control filter, and returns the matching record set
-- =============================================
CREATE FUNCTION [pSurveyRead].[fnSurveyControlFilterXML]
(
	-- Add the parameters for the function here
	@SurveyControlFilter xml
)
RETURNS
@SC TABLE
(
	-- Add the column definitions for the TABLE variable here
	syID int
	, schNo nvarchar(50)
	, syYear int
	, rowNumber int
)
AS
BEGIN

	declare	@SurveyYear int
	declare @District nvarchar(10)
	declare @Island nvarchar(10)
	declare @SchoolType nvarchar(10)
	declare @Authority nvarchar(10)
	declare @FileLocation nvarchar(30)
	declare @StatusChoice int
	declare @SortField nvarchar(50)
	declare @SortDirection nvarchar(30)


	Select @SurveyYear = @SurveyControlFilter.value('./SurveyControlFilter[1]/@surveyYear','int')
	Select @District = @SurveyControlFilter.value('./SurveyControlFilter[1]/@district','nvarchar(10)')
	Select @Island = @SurveyControlFilter.value('./SurveyControlFilter[1]/@island','nvarchar(10)')
	Select @SchoolType = @SurveyControlFilter.value('./SurveyControlFilter[1]/@schoolType','nvarchar(10)')
	Select @Authority = @SurveyControlFilter.value('./SurveyControlFilter[1]/@authority','nvarchar(10)')
	Select @FileLocation = @SurveyControlFilter.value('./SurveyControlFilter[1]/@fileLocation','nvarchar(30)')
	Select @StatusChoice = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@statusChoice','int'),0)
	Select @SortField = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@sortField','nvarchar(50)'),0)
	Select @SortDirection = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@sortDirection','nvarchar(50)'),0)

INSERT INTO @SC
SELECT
	SYH.syID
	, SYH.schNo
	, SYH.syYear
	, case @SortField
		when 'schNo' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY SYH.SchNo DESC)
				else row_number() OVER (ORDER BY SYH.SchNo)
			end

		when 'schName' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY schName DESC)
				else row_number() OVER (ORDER BY schName)
			end

		when 'saSent' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saSent DESC)
				else row_number() OVER (ORDER BY saSent)
			end

		when 'saCollected' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saCollected DESC)
				else row_number() OVER (ORDER BY saCollected)
			end

		when 'saReceived' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saReceived DESC)
				else row_number() OVER (ORDER BY saReceived)
			end

		when 'saReceivedTarget' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saReceivedTarget DESC)
				else row_number() OVER (ORDER BY saReceivedTarget)
			end

		when 'saCompletedby' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saCompletedby DESC)
				else row_number() OVER (ORDER BY saCompletedby)
			end

		when 'saEntered' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saEntered DESC)
				else row_number() OVER (ORDER BY saEntered)
			end

		when 'saAudit' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saAudit DESC)
				else row_number() OVER (ORDER BY saAudit)
			end

		when 'saAuditDate' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saAuditDate DESC)
				else row_number() OVER (ORDER BY saAuditDate)
			end

		when 'saAuditBy' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saAuditBy DESC)
				else row_number() OVER (ORDER BY saAuditBy)
			end


		when 'saFileLocn' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saFileLocn DESC)
				else row_number() OVER (ORDER BY saFileLocn)
			end

		when 'syDormant' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY syDormant DESC)
				else row_number() OVER (ORDER BY syDormant)
			end

		when 'saReceivedD' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saReceivedD DESC)
				else row_number() OVER (ORDER BY saReceivedD)
			end

		when 'saReceivedDTarget' then
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY saReceivedDTarget DESC)
				else row_number() OVER (ORDER BY saReceivedDTarget)
			end
		else
			case @SortDirection
				when 'DESC' then row_number() OVER (ORDER BY SYH.SchNo DESC)
				else row_number() OVER (ORDER BY SYH.schNo)
			end

	end rowNumber
	--, row_number() over (ORDER BY) RowNumber
FROM
	Islands
		INNER JOIN Schools
			ON Islands.iCode = Schools.iCode
		INNER JOIN SchoolYearHistory SYH
			ON Schools.schNo = SYH.schNo
		LEFT JOIN Survey
			ON Survey.svyYear = SYH.syYear
		LEFT JOIN
			SchoolSurvey
			ON (SYH.schNo = SchoolSurvey.schNo) AND (SYH.syYear = SchoolSurvey.svyYear)
		LEFT JOIN pSurveyRead.ssIDQuality QUAL
			ON SchoolSurvey.ssID = QUAL.ssID
			AND @StatusChoice in (7,8)

WHERE
		(syYear = @SurveyYear or @SurveyYear is null)
	and (iGroup = @District or @District is null)
	and (Schools.iCode = @Island or @Island is null)
	and (schType = @SchoolType or @SchoolType is null)
	and (schAuth = @Authority or @Authority is null)
	and (saFileLocn = @FileLocation or @FileLocation is null)

	and (
			(@StatusChoice <= 0)
		or	(@StatusChoice = 1 and saSent is null )
		or	(@StatusChoice = 2 and saReceived is null )
		or	(@StatusChoice = 3 and saEntered is null )
		or	(@StatusChoice = 4 and saAudit = 1  )
		or	(@StatusChoice = 5 and saAudit = 1  and saAuditDate is null )
		or	(@StatusChoice = 6 and saAuditDate is not null )
		or	(@StatusChoice = 7 and saAuditResult = 'F' )
		or	(@StatusChoice = 8 and MaxLevel in (1,2) )
		or	(@StatusChoice = 9 and MaxLevel > 0 )
		or	(@StatusChoice = 10 and saSent is not null )
		or	(@StatusChoice = 11 and saReceived is not null )
		or	(@StatusChoice = 12 and saEntered is not null )
		or	(@StatusChoice = 13 and saEntered is null and SchoolSurvey.ssID is not null )
		or	(@StatusChoice = 14 and syDormant = 1 )

		)
	RETURN
END
GO

