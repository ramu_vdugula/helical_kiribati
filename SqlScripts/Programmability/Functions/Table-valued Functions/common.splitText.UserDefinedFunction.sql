SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 11 2009
-- Description:	Split text separated by a delimiter
-- =============================================
CREATE FUNCTION [common].[splitText]
(
	-- Add the parameters for the function here
	@textIn nvarchar(4000),
	@delim nvarchar(5)
)
RETURNS
@splits TABLE
(
	-- Add the column definitions for the TABLE variable here
	split nvarchar(4000)
	, rownum	int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	declare @out nvarchar(4000)
	declare @str nvarchar(4000)
	declare @i int
	declare @rowNum int

	if @textIn is null
		return

	select @out = @textIn
	select @rowNum = 0

	while len(@out) > 0 begin
		select @i = charindex(@delim, @out)

		if @i  > 0 begin
				select  @str = ltrim(rtrim(left(@out, @i-1)))

				INSERT INTO @splits
				Select @str, @rowNum

				Select @out = substring(@out,@i+1, len(@out) - @i + 1)
				Select @rowNum = @RowNum + 1
			end
		else
			if len(@out) > 0 begin
				INSERT INTO @splits
				Select ltrim(rtrim(@out)), @rowNum

				Select @out = ''
			end


	end

	RETURN
END
GO

