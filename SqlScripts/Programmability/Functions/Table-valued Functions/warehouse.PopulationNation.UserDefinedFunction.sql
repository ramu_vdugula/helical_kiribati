SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	Aggregated population at national level
-- for an optional model code
-- Uses the popmodDefault Flag to get the default
-- =============================================
CREATE FUNCTION [warehouse].[PopulationNation]
(
	@modelCode nvarchar(10) = null
)

RETURNS TABLE
AS
	RETURN
	select popYear, popAge
	, sum(popM) popM
	, sum(popF) popF
	, sum(popSum) pop
	From Population PP
		INNER JOIN PopulationModel PM
	ON PP.popModCode = PP.popmodCode
		WHERE (
		PM.popmodCode = @modelCode OR
			(
				PM.popModDefault = 1
				AND @modelCode is null
			)
		)
	GROUP By popYear, popAge
GO

