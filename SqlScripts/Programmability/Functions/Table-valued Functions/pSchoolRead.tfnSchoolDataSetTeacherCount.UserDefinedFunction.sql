SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 6 2010
-- Description:	Function version of this
-- =============================================
CREATE FUNCTION [pSchoolRead].[tfnSchoolDataSetTeacherCount]
(
	-- Add the parameters for the function here
	@Year int
	, @SchoolNo nvarchar(50)
	, @GovtOther nvarchar(10) = null
)
RETURNS
@schoolData TABLE
(
	-- Add the column definitions for the TABLE variable here
	schNo nvarchar(50)
	, dataValue float
	, Estimate int
	, Quality int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	INSERT INTO @schoolData

	Select schNo, count(DISTINCT TI.tID), Estimate, ActualssqLevel
	FROM dbo.tfnESTIMATE_BestSurveyTeachers(@Year, DEFAULT) EE
		INNER JOIN TeacherSurvey TS
			ON EE.bestssID = TS.ssID
		INNER JOIN TeacherIdentity TI
			ON TS.tID = TI.tID

	WHERE lifeYear = @Year
		AND (SchNo = @SchoolNo or @SchoolNo is null)
		AND ((TI.tPayroll is not null and @GovtOther = 'Y')
				or (TI.tPayroll is null and @GovtOther = 'N')
				or @GovtOther is null
			)
	GROUP BY SchNo, Estimate, ActualssqLevel

	RETURN
END
GO

