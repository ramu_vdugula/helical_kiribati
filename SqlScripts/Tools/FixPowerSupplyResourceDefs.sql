/*
Adds to metaResourecDefs for power supply, water suply and handwashing as defined in census workbooks
*/ 
begin transaction

---- IMPORTANT Change @commitMode to 1 to commmit the work
declare @commitMode int = 0
if @commitMode = 0
	select 'ROLLBACK mode'
else
	select 'COMMIT mode'

select 'Database is ' + DB_NAME() + ' on ' + @@SERVERNAME

--- Power Supply
declare @cat nvarchar(50) = 'Power Supply'

Select *
from metaResourceDefs
WHERE mresCat = @cat

DELETE
from metaResourceDefs
WHERE mresCat = @cat

INSERT INTO metaResourceDefs
(mresCat, mresSeq, mresName
, mresPromptAdq, mresPromptAvail
, mresPromptNum, mresPromptCondition, mresPromptQty
)
VALUES
('Power Supply', 110, 'Public Utility'
, 0, 1
,1,1,0
),
('Power Supply', 120, 'Solar'
, 0, 1
,1,1,0
),
('Power Supply', 130, 'Generator'
, 0, 1
,1,1,0
),
('Power Supply', 990, 'None'
, 0, 0
, 0,0,0
)

Select *
from metaResourceDefs
WHERE mresCat = @cat

--- water supply
select @cat  = 'Water Supply'

Select *
from metaResourceDefs
WHERE mresCat = @cat

DELETE
from metaResourceDefs
WHERE mresCat = @cat

if db_name() = 'FEDEMIS' begin

	INSERT INTO metaResourceDefs
	(mresCat, mresSeq, mresName
	, mresPromptAdq, mresPromptAvail
	, mresPromptNum, mresPromptCondition, mresPromptQty
	)
	VALUES
	('Water Supply', 110, 'Piped water supply'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 120, 'Protected well/spring'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 130, 'Unprotected well/spring'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 140, 'Rainwater'
	, 0, 0
	, 0,0,0
	),
	('Water Supply', 150, 'Packaged/bottled water'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 160, 'Tanker-truck or cart'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 200, 'Surface water (lake, river, stream)'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 990, 'No water source'
	, 0, 0
	, 0,0,0
	)

end
if db_name() = 'MIEMIS' begin

	INSERT INTO metaResourceDefs
	(mresCat, mresSeq, mresName
	, mresPromptAdq, mresPromptAvail
	, mresPromptNum, mresPromptCondition, mresPromptQty
	)
	VALUES
	('Water Supply', 110, 'Piped from protected source'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 120, 'Piped from unprotected source'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 130, 'Protected well/spring'
	, 0, 1
	,0,0,0
	),
	('Water Supply', 140, 'Unprotected well/spring'
	, 0, 0
	, 0,0,0
	),
	('Water Supply', 150, 'Protected rainwater'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 160, 'Unprotected rainwater'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 200, 'Bottled water'
	, 0, 1
	, 0,0,0
	),
	('Water Supply', 990, 'No water source'
	, 0, 0
	, 0,0,0
	)

end

Select *
from metaResourceDefs
WHERE mresCat = @cat



-- Handwashing
select @cat  = 'Handwashing'

Select *
from metaResourceDefs
WHERE mresCat = @cat

DELETE
from metaResourceDefs
WHERE mresCat = @cat

INSERT INTO metaResourceDefs
(mresCat, mresSeq, mresName
, mresPromptAdq, mresPromptAvail
, mresPromptNum, mresPromptCondition, mresPromptQty
)
VALUES
('Handwashing', 110, 'Water and Soap'
, 0, 1
,0,0,0
),
('Handwashing', 120, 'Water only'
, 0, 1
,0,0,0
),
('Handwashing', 130, 'Soap only'
, 0, 1
,0,0,0
),
('Handwashing', 990, 'Neither Water nor Soap'
, 0, 0
, 0,0,0
)

Select *
from metaResourceDefs
WHERE mresCat = @cat


if @commitMode = 0 begin
	rollback
	Select 'These changes have been rolled back - change @CommitMode to commit'
	print 'These changes have been rolled back - change @CommitMode to commit'
end
if @commitMode = 1 begin
	commit
	Select 'Changes commited'
	print 'Changes commited'

end
