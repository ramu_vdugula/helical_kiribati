/*
move nationality codes from census workbooks to existing 
table lkpNationality.
*/
begin transaction

---- IMPORTANT Change @commitMode to 1 to commmit the work
declare @commitMode int = 0
if @commitMode = 0
	select 'ROLLBACK mode'
else
	select 'COMMIT mode'
select 'Database is ' + DB_NAME() + ' on ' + @@SERVERNAME

Select tchCitizenship, count(*)
from TeacherSurvey
GROUP BY tchCitizenship


Select 'BEFORE: lkNationality'
Select * from lkpNationality

declare @fsmSeq int = case db_name() when 'fedemis' then 0 else 100 end
declare @rmiSeq int = case db_name() when 'miemis' then 0 else 100 end
DELETE from lkpNationality
INSERT INTO lkpNationality
(codeCode, codeDescription, codeSeq)
VALUES 
('FM', 'FSM', @fsmSeq)
,('MH', 'Marshall Islands',@rmiSeq)
,('KI', 'Kiribati',100)
, ('PW', 'Palau',100)
, ('SB', 'Solomon Islands', 100)
, ('FJ', 'Fiji', 100)
, ('PG', 'Papua New Guinea', 100)
, ('XPI', 'Other Pacific Islands', 190)

, ('US', 'USA',200)
, ('AU', 'Australia',210)
, ('NZ', 'New Zealand',220)
, ('CA', 'Canada',230)
, ('UK', 'United Kingdom',240)

, ('JP', 'Japan', 300)
, ('TW', 'Taiwan', 300)
, ('CN', 'China', 300)
, ('PH', 'Philippines',300)

, ('RU', 'Russia',400)

, ('XEU', 'Other - Europe', 910)
, ('XAS', 'Other - Asia', 910)
, ('XLT', 'Other - South America', 910)
, ('XXX','Other', 990)

Select 'AFTER: lkpNationality'
Select * from lkpNationality
ORDER BY codeSeq, codeDescription

Select 'BEFORE: TeacherSurvey / tchCitizenship'
Select tchCitizenship, count(*)
from TeacherSurvey
GROUP BY tchCitizenship

Select 'AFTER: TeacherSurvey / tchCitizenship'
UPDATE TeacherSurvey
	SET tchCitizenship = codeCode
FROM TeacherSurvey
INNER JOIN lkpNationality
ON tchCitizenship = codeDescription

UPDATE TeacherSurvey
	SET tchCitizenship = 
	case tchCitizenship
		when 'gilberts' then 'KI'
		when 'Phillipines' then 'PH'
		when 'Philippine' then 'PH'
		when 'Marshall Island' then 'MH'
		when 'Solomon Island' then 'SB'
		when 'U.S' then 'US'
		when 'United States' then 'US'
		when 'PNG' then 'PG'
		when 'Colombian' then 'XLT'
		else tchCitizenship end

Select tchCitizenship, codeDescription, count(*)
from TeacherSurvey 
	LEFT JOIN lkpNationality N
		ON tchCitizenship = N.codeCode
GROUP BY tchCitizenship, codeDescription

if @commitMode = 0 begin
	rollback
	Select 'These changes have been rolled back - change @CommitMode to commit'
	print 'These changes have been rolled back - change @CommitMode to commit'
end
if @commitMode = 1 begin
	commit
	Select 'Changes committed'
	print 'Changes committed'

end

