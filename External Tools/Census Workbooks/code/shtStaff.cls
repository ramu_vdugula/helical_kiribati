' shtStaff
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Private Sub Worksheet_Change(ByVal Target As Range)

Dim dv As Validation

Set dv = Target.Validation
On Error Resume Next
Dim f As String
f = dv.Formula1
If Err Then Exit Sub
If f = "X,x" Then
    Application.EnableEvents = False
    Target.Value = UCase(Target.Value)
End If
Application.EnableEvents = True
End Sub
