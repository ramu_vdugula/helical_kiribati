' shtRollover
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit

Private Sub Worksheet_Activate()
' this is to make sure the list of workbooks is up to date
' this is better than having a volatile function
Application.CalculateFull
End Sub
