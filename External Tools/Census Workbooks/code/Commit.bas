' Commit
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit
'
' comments about workbook versions can be inserted here to get them into the repo as plain text
'
' 2018 10 18 Added to repo


Sub CommitWorkbook()


shtSchools.Activate

DeleteAllRows

shtStudents.Activate
DeleteAllRows

shtStaff.Activate
DeleteAllRows

shtWash.Activate
DeleteAllRows


ExportCode
StructureSummary

End Sub



Sub ExportCode()
    
    Dim objMyProj As VBProject
    Dim objVBComp As VBComponent
    
    Set objMyProj = Application.VBE.ActiveVBProject
    
    Dim path As String
    Dim ext As String
    Dim s As New Scripting.FileSystemObject
    path = s.BuildPath(s.GetParentFolderName(ThisWorkbook.FullName), "code")
    If Not s.FolderExists(path) Then
        s.CreateFolder path
    Else
        Dim fldr As Scripting.Folder
        Set fldr = s.GetFolder(path)
        Dim f As Scripting.File
        For Each f In fldr.Files
            f.Delete
        Next
    End If

    Dim dt As Variant
    dt = Range("versiondate")
    Dim msg As String
    msg = Range("versionmsg")
    

    For Each objVBComp In objMyProj.VBComponents
        If objVBComp.CodeModule.CountOfLines > 1 Then
            Select Case objVBComp.Type
                Case vbext_ct_StdModule:
                    ext = ".bas"
                Case vbext_ct_ClassModule:
                    ext = ".cls"
                Case vbext_ct_Document:
                    ext = ".cls"
                    
            End Select
            If ext <> "" Then
                Dim fname As String
                fname = s.BuildPath(path, objVBComp.name & ext)
                'objVBComp.Export fname
                Dim tx As Scripting.TextStream
                Set tx = s.OpenTextFile(fname, ForWriting, True)
                Dim l As String
                Dim cm As CodeModule
                Set cm = objVBComp.CodeModule
                tx.WriteLine "' " & objVBComp.name
                tx.WriteLine "' Version date: " & dt
                tx.WriteLine "' Version msg: " & msg
                tx.WriteLine
                Dim idx As Integer
                idx = 1
                l = cm.Lines(idx, 1)
                If l = "' " & objVBComp.name Then
                    idx = 2
                    l = cm.Lines(idx, 1)
                    If Left(l, 9) = "' Version" Then
                        idx = 3
                        l = cm.Lines(idx, 1)
                        If Left(l, 9) = "' Version" Then
                            idx = 4
                        End If
                    End If
                End If
                tx.Write cm.Lines(idx, cm.CountOfLines)
                tx.WriteLine
                tx.Close
            End If
        End If
    Next
        
End Sub

Sub StructureSummary()
    Dim path As String
    Dim ext As String
    Dim s As New Scripting.FileSystemObject
    path = s.GetParentFolderName(ThisWorkbook.FullName)
    
    
    Dim fname As String
    fname = s.BuildPath(path, "TableStructure.txt")
                'objVBComp.Export fname
    Dim tx As Scripting.TextStream
    Set tx = s.OpenTextFile(fname, ForWriting, True)
    
    Dim sh As Worksheet
    Dim lo As ListObject
    Dim n As name
    Dim str As String
    
    tx.WriteLine "Sheets"
    tx.WriteLine "------"
    tx.WriteLine
    
    For Each sh In ThisWorkbook.Worksheets
    str = Left(sh.name + String(40, " "), 40)
        tx.WriteLine str & sh.CodeName
    Next
    tx.WriteLine
    
    For Each sh In ThisWorkbook.Worksheets
        For Each lo In sh.ListObjects
            DumpTableStructure tx, lo
        Next

    Next
    tx.WriteLine
    
    '
    ' now dump all range names and theor definitions
    '
    tx.WriteLine "Workbook Defined Names"
    tx.WriteLine "----------------------"
    tx.WriteLine
    For Each n In ThisWorkbook.Names
        str = Left(n.name & String(40, " "), 40)
        tx.WriteLine str & n.RefersTo
    Next
    tx.WriteLine
    tx.Close
End Sub

Private Sub DumpTableStructure(tx As TextStream, lo As ListObject)
tx.WriteLine
Dim str As String
str = "ListObject: " & lo.name & ":" & lo.Parent.name
tx.WriteLine str
tx.WriteLine String(Len(str), "-")
tx.WriteLine
Dim lc As ListColumn
For Each lc In lo.ListColumns
    tx.WriteLine lc.name
Next

End Sub

