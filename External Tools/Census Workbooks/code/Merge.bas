' Merge
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit
Option Compare Text

'*******************************************************************************
' Merge bWorkbooks
'*******************************************************************************
'

'' imports all the rows of one listobject into another
'' typically this imports To theis workbook from an external

Sub MergeListObject(dest As ListObject, src As ListObject)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection

Dim lr As ListRow
Set lr = dest.ListRows.Add()
Dim row As Range
Dim r As Range

Set row = lr.Range
dest.Parent.Activate
DoEvents
Application.Calculation = xlCalculationManual
Application.ScreenUpdating = False
Application.StatusBar = "Importing " & src.ListRows.Count & " rows..."
On Error GoTo ErrorHandler
On Error GoTo ErrorHandler
'' now we can clear out anaything on the sheet below our desired range
Set r = row.Offset(1, 0).Resize(src.ListRows.Count)
r.Clear
For Each col In dest.ListColumns
    ' is it calculated?
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.name = col.name Then
                ' matches.Add col.name
                Dim v As Variant
                v = srcCol.DataBodyRange.Value2
                Dim r2 As Range
                
                Set r2 = row.Cells(1, srcCol.Index).Resize(src.ListRows.Count, 1)
                r2.Value2 = v
                Exit For
            End If
        Next
    End If
Next
Application.Calculate
Application.Calculation = xlCalculationAutomatic
Application.ScreenUpdating = True
Application.StatusBar = False
Exit Sub

ErrorHandler:
Application.Calculate
Application.Calculation = xlCalculationAutomatic
Application.ScreenUpdating = True
Application.StatusBar = False
    
MsgBox Err.Description, vbExclamation, "Merge Error"


End Sub



Sub MergeFilteredListObject(dest As ListObject, src As ListObject, Optional filterColumn As String = "", Optional filterValue As Variant)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection

Dim lr As ListRow
' if there is only one row and it is empty, then use it otherwise we end up with a blank line
If dest.ListColumns("School Name").DataBodyRange.Cells(dest.ListRows.Count, 1) > "" Then
    Set lr = dest.ListRows.Add()
Else
    Set lr = dest.ListRows(dest.ListRows.Count)
End If
Dim row As Range
Dim r As Range

Set row = lr.Range
''dest.Parent.Activate            '' why??
DoEvents
Application.Calculation = xlCalculationManual
Application.ScreenUpdating = False
Application.StatusBar = "Importing " & src.name & ": " & src.ListRows.Count & " rows..."
On Error GoTo ErrorHandler
On Error GoTo ErrorHandler
'' now we can clear out anaything on the sheet below our desired range
Set r = row.Offset(1, 0).Resize(src.ListRows.Count)

Dim idx As Integer

Dim intMatches As Integer
Dim srcRange As Range
If filterColumn <> "" Then
    idx = src.ListColumns(filterColumn).Index
    src.Parent.Unprotect
    src.Range.AutoFilter idx, filterValue, xlFilterValues
    On Error Resume Next
    Set srcRange = src.ListColumns(filterColumn).DataBodyRange.SpecialCells(xlCellTypeVisible)
    On Error GoTo ErrorHandler
    If srcRange Is Nothing Then
        MsgBox "No matching row to copy from " & src.Parent.name, vbInformation, "Merge"
        GoTo SubExit
    End If
    If srcRange.Areas.Count > 1 Then
        '' sort it to get just one area
        '' but to do that, we need to take off the filter then reapply after the sort
        ' remove the filter....
        src.Range.AutoFilter idx
        ' apply the sort....
        src.Sort.SortFields.Clear
        src.Sort.SortFields.Add _
            Key:=Range(src.name & "[[#All],[" & filterColumn & "]]"), _
            SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
         With src.Sort
            .Header = xlYes
            .MatchCase = False
            .Orientation = xlTopToBottom
            .SortMethod = xlPinYin
            .Apply
        End With
        'reapply the filter....
        src.Range.AutoFilter idx, filterValue, xlFilterValues

    End If
    intMatches = srcRange.Rows.Count
Else
    ' remove any filter that may already be there
    src.AutoFilter.ShowAllData
End If
r.Clear
For Each col In dest.ListColumns
    ' is it calculated?
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.name = col.name Then
                ' matches.Add col.name
                Dim v As Variant

                Set srcRange = Nothing
                If filterColumn = "" Then
                    '' no filter - grab the whole column
                    Set srcRange = srcCol.DataBodyRange
                Else
                    ' the filter is applied so just get the visible cells
                    On Error Resume Next
                    Set srcRange = srcCol.DataBodyRange.SpecialCells(xlCellTypeVisible)
                    On Error GoTo ErrorHandler
                End If
                If Not (srcRange Is Nothing) Then
                    v = srcRange.Value2
                    Dim r2 As Range
                    Set r2 = row.Cells(1, srcCol.Index).Resize(srcRange.Rows.Count, 1)
                    r2.Value2 = v
                End If
                ''Exit For
            End If
        Next
    End If
Next

SubExit:

Application.Calculate
Application.Calculation = xlCalculationAutomatic
Application.ScreenUpdating = True
Application.StatusBar = False
If idx > 0 Then
    ' we applied a filter on the source - remove it
    src.Range.AutoFilter idx            ' will remoce the filter
    doProtect src.Parent
End If
Exit Sub

ErrorHandler:
Application.Calculate
Application.Calculation = xlCalculationAutomatic
Application.ScreenUpdating = True
Application.StatusBar = False

MsgBox Err.Description & ": " & src.name, vbExclamation, "Merge Error"

doProtect src.Parent
    


End Sub
Function getListObject(workbookname As String, sheetName As String)

Dim wk As Workbook
On Error GoTo ErrorHandler
For Each wk In Application.Workbooks
    If UCase(wk.FullName) = UCase(workbookname) Then
        Set getListObject = wk.Worksheets(sheetName).ListObjects(1)
        Exit Function
    End If
Next
Set getListObject = Nothing
      
Exit Function

ErrorHandler:

Set getListObject = Nothing

End Function

Function getSheetObject(workbookname As String, sheetName As String) As Worksheet

Dim wk As Workbook

For Each wk In Application.Workbooks
    If UCase(wk.FullName) = UCase(workbookname) Then
        Set getSheetObject = wk.Worksheets(sheetName)
        Exit Function
    End If
Next
        

End Function
Sub TestIt()

Dim dest As ListObject
Dim src As ListObject

Set src = getListObject("d:\files\pineapples\projects\fsm\test.xlsm", "Students")

shtStudents.Unprotect
Set dest = shtStudents.ListObjects(1)

MergeListObject dest, src
doProtect shtStudents
End Sub

' method tied to the button
Sub doMerge()
Dim wkName As String
Dim src As ListObject
Dim selectedSchool As String

Dim dest As ListObject

Dim SelectedList As String

On Error GoTo ErrorHandler

' get rid of spaces - which are not in 'SchoolStaff'
SelectedList = Replace(Range("selectedListName"), " ", "")
wkName = Range("selectedWorkbookName")
If wkName > "" Then
Else
        MsgBox "No source workbook selected. Open the source workbook, then select it form the list", vbExclamation, "Merge"
        Exit Sub
End If
If Range("AllOrOneSchool") = 2 Then
    If ThisWorkbook.MergeSelectedSchool > 0 Then
        selectedSchool = ThisWorkbook.MergeSelectedSchoolName
    Else
        MsgBox "Select a school from the list, or else select to merge All Schools", vbExclamation, "Merge"
        Exit Sub
    End If
End If
Select Case SelectedList
    Case "(all)"
        shtStudents.Unprotect
        Set dest = shtStudents.ListObjects(1)
        Set src = getListObject(wkName, "Students")
        If selectedSchool <> "" Then
            MergeFilteredListObject dest, src, "School Name", selectedSchool
        Else
            MergeFilteredListObject dest, src
        End If
        doProtect shtStudents
        
        shtStaff.Unprotect
        Set dest = shtStaff.ListObjects(1)
        Set src = getListObject(wkName, "SchoolStaff")
        If selectedSchool <> "" Then
            MergeFilteredListObject dest, src, "School Name", selectedSchool
        Else
            MergeFilteredListObject dest, src
        End If
        doProtect shtStaff
        
        shtSchools.Unprotect
        Set dest = shtSchools.ListObjects(1)
        Set src = getListObject(wkName, "Schools")
        If selectedSchool <> "" Then
            MergeFilteredListObject dest, src, "School Name", selectedSchool
        Else
            MergeFilteredListObject dest, src
        End If
        doProtect shtSchools
        
        shtWash.Unprotect
        Set dest = shtWash.ListObjects(1)
        Set src = getListObject(wkName, "Wash")
        If selectedSchool <> "" Then
            MergeFilteredListObject dest, src, "School Name", selectedSchool
        Else
            MergeFilteredListObject dest, src
        End If
        doProtect shtWash
        MsgBox "Merge All sheets completed", vbInformation, "Merge All Sheets"
    Case Else
        ThisWorkbook.Worksheets(SelectedList).Unprotect
        Set dest = ThisWorkbook.Worksheets(SelectedList).ListObjects(1)
        Set src = getListObject(wkName, SelectedList)
        If selectedSchool <> "" Then
            MergeFilteredListObject dest, src, "School Name", selectedSchool
        Else
            MergeFilteredListObject dest, src
        End If
        doProtect ThisWorkbook.Worksheets(SelectedList)
        
End Select
    
    
Application.CalculateFull
Exit Sub

ErrorHandler:
    Application.Calculation = xlCalculationAutomatic
    Application.ScreenUpdating = True
    Application.StatusBar = False

    MsgBox "An error occured performing the merge to " & SelectedList & ": " & vbCrLf & Err.Description, vbExclamation, "Merge Error"
    
End Sub

'Function openWorkbooks()
'Application.Volatile True
'Dim wk As Workbook
'Dim d As Scripting.Dictionary
'Set d = New Scripting.Dictionary
'
'For Each wk In Application.Workbooks
'    If wk Is ThisWorkbook Then
'    Else
'        d.Add wk.FullName, wk
'    End If
'Next
'
'ReDim p(1 To d.Count, 1 To 1)
'Dim name As Variant
'Dim i As Integer
'i = 1
'For Each name In d.Keys
'    p(i, 1) = name
'    i = i + 1
'Next
'openWorkbooks = p
'
'End Function


Function openWorkbook(idx As Integer)
'' no longer volatile - events are used to ensure this list is up to date
'''Application.Volatile True
If idx > Application.Workbooks.Count Then
    openWorkbook = ""
ElseIf Application.Workbooks(idx).FullName = ThisWorkbook.FullName Then
    openWorkbook = ""
Else
    openWorkbook = Application.Workbooks(idx).FullName
End If
End Function

Function getSelectedSchool()
Dim ddshp As Shape
Dim dd As ControlFormat
Set ddshp = ThisWorkbook.Sheets("Merge").Shapes("ddMergeSelectedSchool")
Set dd = ddshp.ControlFormat
getSelectedSchool = dd.List(Range("mergeSelectedSchool"))
End Function

Sub filterListBySchool(lo As ListObject, schName As String)
Dim idx As Integer
idx = lo.ListColumns("School Name").Index
lo.Range.AutoFilter idx, schName, xlFilterValues

End Sub


