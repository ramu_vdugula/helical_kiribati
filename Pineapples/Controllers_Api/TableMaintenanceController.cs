﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using Softwords.Web;
using Softwords.Web.Models;
using System.Security.Claims;
using System.Data.Entity;


namespace Pineapples.Controllers
{
    public class TableMaintenanceController<T, TKey> : PineapplesApiController, ITableMaintenancePermissions where T : class, new()
    {
        #region constructors

        /// <summary>
        /// constructor with no security controls
        /// </summary>
        /// <param name="factory"></param>
        public TableMaintenanceController(DataLayer.IDSFactory factory) : base(factory) { }

        /// <summary>
        /// Security by topic and access level, with default access levels
        /// </summary>
        /// <param name="topic">the security topic</param>
        /// <param name="factory"></param>
        public TableMaintenanceController(int topic, DataLayer.IDSFactory factory) : base(factory)
        {
            Topic = topic;
            Role = null;
        }

        /// <summary>
        /// Security by topic and access level, with specified access levels
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="createAccess"></param>
        /// <param name="updateAccess"></param>
        /// <param name="deleteAccess"></param>
        /// <param name="factory"></param>
        public TableMaintenanceController(int topic,
            PermissionAccess createAccess, PermissionAccess updateAccess, PermissionAccess deleteAccess,
            DataLayer.IDSFactory factory) : base(factory)
        {
            Topic = topic;
            Role = null;
            this.CreateAccess = createAccess;
            this.UpdateAccess = updateAccess;
            this.DeleteAccess = deleteAccess;
        }

        /// <summary>
        /// Security by role
        /// </summary>
        /// <param name="role">role required to edit table</param>
        /// <param name="factory"></param>
        public TableMaintenanceController(string role, DataLayer.IDSFactory factory) : base(factory)
        {
            Topic = -1;
            Role = role;
        }

        /// <summary>
        /// security by topic, or role
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="role"></param>
        /// <param name="factory"></param>
        public TableMaintenanceController(int topic, string role, DataLayer.IDSFactory factory) : base(factory)
        {
            Topic = topic;
            Role = role;
        }

        /// <summary>
        /// security by topic with custom access levels, or role
        /// </summary>
        /// <param name="topic"></param>
        /// <param name="createAccess"></param>
        /// <param name="updateAccess"></param>
        /// <param name="deleteAccess"></param>
        /// <param name="role"></param>
        /// <param name="factory"></param>
        public TableMaintenanceController(int topic,
            PermissionAccess createAccess, PermissionAccess updateAccess, PermissionAccess deleteAccess,
            string role, DataLayer.IDSFactory factory) : base(factory)
        {
            Topic = topic;
            this.CreateAccess = createAccess;
            this.UpdateAccess = updateAccess;
            this.DeleteAccess = deleteAccess;

            Role = role;
        }
        #endregion

        #region rest operations
        [HttpGet]
        [Route("")]
        [TablePermission(Softwords.Web.TableOperation.Read)]
        public virtual IEnumerable<T> ReadAll()
        {
            return Factory.Context.Set<T>();
        }

        [HttpGet]
        [Route(@"{ID}")]
        [TablePermission(Softwords.Web.TableOperation.Read)]
        public async virtual Task<T> Read(TKey ID)
        {
            return await Factory.Context.Set<T>().FindAsync(ID);
        }

        [HttpPost]
        [Route(@"")]
        [Route(@"{ID}")]
        [TablePermission(Softwords.Web.TableOperation.Create)]
        public async Task<T> Create(Softwords.Web.Models.DynamicBinder<T> binder)
        {
            try
            {
                return await binder
                   .Identity((ClaimsIdentity)User.Identity)
                   .Context(Factory.Context)
                   .Create();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpPut]
        [Route(@"{ID}")]
        [TablePermission(Softwords.Web.TableOperation.Update)]
        public async Task<DynamicBinder<T>> Update(TKey ID, DynamicBinder<T> binder)
        {
            try
            {
                return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Update(ID);

            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpDelete]
        [Route(@"{ID}")]
        [TablePermission(Softwords.Web.TableOperation.Delete)]
        public async Task<T> Delete(TKey ID, DynamicBinder<T> binder)
        {
            return await binder
                    .Identity((ClaimsIdentity)User.Identity)
                    .Context(Factory.Context)
                    .Delete(ID);
        }

        /// <summary>
        /// Return a default "view mode", recorded on the server as a json file
        /// for the current code type
        /// </summary>
        /// <returns>the json representation of the appropriate view mode</returns>
        [HttpGet]
        [Route(@"info/viewmode")]
        public virtual object getViewMode()
        {
            Type tt = typeof(T);
            string codetable = tt.Name;
            object json = viewMode("lookups", codetable);
            if (json == null)
            {
                Pineapples.Models.ViewMode vm = new Pineapples.Models.ViewMode(tt);
                return vm;
                //if (tt.IsSubclassOf(typeof(Pineapples.Data.Models.SequencedCodeTable)))
                //{
                //    json = viewMode("lookups", "sequencedCodeTable");
                //}
                //else
                //{
                //    json = viewMode("lookups", "simpleCodeTable");
                //}

            }
            return json;
        }
        #endregion

        #region viewmode
        protected object viewMode(string feature, string name)
        {
            const string viewModeRoot = @"\viewModes";

            string viewModeBase = System.Web.Hosting.HostingEnvironment.MapPath(viewModeRoot);  // the base path
            string filePath = String.Empty;

            // get any context modes first....
            if (Context != String.Empty)
            {
                filePath = System.IO.Path.Combine(viewModeBase, Context, feature, name) + ".json";

                if (File.Exists(filePath))
                {
                    return jsonFromFile(filePath);
                }
            }
            filePath = System.IO.Path.Combine(viewModeBase, "default", feature, name) + ".json";
            if (File.Exists(filePath))
            {
                return jsonFromFile(filePath);
            }
            return null;

        }

        private JObject jsonFromFile(string path)
        {
            string jsonstring = System.IO.File.ReadAllText(path);
            Newtonsoft.Json.Linq.JObject json = (Newtonsoft.Json.Linq.JObject)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonstring);
            return json;
        }
        #endregion

        #region permissions

        protected PermissionAccess ReadAccess = PermissionAccess.Read;
        protected PermissionAccess UpdateAccess = PermissionAccess.Write;
        protected PermissionAccess DeleteAccess = PermissionAccess.Write;
        protected PermissionAccess CreateAccess = PermissionAccess.Write;

        protected int Topic { get; private set; }
        protected string Role { get; private set; }

        public bool checkPermission(TableOperation operation, ClaimsPrincipal principal)
        {
            if (Topic < 0 && Role == null)
            {
                // no security assigned 
                return true;
            }
            // authorization is required - so return 401 if not logged in
            if (!principal.Identity.IsAuthenticated)
            {
                // note that 401 is typically interpreted as Unuathenticated, not unauthorized
                // we use 403 (below) when autnicated, but no permission
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            if (Topic >= 0)
            {
                PermissionAccess access = PermissionAccess.Admin;
                switch (operation)
                {
                    case TableOperation.Read:
                        access = ReadAccess;
                        break;
                    case TableOperation.Create:
                        access = CreateAccess;
                        break;
                    case TableOperation.Delete:
                        access = DeleteAccess;
                        break;
                    case TableOperation.Update:
                        access = UpdateAccess;
                        break;
                }
                string permissionHash = principal.FindFirst("Permission").Value;

                // find the character offset for the permission area
                // 3 pieces need to be kept in synch on this array
                // 1) here
                // 2) in the stored proc permissionHash
                // 3) in the javascript configPermissions
                byte charvalue = System.Text.Encoding.ASCII.GetBytes(permissionHash)[Topic];

                PermissionAccess topicHash = (PermissionAccess)(charvalue - 48);            // remove the offset which is there to make a printable string

                if (topicHash.HasFlag(access))
                {
                    return true;

                }
                if (Role == null)
                {
                    // nowhere to go
                    throw new PermissionException(Topic, access, topicHash);
                }
            }
            // role must be defined to get to here....
            switch (operation)
            {
                case TableOperation.Read:
                    return true;
                case TableOperation.Create:
                case TableOperation.Delete:
                case TableOperation.Update:
                    if (principal.IsInRole(Role))
                    {
                        return true;
                    }
                    break;
            }
            throw new HttpResponseException(HttpStatusCode.Forbidden);
        }

        #endregion
    }
}
