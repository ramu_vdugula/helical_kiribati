﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.Web;

namespace Pineapples.Controllers
{
  public class PerfAssessController : PineapplesApiController
  {
    public PerfAssessController(DataLayer.IDSFactory factory) : base(factory) { }

    #region PA methods
    [HttpGet]
    public object Read(int id)
    {
      return Factory.PerfAssess().Read(id);
    }

    [HttpPost]
    public object Save(Data.Models.PerfAssessment pf)
    {
      try
      {
        return Factory.PerfAssess().Save(pf);
      }
      catch (ConcurrencyException ex)
      {
        // return the object as the body
        var resp = Request.CreateResponse(HttpStatusCode.Conflict, (Data.Models.PerfAssessment)ex.NewVersion);
        throw new HttpResponseException(resp);
      }
    }

    [HttpPost]
    [Route("api/perfassess/item/template")]
    public Data.Models.PerfAssessment Template(Data.Models.PerfAssessment pf)
    {
      return Factory.PerfAssess().Template(pf.Framework, pf.TeacherId);
    }

    [HttpPost]
    [Route("api/perfassess/collection/filter")]
    public object Filter(PerfAssessFilter fltr)
    {
      return Factory.PerfAssess().Filter(fltr);
    }

    [HttpPost]
    [Route("api/perfassess/collection/table")]
    public object Table(PerfAssessTableFilter fltr)
    {
      return Factory.PerfAssess().Table(fltr);
    }
    #endregion
  }
}
