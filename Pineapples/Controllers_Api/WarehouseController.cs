﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/warehouse")]
    public class WarehouseController : PineapplesApiController
    {
        public WarehouseController(DataLayer.IDSFactory factory) : base(factory) { }


        [HttpGet]
		[Deflatable]
		[Route(@"enrolbyschool/{schoolNo}")]
        //[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object GetSchool(string schoolNo)
        {
          //AccessControl(schoolNo);
          IDataResult ds = Ds.TableEnrolBySchool(schoolNo);
          return ds.ResultSet;

        }

        [HttpGet]
		[Deflatable]
		[Route("tableenrol")]
        public object TableEnrol()
        {
            IDataResult ds = Ds.TableEnrol();
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("districtenrol")]
        public object DistrictEnrol()
        {
          IDataResult ds = Ds.TableDistrictEnrol();
          return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("schoolflowrates")]
        public object AllSchoolFlowRates()
        {
            IDataResult ds = Ds.AllSchoolFlowRates();
            return ds.ResultSet;
        }
		[HttpGet]
		[Deflatable]
		[Route("schoolflowrates/{schoolNo}")]
		public object SchoolFlowRates(string schoolNo)
		{
			IDataResult ds = Ds.SchoolFlowRates(schoolNo);
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("schoolteachercount/{schoolNo}")]
        public object SchoolTeacherCount(string schoolNo)
        {
            IDataResult ds = Ds.SchoolTeacherCount(schoolNo);
            return ds.ResultSet;
        }
		[HttpGet]
		[Deflatable]
		[Route("schoolteachercount")]
		public object AllSchoolTeacherCount()
		{
			IDataResult ds = Ds.SchoolTeacherCount(null);
			return ds.ResultSet;
		}

		[HttpGet]
		[Deflatable]
		[Route("schoolteacherpupilratio")]
        public object SchoolTeacherPupilRatio()
        {
            IDataResult ds = Ds.SchoolTeacherPupilRatio();
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("examsdistrictresults")]
        public object ExamsDistrictResults()
        {
            // to do - what should this return??
            IDataResult ds = Ds.ExamDistrictResults();
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("examsschoolresults")]
        public object ExamsAllResults()
        {
            IDataResult ds = Ds.ExamAllSchoolResults();
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("examsschoolresults/{schoolNo}")]
        public object ExamsSchoolResults(string schoolNo)
        {
            IDataResult ds = Ds.ExamSchoolResults(schoolNo);
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("teachercount")]
        public object TeacherCount()
        {
            IDataResult ds = Ds.TeacherCount();
            return ds.ResultSet;
        }

        [HttpGet]
		[Deflatable]
		[Route("teacherqual")]
        public object TeacherQual()
        {
            IDataResult ds = Ds.TeacherQual();
			return ds.ResultSet;
		}

        [HttpGet]
		[Deflatable]
		[Route("teacherpupilratio")]
        public object TeacherPupilRatio()
        {
            IDataResult ds = Ds.TeacherPupilRatio();
			return ds.ResultSet;
		}

        [HttpGet]
		[Deflatable]
		[Route("classleveler")]
        public object ClassLevelER()
        {
            IDataResult ds = Ds.ClassLevelER();
			return ds.ResultSet;
		}

        [HttpGet]
		[Deflatable]
		[Route("edleveler")]
        public object EdLevelER()
        {
            IDataResult ds = Ds.EdLevelER();
			return ds.ResultSet;
		}

        [HttpGet]
		[Deflatable]
		[Route("flowrates")]
        public object FlowRates()
        {
            IDataResult ds = Ds.FlowRates();
			return ds.ResultSet;
		}

        [HttpGet]
		[Deflatable]
		[Route("edlevelage")]
        public object EdLevelAge()
        {
            IDataResult ds = Ds.EdLevelAge();
			return ds.ResultSet;
		}

      
        /// <summary>
        /// In this controller, use the 'deflating' json serializer since these arae likely to be big
        /// </summary>
        /// <param name="dt">the datatable to serialize</param>
        /// <returns></returns>
        private HttpResponseMessage Deflate( System.Data.DataTable dt)
        {
            var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

            var json = formatter.SerializerSettings;

            json.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
            json.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
            json.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            json.Formatting = Newtonsoft.Json.Formatting.None;
            json.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
           
            var stream = Softwords.Web.Json.DatasetSerializer.toStream(dt);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return response;

        }
        private IDSWarehouse Ds
        {
            get { return Factory.Warehouse(); }
        }
    }
}
