﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/quarterlyreportsx")]
    public class QuarterlyReportsXController : PineapplesApiController
    {
        public QuarterlyReportsXController(DataLayer.IDSFactory factory) : base(factory) { }

        #region QuarterlyReportX methods
        [HttpPost]
        [Route(@"")]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public HttpResponseMessage Create(QuarterlyReportXBinder qr)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(qr, (ClaimsIdentity)User.Identity));
                return response;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.ReadX)]
        public object Read(int qrID)
        {
            return Ds.Read(qrID);
        }
                
        [HttpPut]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public object Update(QuarterlyReportXBinder qr)
        {
            try
            {
                return Ds.Update(qr, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpDelete]
        [Route(@"{qrID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public object Delete([FromBody] QuarterlyReportXBinder qr)
        {
            // TO DO
            return qr.definedProps;
        }
        #endregion

        #region QuarterlyReportX Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(QuarterlyReportXFilter fltr)
        {
            return Ds.Filter(fltr);
        }
        #endregion

        private IDSQuarterlyReportX Ds
        {
            get { return Factory.QuarterlyReportX(); }
        }

    }
}
