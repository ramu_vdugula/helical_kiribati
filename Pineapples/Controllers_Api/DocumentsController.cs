﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Softwords.Web.Identity;
using System.Threading.Tasks;
using Softwords.Web;


namespace Pineapples.Controllers
{
    [Authorize]
    [RoutePrefix("api/documents")]
    public class DocumentsController : PineapplesApiController
    {
        public DocumentsController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpPut]
        [Route(@"{docID}")]
        public object Update(Data.DocumentBinder binder)
        {
            try
            {
                return Ds.Update(binder, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        private IDSDocument Ds
        {
            get { return Factory.Document(); }
        }


    }
}
