﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Softwords.Web.Models;

namespace Pineapples.Providers
{
    public class ApplicationOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        public ApplicationOAuthProvider(string publicClientId)
        {
            if (publicClientId == null)
            {
                throw new ArgumentNullException("publicClientId");
            }

            _publicClientId = publicClientId;
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var userManager = context.OwinContext.GetUserManager<Softwords.Web.ApplicationUserManager>();

            ApplicationUser user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }

            ClaimsIdentity oAuthIdentity = await user.GenerateUserIdentityAsync(userManager,
               OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await user.GenerateUserIdentityAsync(userManager,
                CookieAuthenticationDefaults.AuthenticationType);

            // see http://bitoftech.net/2014/06/01/token-based-authentication-asp-net-web-api-2-owin-asp-net-identity/
            // for some explanation of what this is all about
            AuthenticationProperties properties = CreateProperties(user, context.UserName,  oAuthIdentity);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);

            // validated by ticket allows us to pass authentication properties back to the client
            // validation of ClaimsIdentity is shown in the link above, so this is the more general form
            context.Validated(ticket);

            // cookie sign in???? presumably if we take this out there
            //context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            // clientID means ... the client app? so can be generally ignored
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }


        // this: http://stackoverflow.com/questions/20693082/setting-the-redirect-uri-in-asp-net-identity
        // explains a bit about where the below comes from, 
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        //--------------------
        // this is the critical place for adding any properties you want returned with the login token
        //---------------------
        public static AuthenticationProperties CreateProperties(ApplicationUser user, string userName, ClaimsIdentity oAuthIdentity)
        {
            string menuContent = user.Menu.MenuContent;
            string menuHome = user.Menu.MenuHome;
            //string menuKey = user.MenuKey;
            //using (ApplicationDbContext cxt = new ApplicationDbContext())
            //{
            //    var lnq2sql = (from m in cxt.ClientMenu where m.MenuKey == menuKey select m);
            //    menuContent = (from m in lnq2sql select m.MenuContent).SingleOrDefault<System.String>();
            //}
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName },
                // here we send back the roles - note the change of signature in this function
                // see http://stackoverflow.com/questions/23734791/how-can-i-send-authorization-information-back-to-my-client-app-when-using-angula
                // however have modified this idea to send roles as an array, as expected by the client
                // based on this example
                //http://plnkr.co/edit/UkHDqFD8P7tTEqSaCOcc?p=preview
                 { "roles",string.Join(",",oAuthIdentity.Claims.Where(c=> c.Type == ClaimTypes.Role).Select(c => c.Value).ToArray())},
                 { "menu", menuContent},
                 { "home", menuHome}
                 

            };
            return new AuthenticationProperties(data);
        }
    }
}