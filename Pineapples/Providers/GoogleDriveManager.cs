﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
//using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Pineapples
{
	public class GoogleDriveManager
	{
		static string[] Scopes = { DriveService.Scope.Drive };
		static string ApplicationName = "Pacific EMIS ";


		static public DriveService getDriveService()
		{
			var fsSource = new System.IO.FileStream
			  (System.Web.Hosting.HostingEnvironment.MapPath(@"~/assets_local/google_credentials.json"),
				System.IO.FileMode.Open, System.IO.FileAccess.Read);

			GoogleCredential credential = GoogleCredential.FromStream(fsSource)
				.CreateScoped(Scopes);

			// Create Drive API service.
			var service = new DriveService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = ApplicationName,
			});

			return service;
		}

		/// <summary>
		/// Return a memory stream containing the file content
		/// </summary>
		/// <param name="fileId">the file Id of the google file</param>
		/// <returns></returns>
		static async public Task<System.IO.MemoryStream> getFileContent(string fileId)
		{
			var service = getDriveService();
			var request = service.Files.Get(fileId);
			var stream = new System.IO.MemoryStream();

			Google.Apis.Download.IDownloadProgress progress = await request.DownloadAsync(stream);
			switch (progress.Status) 
			{
				case Google.Apis.Download.DownloadStatus.Completed:
					break;
			}
			stream.Position = 0;
			return stream;
		}

		/// <summary>
		/// Get selected metadata for a goggle file
		/// </summary>
		/// <param name="fileId">The google file Id</param>
		/// <returns></returns>
		static async public Task<File> getFileMetaData(string fileId)
		{
			var service = getDriveService();
			var request = service.Files.Get(fileId);
			request.Fields =
				"id, name, mimeType, appProperties, createdTime, modifiedTime, lastModifyingUser, properties, description, fileExtension, quotaBytesUsed";
			return await request.ExecuteAsync();

		}

		/// <summary>
		/// Return search results based on file name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		static async public Task<FileList> FindFiles(string name)
		{
			return await FindFiles(name, null, null);
		}
		static async public Task<FileList> FindFiles(string name, string mimeType)
		{
			return await FindFiles(name, mimeType, null);
		}
		static async public Task<FileList> FindFiles(IDictionary<string, string> properties)
		{
			return await FindFiles(null, null, properties);
		}
		static async public Task<FileList> FindFiles(string name, string mimeType, IDictionary<string, string> properties)
		{
			
			
			System.Collections.Generic.List<string> listq = new List<string>();

			if (!String.IsNullOrEmpty(name) )
			{
				listq.Add(String.Format("name='{0}'", name));
			}
			if (!String.IsNullOrEmpty(mimeType))
			{
				listq.Add(String.Format("mimeType contains '{0}'", mimeType));
			}

			if (properties != null)
			{
				foreach (var prop in properties)
				{
					listq.Add(String.Format("properties has {{ key = '{0}' and value = '{1}' }}", prop.Key, prop.Value));
				}
			}
			string q = string.Join(" AND ", listq.ToArray());
			return await FilesQuery(q);

		}


		static async public Task<FileList> FilesQuery(string Q)
		{
			var service = getDriveService();

			// Define parameters of request.
			FilesResource.ListRequest listRequest = service.Files.List();
			listRequest.PageSize = 50;
			listRequest.Fields = "nextPageToken, files(id, name, mimeType, appProperties, createdTime, lastModifyingUser, hasThumbnail, properties, description, kind, fileExtension)";
			listRequest.Q = Q;

			// List files.
			return await listRequest.ExecuteAsync();
			
		}


		/// <summary>
		/// set a collection of properties on the file object
		/// properties in the collection are 
		///  - inserted , if the property name doe not yet exist
		///  - updated  , if the property name already exists on the file
		///  - removed  , if the property value is null
		///  property names already on the file that ar not referenced in the propertiesToSet are not affected
		/// </summary>
		/// <param name="fileId"></param>
		/// <param name="propertiesToSet">dictionary of properties</param>
		/// <returns></returns>
		static public object SetProperties(string fileId, IDictionary<string, string> propertiesToSet)
		{
			var service = getDriveService();
			var fileMetaData = new File();
			fileMetaData.Properties = propertiesToSet;

			var request = service.Files.Update(fileMetaData, fileId);
			request.Fields = "id, name,  properties";
			return request.Execute();
		}

	}
}
