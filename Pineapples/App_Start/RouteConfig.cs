﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Pineapples
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes(); //Enables Attribute Routing

            // feature on the common pages allows them to be configured according to the current feature
            routes.MapRoute(
                name: "common",
                url: "common/{action}/{feature}",
                defaults: new { controller = "common", action = "Index", feature = UrlParameter.Optional }
            );

            // local routes are comon routes that need to be configured with local data
            routes.MapRoute(
                    name: "local",
                    url: "local/{action}/{feature}",
                    defaults: new { controller = "local", action = "Index", feature = UrlParameter.Optional }
                );

            // sharepoint documents by redirection
            routes.MapRoute(
              name: "docByPath",
              url: "document/path",
              defaults: new { controller = "document", action = "path"}
            );

            routes.MapRoute(
              name: "docByType",
              url: "document/type/{id}/{docType}",
              defaults: new { controller = "document", action = "type" }
            );
            // for redirecting the base page to get the correct base on the client
            routes.MapRoute(
              name: "blank",
              url: "",
              defaults: new { controller = "Home", action = "blank", id = UrlParameter.Optional }
            );

            // SSRS routes
            routes.MapRoute(
                 name: "SSRS",
                 url: "SSRS/{format}",
                 defaults: new { controller = "SSRS", action = "RunReport", format="EXCEL" }
             );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
