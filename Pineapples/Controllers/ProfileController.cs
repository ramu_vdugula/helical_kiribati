﻿using System.Web.Mvc;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class ProfileController : mvcControllerBase
    {
        [Route("profile/permissions")]
        public ActionResult Permissions()
        {
            return View("ProfilePermissions");
        }
    }
}