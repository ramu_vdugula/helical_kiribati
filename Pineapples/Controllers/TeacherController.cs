﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;
using Softwords.Web;

namespace Pineapples.mvcControllers
{
    public class TeacherController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Teacher
        [Authorize]
        public ActionResult Searcher(string version)
        {
            // pass through the data needed for the model
            TeacherSearcherModel model = new TeacherSearcherModel();
            string viewName = "TeacherSearcher";

            switch (CurrentUser.MenuKey)
            {
                default:
                    viewName = viewName + version;
                    break;
            }
            return View(viewName, model);
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Teacher, PermissionAccess.Write);
            return View("TeacherItem");
        }

        #region Teacher-centric components
        public ActionResult SearcherComponent()
        {
            return View();
        }

        public ActionResult SurveyList()
        {
            return View();
        }


        public ActionResult AppointmentList()
        {
            return View();
        }
        #region Qualifications
        public ActionResult QualificationList()
        {
            PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Teacher);
            return View(ps);
        }
        [LayoutInjector("EditDialogLayout")]
        public ActionResult QualificationDialog()
        {
            PermissionSet ps = new Models.PermissionSet(PermissionTopicEnum.Teacher);
            return View(ps);
        }

        #endregion
        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult PayslipDialog()
        {
            return View();
        }

        public ActionResult Payslip()
        {
            return View();
        }
        public ActionResult PayslipList()
        {
            return View();
        }
        public ActionResult LinkList()
        {
            return View();
        }

        public ActionResult Upload()
        {
            return View();
        }
        #endregion
    }
}