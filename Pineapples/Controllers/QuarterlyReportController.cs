﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class QuarterlyReportController : Softwords.Web.mvcControllers.mvcControllerBase
    {

        public ActionResult Dashboard()
        {
            return View();
        }

        [Authorize]
        public ActionResult Searcher(string version)
        {
            return View();
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            // pass through the data needed for the model
            // refine permissions (e.g. PermissionTopicEnum.QuarterlyReport)?
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.Inspection, PermissionAccess.Write);

            return View();
        }

        #region QuarterlyReport-centric components
        public ActionResult SearcherComponent()
        {
            return View();
        }
        #endregion
    }
}