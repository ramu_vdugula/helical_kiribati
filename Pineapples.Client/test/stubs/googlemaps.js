﻿// stub to define google maps on the window
; (function (window, googlemaps) {
  if (!googlemaps) {
    var point = function (x, y) {
      this.x = x;
      this.y = y;
    };
    var polygon = function () {
    };
    var map = function () {
    };
    var latlngBounds = function () {
    };
    var marker = function () {
    };
    var maptypeid = {
      HYBRID: "HYBRID",
      SATELLITE: "SATELLITE",
      TERRAIN: "TERRAIN",
      ROADMAP: "ROADMAP"
    };
    var google = {
      maps: {
        Point: point,
        Polygon: polygon,
        Map: map,
        LatLngBounds: latlngBounds,
        Marker: marker,
        MapTypeId: maptypeid,
        OverlayView: function() { return {};}
      }
    };
    window.google = google; 
  }
})(window, window.google);