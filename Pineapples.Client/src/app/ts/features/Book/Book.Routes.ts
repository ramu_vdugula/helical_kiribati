// Books Routes
namespace Pineappples.Books {

  let routes = function ($stateProvider) {
    var featurename = 'Books';
    var filtername = 'BookFilter';
    var templatepath = "book";
    var tableOptions = "bookFieldOptions";
    var url = "books";
    var usersettings = null;
    //var mapview = 'SchoolMapView';

    // root state for 'school' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath);

    // default 'api' in this feature is schoolsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "booksAPI";

    let statename = "site.books";
    $stateProvider.state("site.books", state);

    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("Book");
    statename = "site.books.list";
    state.url = "^/books/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/books/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("book/item");
        $templateCache.remove("book/searcher");
        $state.go("site.books.list");
      }]
    };
    statename = "site.books.reload";
    $stateProvider.state(statename, state);

    // chart table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
//    Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

    // new - state with a custom url route
    state = {
      url: "^/books/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.books.list": {
          component: "componentBook"
        }

      },
      resolve: {
        model: ['booksAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.books.list.new", state);

    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/books/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.books.list": {
          component: "componentBook"
        }
      },
      resolve: {
        model: ['booksAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.books.list.item", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}
