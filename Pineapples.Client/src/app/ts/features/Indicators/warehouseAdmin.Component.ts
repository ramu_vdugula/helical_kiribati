﻿namespace Pineapples.Admin {

  interface IBindings {

  }

  class edLevelERInfo {
    edLevel: Sw.Lookups.ILookupEntry;
    Population: Pineapples.Indicators.IGenderedValue;
    Enrolment: Pineapples.Indicators.IGenderedValue;
    NetEnrolment: Pineapples.Indicators.IGenderedValue;
    GER: Pineapples.Indicators.IGenderedValue;
    NER: Pineapples.Indicators.IGenderedValue;
    YearsOfSchooling: number;
    OfficialStartAge: number;
    AgeRange: string;
  }

  class Controller implements IBindings {

    static $inject = ["IndicatorsAPI", "ApiUi","IndicatorsMgr","Lookups"];
    constructor(public api: Pineapples.Api.IIndicatorsApi
      , public apiUi: Sw.Api.IApiUi
      , public indicatorsMgr: Pineapples.Indicators.IndicatorsMgr
      , public lookups: Sw.Lookups.LookupService) { }

    public startAtYear: number = null;

    public enrolTable: any[]; // table of enrolment reconciliation produced by warehouse.reconcile
    public enrolTableCols: any[]; // columns in enrolTable - these may get added too, so try to make this dynamic when it is rendered

    public teacherTable: any[]; // table of teacher reconciliation produced by warehouse.reconcile
    public teacherTableCols: any[]; // columns in teacherTable

    public referenceYear: number;   // last year of enrol data

    public nationCalculator: Pineapples.Indicators.IndicatorCalc;
    public indicatorSet: Pineapples.Indicators.IndicatorSet;
    public ERTables: edLevelERInfo[]; 

    public warehouseRefreshed: boolean = false;
    public vermdataRefreshed: boolean = false;
    public calculatorRefreshed: boolean = false;
    public districtUpdates: string[];

    public isWaiting: boolean;

    // refresh the warehouse.... this may take some time
    public refresh() {
      this.isWaiting = true;
      this.warehouseRefreshed = false;
      this.vermdataRefreshed = false;
      this.calculatorRefreshed = false;
      this.districtUpdates = [];

      this.api.refreshWarehouse(this.startAtYear).then((response) => {
        this.isWaiting = false;
        this.enrolTable = response.ResultSet[0];
        this.teacherTable = response.ResultSet[1];
        // collect the column names in both sets - we can safely assume that there is at least one row
        // in each table
        this.enrolTableCols = Object.keys(this.enrolTable[0]);
        this.teacherTableCols = Object.keys(this.teacherTable[1]);
        // extract the last year for which we got enrolment data
        // , store it for reference later when we calculate from the vermdata
        let e = _.findLast(this.enrolTable, (e) => (e[this.enrolTableCols[1]] > 0));
        if (!e) {
          e = this.enrolTable[this.enrolTable.length - 1];    // the very last
        }
        this.referenceYear = e.svyYear;

        this.warehouseRefreshed = true;
        this.refreshVermData();
      }, (e) => {
        this.isWaiting = false;
        this.apiUi.showErrorResponse(null, e)
      });
    };

    public refreshVermData() {
      this.isWaiting = true;
      this.vermdataRefreshed = false;
      this.calculatorRefreshed = false;

      this.api.refreshVermData().then(() => {
        this.isWaiting = false;
        this.vermdataRefreshed = true;
        // the indicatorsMgr needs to know that all caches are now invalid
        this.indicatorsMgr.initialise();       
        this.getCalculator();
      }, (e) => {
        this.isWaiting = false;
        this.apiUi.showErrorResponse(null, e);
        });
      this.districtUpdates = [];
      this.lookups.cache["districts"].forEach((c) => {
        this.api.refreshVermDataDistrict(c.C).then(() => {
          this.districtUpdates.push(c.N);
        })
      })
      
    };

    public getCalculator() {
      this.isWaiting = true;
      this.calculatorRefreshed = false;

      this.indicatorsMgr.getCalculator().then((calc) => {
        this.calculatorRefreshed = true;
        this.nationCalculator = calc;

        // to do - where does PRI come from? its the lookup in EducationLevels with startYear = 1?
        this.indicatorSet = this.indicatorsMgr.get(calc, this.referenceYear, 'PRI');
        // run through al the ed levels and buld a little ER summary
        this.ERTables = this.lookups.cache["educationLevels"].map((c) => {
          let edi = new edLevelERInfo();
          edi.edLevel = c;

          let edLevelNode = calc.getEdLevelNode(this.referenceYear, c.C);
          edi.Population = calc.getMFT(edLevelNode, 'pop');
          edi.Enrolment = calc.getMFT(edLevelNode, 'enrol');
          edi.NetEnrolment = calc.getMFT(edLevelNode, 'nenrol');
          edi.GER = calc.getMFT(edLevelNode, 'ger');
          edi.NER = calc.getMFT(edLevelNode, 'ner');

          edi.YearsOfSchooling = calc.edLevelNumYears(this.referenceYear, c.C);
          edi.OfficialStartAge = calc.edLevelStartAge(this.referenceYear, c.C);
          edi.AgeRange = (edi.OfficialStartAge).toString() + "-" + (edi.OfficialStartAge + edi.YearsOfSchooling - 1).toString() + " yo";

          return edi;
        })
      })
    }

    public $onChanges(changes) {
    }
  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "indicators/admin";
    }
  }

  angular
    .module("pineapples")
    .component("componentWarehouseAdmin", new Component());
}