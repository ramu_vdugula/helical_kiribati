﻿module Pineapples {
  interface IIndicator {
    IndicatorId: number;
    PerformanceLevel: string;
    IndicatorCode: string;
  }

  interface IPerfAssess {

  }

  interface IKeyValues {
    key: any;
    values: any;
  }

  class PerfAssessController {
    public teacherFilter: any;
    public schoolFilter: any;
    public lkp: any;
    public api: any;
    public apiUi: any;
    private dialogs: angular.material.IDialogService;

    public indicatorCount: number;
    public indicatorsCompleted: number;

    private _pa;
    public list: any[]; // list of teacher's pas

    public isWaiting: boolean;
    private _collapsers;

    public selectedTeacher: any;
    public selectedSchool: any;

    public paNest: IKeyValues[];

    private _elemAggs: IKeyValues[];
    private _comAggs: IKeyValues[];
    private _paAggs: IKeyValues[];

    private indicatorLevels;

    static $inject = ["$scope", "perfAssessAPI", "TeacherFilter", "SchoolFilter", "Lookups", "ApiUi", "$mdDialog"];
    constructor(scope, api, tfltr, sfltr, lookups, apiUi
      , dialogs: angular.material.IDialogService) {
      this.teacherFilter = tfltr;
      this.schoolFilter = sfltr;
      this.lkp = lookups;
      this.apiUi = apiUi;
      this.api = api;
      this.dialogs = dialogs;     // angular dialogs service

      lookups.pa()
        .promise.then((cache) => {
          // what ?
        });
      // TO DO optimise the watchers -
      // if the Pa is a new object, we need to initialise
      // only if the Indicators have changed do we need to recalc the totals?
      // this deep watch is a sledgehammer!
      scope.$watch("vm.pa", this._recalc, true);

      // watch the framework - we only need to change the collapsers when the framework changes
      // this allows the collapse to be preserved across Pa's
      scope.$watch("vm.pa.Framework", () => {
        this._collapsers = {};
        this.collapseAll("C", true);
        this.collapseAll("E", true);

      });
    }
    public get pa() {
      return this._pa;
    };
    public set pa(newPa) {
      this._pa = newPa;
      this._loadPa();
      if (this.pa) {
        this._recalc();
      }
    };
    private _getIndValue = (d) => {
      let n = _.result(_.find(this.pa.IndicatorLevels,
        { 'IndicatorId': d.IndicatorId, 'Level': d.PerformanceLevel }), 'Value') as number;
      if (!isNaN(n)) {
        return n;
      };
      return null;
    };

   /**
    * Perf Assessment IO
    * savePerfAssess - save the current pa
    * showPerfAssess - load and display a pa
    * newPerfAssess - retrieve a template for a new pa
    */
    public savePerfAssess = (ev) => {
      // first check if all indicators are entered, if this is a new PerfAssess
      if (this.indicatorCount !== this.indicatorsCompleted) {
        let confirm = this.dialogs.confirm()
          .title("Save Assessment")
          .content("You have not entered all the indicators. Continue?")
          .ariaLabel("Save Assessment")
          .targetEvent(ev) // nice idea to get the animation to expand from the button - return to this
          .ok('Save anyway')
          .cancel('Cancel save');

        this.dialogs.show(confirm)
          .then(() => { this.doSavePa(); });
      } else {
        this.doSavePa();
      }
    };

    private doSavePa() {
      this.isWaiting = true;
      this.pa.post().then((data) => {
        this.pa = this._transformData(data);
        this._getTeacherAssessments(this.pa.TeacherId);
        this.isWaiting = false;
      }, (response) => {
        this.isWaiting = false;
        switch (response.status) {
          case 409:
            // concurrency
            var nv = this._transformData(response.data);
            this.isWaiting = false;
            this.apiUi.conflictReport().then( result => {
              switch (result) {
                case "overwrite":
                  this.pa.Rowversion = nv.Rowversion;
                  this.doSavePa();
                  break;
                case "discard":
                  // to do we already have all the data in nv, but
                  // can;t use it becuase it is not restangularized
                  // however merging nv back into this.pa seems need to be handled case-by-case
                  angular.merge(this.pa, nv);
                  this._loadPa();
                  break;
              };
            });
        }
      });

    };

    public newPerfAssess = () => {
      this.lkp.pickCode("paFrameworks", "TA", "Assessment Framework", "Select the assessment framework for this assessment")
        .then((code) => {
          this.isWaiting = true;
          this.api.template({ Framework: code.C, TeacherId: this.selectedTeacher.C })
            .then((data) => {
              this.pa = this._transformData(data);
              this.isWaiting = false;
            }, (error) => {
              this.isWaiting = false;
            });
        });
    };

    public showPerfAssess = (id) => {
      this.isWaiting = true;
      this.api.read(id).then((data) => {
        this.pa = this._transformData(data);
        this.isWaiting = false;
      }, (error) => {
        this.isWaiting = false;
      });
    };

    private _transformData(data) {
      // in here for now, but....
      // there needs to be a generic way of handling this for ALL dates
      // restangular interceptor perhaps?
      // http://stackoverflow.com/questions/23060283/how-to-convert-date-from-utc-to-local-time-zone-and-reverse-with-restangular
      // handled in PerfAssess class
      //data.Date = new Date(data.Date);
      return data;
    }

    private _getTeacherAssessments(teacherId) {
      var params = { TeacherID: teacherId, ColumnSet: 0, PageSize: 0 };
      this.api.filterPaged(params).then((data) => {
        this.list = data;
      });
    };

    public hasTeacher: boolean = false;
    public teacherSelected = (item) => {
      // get any existing for this teacher
      this._getTeacherAssessments(item.C);
      this.pa = null;
      this.hasTeacher = true;
    };

    public schoolSelected = (item) => {
      this.pa.SchoolNo = item.C;
      this.pa.SchoolName = item.N;
    };

    private _recalc = () => {
      if (this.selectedTeacher === undefined) {
        this.selectedTeacher = { C: this.pa.TeacherId, N: this.pa.TeacherName };
      } else {
        if (this.selectedTeacher.N !== this.pa.TeacherName ||
          this.selectedTeacher.C !== this.pa.TeacherId) {
          this.selectedTeacher = { C: this.pa.TeacherId, N: this.pa.TeacherName };
        }
      }
      if (this.selectedSchool === undefined) {
        this.selectedSchool = { C: this.pa.SchoolNo, N: this.pa.SchoolName };
      } else {
        if (this.selectedSchool.N !== this.pa.SchoolName ||
          this.selectedSchool.C !== this.pa.SchoolNo) {
          this.selectedSchool = { C: this.pa.SchoolNo, N: this.pa.SchoolName };
        }
      }
      // have to get the right length of the slice :(
      var l = this.pa.Framework.length;
      // elements SLSS.01.01 or TA.01.01 ie l = 6
      this._elemAggs = d3.nest()
        .key( (d: IIndicator) => (d.IndicatorCode).slice(0, l + 6))
        .rollup( d => d3.mean(d, this._getIndValue))
        .entries(this.pa.Indicators);
      this._comAggs = d3.nest()
        .key((d: IKeyValues) => d.key.slice(0, l + 3))
        .rollup( d => d3.mean(d, (kv: IKeyValues) => kv.values ))
        .entries(this._elemAggs);
      this._paAggs = d3.nest()
        .key((d: IKeyValues) => d.key.slice(0, l))
        .rollup(d => d3.mean(d, (kv: IKeyValues) => kv.values ))
        .entries(this._comAggs);
      this.indicatorsCompleted = this._getIndicatorsCompleted();

    };

    public hasPa() {
      if (this.pa) {
        return true;
      }
      return false;
    };

    private _getIndicatorsCompleted() {
      if (!this.hasPa()) {
        return null;
      }
      return _.reduce(this.pa.Indicators, (agg, i: IIndicator) => {
        if (i.PerformanceLevel != null) {
          agg++;
        };
        return agg;
      }, 0);
    }
    private _loadPa() {
      // these are initialisations that are required when a new Pa is loaded
      // they are not required when the Pa is changed
      // therefore, they are not in recalc
      if (!this.pa) {
        this.paNest = [];
        this._elemAggs = [];
        this._comAggs = [];
        this._paAggs = [];
        this.indicatorCount = null;
        this.indicatorsCompleted = null;
        return;
      }
      var l = this.pa.Framework.length;
      this.paNest = d3.nest()
        .key((d: IIndicator) => (d.IndicatorCode).slice(0, l + 3))
        .key((d: IIndicator) => (d.IndicatorCode).slice(0, l + 6))
        .entries(this.pa.Indicators);

      // we also need to rebuld the indicator levels now
      this.indicatorLevels = _(d3.nest()
        .key((d: IIndicator) => d.IndicatorId.toString() )
        .rollup((group) => {
          return {
            values: _.map(group, "Value"),
            codes: _.map(group, "Level"),
            tooltips: _.map(group, "Criterion")
          };
        })
        .entries(this.pa.IndicatorLevels)
      ).reduce((aggregate, item: IKeyValues) => {
        aggregate[item.key] = item.values;
        return aggregate;
        }, {});

      this.indicatorCount = this.pa.Indicators.length;
      this.indicatorsCompleted = this._getIndicatorsCompleted();
    };

    /**
     * Collapse management of hierarchies ( component, element, indicator)
     * @param level
     * @param collapse
     */
    public collapseAll(level, collapse) {
      switch (level) {
        case "C":
          this._comAggs.forEach((c: IKeyValues) => {
            this._collapsers[c.key] = collapse;
          });
          break;
        case "E":
          // if we are expanding all E's we need to expand all Cs
          if (!collapse) {
            this._comAggs.forEach((c: IKeyValues) => {
              this._collapsers[c.key] = collapse;
            });
          }
          this._elemAggs.forEach((e: IKeyValues) => {
            this._collapsers[e.key] = collapse;
          });
          break;
      }
    };

    public isCollapsed(key) {
      return this._collapsers[key];
    }

    public toggleCollapse(key) {
      this._collapsers[key] = !this._collapsers[key];
    };

    public getTotal(key) {
      if (!key) {
        return null;
      };
      var l = this.pa.Framework.length;
      switch (key.length) {
        case (l + 6):
          return _.result(_.find(this._elemAggs, { "key": key }), "values") as number;
        case (l + 3):
          return _.result(_.find(this._comAggs, { "key": key }), "values") as number;
        case ((l)):
          return _.result(_.find(this._paAggs, { "key": key }), "values") as number;
      };
      return null;
    };
  };

  angular
    .module("pineapples")
    .controller("perfAssessController", PerfAssessController);
}
