﻿namespace Pineapples.Teachers {

  interface IBindings {
    surveys: any;
  }

  class Controller implements IBindings {
    public surveys: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.surveys) {
//        console.log(this.surveys);
      }
    }

    public showSurveyDetail(schNo: string, year: number) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "survey/auditlogdialog",
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
        resolve: {
          log: ["surveyAPI", (api) => {
            return api.audit(schNo, year);
          }]
        }

      }
      this.mdDialog.show(options);
    }

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        surveys: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/surveylist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "log"];
    constructor(mdDialog: ng.material.IDialogService, public log) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentTeacherSurveyList", new Component());
}
