﻿namespace Pineapples.Teachers {

  interface IBindings {
    links: any[];
  }

  class Controller extends Pineapples.Documents.DocumentRenderer implements IBindings {
    public links: any[];
    public teacher: Teacher;

    static $inject = ["identity", "$state", "$mdDialog", "Restangular", "documentsAPI"]
    constructor(public identity: Sw.Auth.IIdentity, public state: ng.ui.IStateService
      , public mdDialog: ng.material.IDialogService
      , public Restangular: restangular.IService
      , docAPI) {
      super(docAPI, "");
    }

    public $onChanges(changes) {
    }
    public newDoc() {
      //this.state.go("site.teachers.upload", { id: this.teacherId });
      this._uploadDialog().then(() => {
      }, (error) => {
      });
    }

    private _uploadDialog() {
      let options: ng.material.IDialogOptions = {
        locals: { teacher: this.teacher },
        controller: TeacherLinkUploadController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: "teacherlink/uploaddialog"
      }
      return this.mdDialog.show(options);
    }

    private doEdit(link, event) {
      let tlink = new TeacherLink(link);
      // this has cloned the data...
      this.Restangular.restangularizeElement(null, tlink, "teacherlinks");
      this._editDialog(tlink, link);
    }

    private _editDialog(tlink: TeacherLink, link: any) {
      let options: ng.material.IDialogOptions = {
        locals: { model: tlink, modelInit: link, teacher: this.teacher },
        controller: editController,
        controllerAs: 'vm',
        bindToController: true,
        templateUrl: "teacherlink/editdialog"
      }
      return this.mdDialog.show(options);

    }

    private doDelete(link, event) {
      let confirm = this.mdDialog.confirm()
        .title('Delete Document')
        .textContent('Delete the document ' + link.docTitle + ' from the document library?')
        .targetEvent(event)
        .ok('Delete')
        .cancel('Cancel');
      this.mdDialog.show(confirm).then(() => {
        let tlink = new TeacherLink(link);
        // this has cloned the data...
        this.Restangular.restangularizeElement(null, tlink, "teacherlinks");
        tlink.remove().then(() => {
          // find the item by the deleted key
          // let wonderful lodash do the heavy lifting
          _.remove(this.links, { lnkID: link.lnkID });
        }, 
          () => { }
          // remove failed
        );
      });
    }

  }

  /**
   * Controller for the edit Dialog
   */
  class editController extends Sw.Component.ComponentEditController {
    static $inject = ["$mdDialog", "ApiUi"];
    constructor(public mdDialog: ng.material.IDialogService
      , apiUi: Sw.Api.IApiUi) {
      super(apiUi, null);
      this.isEditing = true;
    }
    public teacher: Teacher;

    public onModelUpdated(newData: any) {
      super.onModelUpdated(newData);
      this.teacher.tPhoto = newData.tPhoto;
    }
    public documentChanged(document) {
      // the document was edited in the thumbnail viewer....
      // this is the same object as model, but, the change is not relayed to the modelInit...
      this.pristine();
    }

    public closeDialog() {
      this.mdDialog.cancel();
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        links: "<",
        teacher: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/linklist";
    }
  }
  angular
    .module("pineapples")
    .component("componentTeacherLinks", new Component());

}