﻿namespace Pineapples.Survey {
  interface IBindings {
    log: any;
  }
  class Controller implements IBindings {
    public log: any;

    public $onChanges(changes) {
      if (changes.log) {
        console.log(this.log);
      }
    }
  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        log: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "survey/auditlog";
    }
  }
  angular
    .module("pineapples")
    .component("componentAuditLog", new Component());
}
