﻿namespace Pineapples.Survey {
  export class AuditLog {

    constructor(public auditDate: Date, public errors: number, public warnings: number, public items: any[]) { }

    public static create(resultset) {
      let auditDate = resultset[1].auditDate;
      let errors = resultset[1].Errors;
      let warnings = resultset[1].Errors;
      return new AuditLog(auditDate, errors, warnings, resultset[0]);
    }
  }
}
