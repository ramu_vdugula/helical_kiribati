﻿namespace Pineapples.Lookups {
  export class LookupService extends Sw.Lookups.LookupService {
		static $inject = ["$q", "lookupsAPI", "$mdDialog"];
		constructor(_q: ng.IQService, _api, dialogs: angular.material.IDialogService) {
			super(_q, _api, dialogs);
    }
    /**
     * retrieve code set for Performance Assessments
     */
    public pa() {
      var d = this._q.defer();
      if (this.cache && this.cache["paCompetencies"]) {
        d.resolve(this.cache);
      } else {
        this._api.pa().then((result) => {
          this.addToCache(result);
          d.resolve(this.cache);
        },
          error => {
            d.reject(error);
          }
        );
      };
      return d;
    }
  }
  angular
    .module('pineapples')
    .service('Lookups', LookupService);
}
