module Pineappples {

  class SchoolController {

    public Title = "School Details";

    public schoolForm: ng.IFormController;   // angular matches this to the name of the form in the view

    private schoolInit: any;
    public isWaiting = false;

    // TO DO remove unneeded injections
    static $inject = ["$scope", "$rootScope", "schoolsAPI", "$stateParams", "$state", "ApiUi", "data"];
    constructor($scope, $rootScope, private api, $stateParams, $state, private apiUi: Sw.Api.IApiUi, public School) {
      this.pristine();
    }

    public save() {
      // use the retangular method
      this.isWaiting = true;
      this.School.post().then(
        // success reponse - the matching data fields are returned from the server
        (newData) => {
          // update fields on the school record from the returned data
          // use .plain() method to ignore restangular adornments added to newData
          angular.extend(this.School, newData.plain());
          this.pristine();
          this.isWaiting = false;
        },
        (errorData) => {
          // TO DO some analysis of the error like in RowEditManager,
          // with opportunity to recover from Concurreny error, or continue if validation error,
          // or even back out by restoring this.SchoolInit
          this.isWaiting = false;
          this.apiUi.showErrorResponse(this.School, errorData).then((response) => {
            switch (response) {
              case "retry":
                // retry the save  - note this is not re-entrant becuase it is a callback from the promise
                this.save();
                break;
              case "overwrite":
                // force a save by overwriting the Rowversion and try again
                this.School.pRowversion = errorData.data.pRowversion;
                this.save();
                break;
              case "discard":
                // in a conflict, use the new data
                angular.extend(this.School, errorData.data);
                this.pristine();
                break;
            }
          });
        });
    }

    public newSchool() {
      this.School = this.api.new();
      this.pristine();
    }
    public undo() {
      angular.extend(this.School, this.schoolInit);
      this.pristine();
    }

    public refresh() {
      this.School.get().then((newData) => {
        this.School = newData;
        this.pristine();

      }, (error) => {
        });
    }

    private pristine() {
      if (this.schoolForm) {
        this.schoolForm.$setPristine();
        this.schoolInit = angular.copy(this.School.plain());      // if we want to implement Undo

      }
    }
  }
  angular
    .module('pineapples')
    .controller('SchoolController', SchoolController);
}
