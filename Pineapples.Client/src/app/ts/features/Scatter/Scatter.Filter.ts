﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      {
        field: 'schNo', name: 'Sch No',
        width: 80, pinnedLeft: true,
        cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><a  ng-click="grid.appScope.listvm.action(\'item\',col.field, row.entity);">{{row.entity[col.field]}}</a></div>'
      },
      {
        field: 'schName', name: 'Name',
        width: 250, pinnedLeft: true
      },
      {
        field: 'schType', name: 'schType', displayName: 'Type',
        width: 60, cellClass: 'gdAlignCenter'
      },
      {
        field: 'schAuth', name: 'auth', displayName: 'Auth',
        width: 80
      },
      {
        field: 'schClass', name: 'class', displayName: 'R/U',
        width: 50, cellClass: 'gdAlignLeft'
      },
    ]
  };

  class SchoolScatterParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {
    // TO DO
    constructor(lookups: any) {
      super(lookups);
    };

    public toFlashString(params: any) {
      var tt = ['ApplicationID', 'YearOfStudy', 'Gender', 'CountryApply',
        'AwardType', 'Status', 'PrioritySectorGroup', 'LevelOfStudyS1',
        'InService', 'ApplicantName'
      ];
      return this.flashStringBuilder(params, tt);
    }

    public fromFlashString(flashstring: string) {
      if (flashstring.trim().length === 0) {
        return;
      }
      let params: any = {};
      var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
      var parsed = [];
      for (var i = 0; i < parts.length; i++) {

        // test for numeric
        var s = parts[i].trim();
        if ($.isNumeric(s)) {
          var n = parseInt(s);
          if (n > 1990 && n < 2050) {
            if (!params.YearOfStudy) {
              params.YearOfStudy = n;
            }
          } else {
            // if there is only 1 part, assume its an ID number
            if (!params.ApplicationID) {
              params.ApplicationID = n;
            }
          }
          parsed.push(s);
        } else {
          // for non-numerics look explicitly for a gender, country code, Award Type, Priority Sector or Status
          var S = s.toUpperCase();
          var found = false;

          if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
            continue;
          }
          if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
            continue;
          }
          if (this.cacheFind(S, params, "districts", "District")) {     // this one uses the ID field from the award type record, not the code (satID vs satName vs satDescription in the db)
            continue;
          }
          if (this.cacheFind(S, params, "authorities", "Authority")) {
            continue;
          }

          if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
            if (!params.SchoolName) {
              params.SchoolName = s;
              parsed.push(s);
              found = true;
            }
          }
        }
      }
      return params;
    }

    protected getParamString(name, value) {
      switch (name) {
        case 'AwardType':
          return this.lookups.findByID('awardTypes', value).C;
        default:
          return value.toString();
      }

    }
  }
  class SchoolScatterFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "schoolScatterAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "schoolscatter";
      this.ParamManager = new SchoolScatterParamManager(lookups);
      this.searchApiMethod = "getScatter";
    }

    public DataSets = [
      { n: "Enrolment", v: "pSchoolRead.SchoolDataSetEnrolment" },
      { n: "Repeaters", v: "pSchoolRead.SchoolDataSetRepeaterCount" },
      { n: "Teachers", v: "pSchoolRead.SchoolDataSetTeacherCount" },
      { n: "TeachersQualCert", v: "pSchoolRead.SchoolDataSetQualCertCount" },
      { n: "TeachersQualCertPerc", v: "pSchoolRead.SchoolDataSetQualCertPerc" }
    ];

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
        }
      }
      return fltr;
    }
    public createFindConfig() {

      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "SchNo";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "SchoolType";

      config.defaults.filter = {
        BaseYear: "2014",
        XSeries: "Enrolment",
        XSeriesOffset: 0,
        YSeries: "Enrolment",
        YSeriesOffset: 1
      };

      config.current = angular.copy(config.defaults);
      config.tableOptions = "schoolscatterFieldOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        // resolve it when it the lookup is available
        d.resolve(config);
      }, (reason: any) => {
        // swallow the error
        console.log("Unable to get tableOptions for SchoolScatterFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("SchoolScatterFilter", SchoolScatterFilter);
}
