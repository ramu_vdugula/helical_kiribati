﻿namespace Pineapples.Documents {
  export class DocumentBaseController extends Pineapples.Documents.DocumentRenderer {
    public document: any;
    public height: number;
    public showMenu: boolean;

    static $inject = ["identity", "documentsAPI"]
    constructor(public identity: Sw.Auth.IIdentity, docAPI) {
      super(docAPI, "");
    }

    public $onChanges(changes) {
    }

    public $onInit() {
      this.showMenu = (this.showMenu === undefined ? true : this.showMenu);
    }

  }
  class Component implements ng.IComponentOptions {
    public bindings:any = {
      document: '<',
      height: '<',
      showMenu: "<",
      documentChanged: "&"
    };
    public controller: any = DocumentBaseController;
    public controllerAs: string = "vm";
    public templateUrl: string = "teacherlink/thumbnail";

  }
  angular
    .module("pineapples")
    .component("componentThumbnail", new Component());
}