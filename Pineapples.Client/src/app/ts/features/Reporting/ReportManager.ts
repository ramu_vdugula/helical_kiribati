﻿namespace Pineapples.Reporting {

  export interface IReportDef {
    key: string;
    provider: string;
    endpoint: string;
    description: string;
    name: string;
    canRenderExcel: boolean;
    canRenderPdf: boolean;
    path: string;       // the path to the report in the hierarchy
    promptForParams: string;
  }

  export class Report {
    public endpoint: string;
    public format: string;
    public provider: string;
    public params = { schNo: null, svyYear: null };           // the report arguments

    // this function is supplied - e.g. it will be a function returning a findConfig
    public getParams: (context?: any) => any;
    public getPostData: (context?: any) => any = (context?: any) => {
      return {
        caption: this.caption,
        filter: this.getParams(context)
      }
    };
    public name: string;
    public caption: string;
    public configure: () => boolean = () => {
      return true;
    };
    public description: string;
    public canRenderExcel: boolean = true;
    public canRenderPdf: boolean = true;
    public path: string;
    promptForParams: string = null;   // always / never default is prompt when mandatory params are null

  }

  /**
   * Represents a running report
   **/
  export interface IReportJob {
    promise: ng.IPromise<any>;    // the report request to the reort server
    status: string;               // statu sof the job
    report: Report;               // the Report object
    completed(): boolean;
    onSave: (any) => void;
    onError: (any) => void;

    error: any;
  }

  export interface IReportManagerService {
    clearComplete(): void;
    clearAll(): void;
    createJob(rpt: Report, context?: any): IReportJob;
    jobs: IReportJob[];

    // management of report definitions
    reportDefs: any;
    loadReportDefs: () => void;
    loadJasperReportDefs: (folder) => ng.IPromise<any>;
    getReportFromDefn: (defn: IReportDef) => Report;
    getReport: (defnpath: string) => Report;
    getDefn: (defnpath: string) => IReportDef;
  }

  /**
   * Represents a particular repport provider e.g. Jasper, SSrS
   * Url and request structure details will be specific 
  **/
  export interface IReportProvider {
    createJob(rpt: Report, context?: any): IReportJob;
  }

  class ReportJob implements IReportJob {

    constructor(private _filesaver) { }

    private _promise: ng.IPromise<any>;
    public get promise() {
      return this._promise;
    };
    public set promise(p) {
      this._promise = p;
      this.status = "downloading";
    }

    public status: string;
    public report: Report;

    public error: any;

    public completed() {    // is this needed?
      return (this.status === "complete");
    }

    public onSave = (data: any) => {
      // shows how to get the default name sent by the server, and the mime type
      let mimetype = data.headers("Content-Type");
      let filename: string = data.headers("Content-Disposition");
      // http://stackoverflow.com/questions/23054475/javascript-regex-for-extracting-filename-from-content-disposition-header/23054920
      let fileregex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
      // this pattern will return the name (possibly enclosed in quotes) as capture group 1
      // and any quoting char as capture group 2
      let regResult: RegExpExecArray = fileregex.exec(filename);
      // the resulting array has the input string at [0], then the 'match groups'
      filename = regResult[1];
      if (regResult.length === 3) {
        // remove the quote
        filename = filename.replace(regResult[2], "");
        filename = filename.replace(regResult[2], "");      // 
      }
      let blob = new Blob([data.data], { type: mimetype });
      // this seems the preferred way to get the link to open in a new window, 
      // but the user would have to change their popupblocker settings for this to work....
      // don't other for now - open from the command line
      //let fileURL = URL.createObjectURL(blob);
      //var link = document.createElement('a');
      //link.href = fileURL;
      //link.target = "_blank";
      //link.click();
      //setTimeout(() => {
      //  // For Firefox it is necessary to delay revoking the ObjectURL
      //  window.URL.revokeObjectURL(fileURL);
      //}, 100);
      this._filesaver.saveAs(blob, filename);
      this.status = "complete";
      this.error = null;
    };

    public onError = (response) => {
      console.log(response);
      this.status = "error";
      // we ask for an array buffer to handle binary "success" responses, 
      // but the error response is just html - oly now, its resented as arrayBuffer
      // https://developers.google.com/web/updates/2012/06/How-to-convert-ArrayBuffer-to-and-from-String
      // explains this technique for getting the string back - note the assumption that ssrs is returning utf-8
      // but (TO DO?) this could be determined from the response content-type header

      this.error = String.fromCharCode.apply(null, new Uint8Array(response.data));
    }
  }

  // TO DO Ssrs Support?? unlikely to be required
  class SsrsProvider implements IReportProvider {

    constructor(public http: ng.IHttpService, private _filesaver) { }

    public createJob(report: Report, context?: any) {

      let job = new ReportJob(this._filesaver);

      let url: string;

      if (report.endpoint) {
        // we have a specific api endpoint for this report
        url = "api/" + report.endpoint +
          (report.format ? ("/" + report.format) : "");
      } else {
        // use the generic ssrs endpoint - requires only authentication
        // let path = report.path.replace(" ", "+");
        url = "api/ssrs/report?path=" + report.path +
          (report.format ? ("&format=" + report.format) : "");
      }

      job.promise = this.http({
        method: 'POST',
        data: report.getPostData(context),
        cache: false,
        url: url,
        responseType: 'arraybuffer'
      });

      return job;
    }

  }

  class JasperProvider implements IReportProvider {

    constructor(public http: ng.IHttpService, private _filesaver, private mdDialog: ng.material.IDialogService) { }

    public createJob(report: Report, instanceInfo?: any) {
      let job = new ReportJob(this._filesaver);

      switch (report.promptForParams) {

        case "never":
          // we don't need to get the params definitions
          let rptPromise = this._reportPromise(report, instanceInfo);
          rptPromise.then(job.onSave, job.onError);
          job.promise = rptPromise;
          return job;
        default:
          let httpPromise = this.http({
            method: 'POST',
            data: { reportName: report.path },
            cache: false,
            url: "api/jasper/reportparams"
          });
          httpPromise.then((resp) => {

            let ok = false;
            let inputInfo = <jasper.IinputControlResponse>resp.data;
            let predicate = (input) => {
              if (instanceInfo.parameters[input.id] === undefined
                || instanceInfo.parameters[input.id] === null
                || instanceInfo.parameters[input.id] === ""
              ) {
                return true;
              }
              return false;
            };
            if (resp.status === 204) {
              // jasper returns 204 no data rather than an empty array when there are no parameters
              ok = true;
            } else {
              let mandatoryInputs = _.filter(inputInfo.inputControl, { mandatory: true });
              let missingMandatoryInputs = _.filter(mandatoryInputs, predicate);
              // if all the mandatory inputs are supplied, AND promptForParms != always , nothng to do - straight to the report
              // also nothing to do if there are no params at all
              ok = (missingMandatoryInputs.length == 0 && !(report.promptForParams == "always"));
            }
            if (ok) {
              let rptPromise = this._reportPromise(report, instanceInfo);
              rptPromise.then(job.onSave, job.onError);
              return rptPromise;
            }
            // otherwise we need the material dialog
            let dlgPromise = this._dialogPromise(inputInfo, instanceInfo);
            dlgPromise.then(() => {
              let rptPromise = this._reportPromise(report, instanceInfo);
              rptPromise.then(job.onSave, job.onError);
              return rptPromise;
            },
              // error response ie the dialog was cancelled
              () => {
                job.status = "";
              }
            );
            return dlgPromise;
          },
            // fail branch
            job.onError
          );

          job.promise = httpPromise;
          return job;
      }
    };

    private _dialogPromise(inputInfo, instanceInfo) {
      let options: ng.material.IDialogOptions = {
        locals: { inputInfo: inputInfo, instanceInfo: instanceInfo },
        controller: jasperInputsController,
        controllerAs: 'dvm',
        bindToController: true,
        templateUrl: "reports/jasperinputdialog"
      }
      return this.mdDialog.show(options);
    }

    private _reportPromise(report, instanceInfo) { // if the 
      let url: string;

      if (report.endpoint) {
        // we have a specific api endpoint for this report
        url = "api/" + report.endpoint;
      } else {
        // let path = report.path.replace(" ", "+");
        url = "api/jasper/report";
      }

      let data = {
        path: report.path,
        format: report.format,
        args: null,
        filename: null
      };

      if (instanceInfo) {
        data.args = instanceInfo.parameters;
        data.filename = instanceInfo.filename;
      }
      let reportPromise = this.http({
        method: 'POST',
        data: data,
        cache: false,
        url: url,
        responseType: 'arraybuffer'
      });
      return reportPromise;
    };
  }

  class jasperInputsController extends Sw.Component.MdDialogController {
    public inputInfo: jasper.IinputControlResponse;
    public instanceInfo: any;
  }

  /**
   * The Report Manager services is responsible for runnng reports and managing report definitions
  **/
  class Service implements IReportManagerService {

    static $inject = ["$http", "FileSaver", "$mdDialog"]
    constructor(public http: ng.IHttpService, private _filesaver: any, private mdDialog: ng.material.IDialogService) { }

    public reportDefs: any;

    public getDefn(defnpath: string) {
      let path = defnpath.split("/");
      let d = this.reportDefs;
      for (var i = 0; i < path.length; i++) {
        d = d[path[i]];
      }
      if (d && !(<IReportDef>d).path) {
        d.path = defnpath;
      }
      return d;
    }
    // return a report object from its defn
    public getReportFromDefn(defn: IReportDef) {
      let rpt = new Report();
      angular.extend(rpt, defn);
      if (!rpt.format) {
        if (rpt.canRenderExcel === false) {
          rpt.format = "PDF";
        } else {
          if (rpt.canRenderPdf === false) {
            rpt.format = "EXCEL";
          }
        }
      }
      return rpt;
    }

    public getReport(defnpath: string) {
      return this.getReportFromDefn(this.getDefn(defnpath));
    }

    // load up report definitions, from the reportDefs folder tree
    public loadReportDefs() {
      this.http.get("api/reportdefs").then(
        (response) => {
          this.reportDefs = response.data;
        }, (error) => {
          console.log("Could not load reportdefs");
          console.log(error);
        });
    }

    /**
   * load all the jasper report definitions in a given path
   * @param path
   */
    public loadJasperReportDefs(path) {
      let httpPromise = this.http({
        method: 'POST',
        data: { folder: path },
        cache: false,
        url: "api/jasper/reportlist"
      });
      return httpPromise.then((response:ng.IHttpPromiseCallbackArg<jasper.IResourceLookup>) => {
        if (response.status == 204) {
          // jasper returns no data 204 when the path is there but empty
          return [];
        }
        if (response.status == 404) {
          // jasper returns no data 204 when the path is there but empty
          return [];
        }
        return response.data.resourceLookup;    // resourceLookup is the property of the response that holds the array
      });
    }

    public clearComplete() {
      let newJobs: IReportJob[] = [];
      this.jobs.forEach((job) => {
        if (!job.completed()) {
          newJobs.push(job);
        };
      });
      this.jobs = newJobs;
    }
    public clearAll() {
      this.jobs = [];
    }
    public jobs: IReportJob[];

    public createJob(report: Report, instanceInfo?: any) {
      let provider: IReportProvider;

      switch (report.provider) {
        case "ssrs":
          provider = new SsrsProvider(this.http, this._filesaver);
          break;
        case "jasper":
          provider = new JasperProvider(this.http, this._filesaver, this.mdDialog);
          break;
        default:
          provider = new JasperProvider(this.http, this._filesaver, this.mdDialog);
          break;
      }

      return provider.createJob(report, instanceInfo);
    }
  }

  angular
    .module("pineapples")
    .service("reportManager", Service)
    .run(["reportManager", (reportManager: IReportManagerService) => {
      reportManager.loadReportDefs();
    }]);




}
