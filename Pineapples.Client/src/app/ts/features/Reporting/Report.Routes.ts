﻿// Reports Routes
namespace Pineappples.Reports {

  let routes = function ($stateProvider) {
    //var mapview = 'TeacherMapView';

    // root state for 'reports' feature
    let state: ng.ui.IState;
    let statename: string;

    // base reports state
    state = {
      url: "^/reports",
      abstract: true
    };
    $stateProvider.state("site.reports", state);

    // upload state
    state = {
      url: "^/reports/test",

      views: {
        "@": {
          component: "reportDefCard"
        }
      },
    };
    $stateProvider.state("site.reports.test", state);

    state = {
      url: "^/reports/all",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.reports.all", state);

    // reload state for testing
    state = {
      url: '^/reports/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("reports/reportdefcomponent/card");
        $templateCache.remove("reports/reportdefcomponent/button");
        $templateCache.remove("reports/reportdefcomponent/icon");
        $templateCache.remove("reports/reportdefcomponent/row");
        $state.go("site.reports.test");
      }]
    };
    statename = "site.reports.reload";
    $stateProvider.state(statename, state);

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}