﻿namespace Pineapples.SchoolAccreditations {

  interface IBindings {
    model: SchoolAccreditation;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: SchoolAccreditation;

    static $inject = ["ApiUi", "schoolAccreditationsAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentSchoolAccreditation", new Sw.Component.ItemComponentOptions("schoolaccreditation", Controller));
}