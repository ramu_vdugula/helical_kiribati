namespace Pineappples.SchoolAccreditations {

  let routes = function ($stateProvider) {
    var featurename = 'SchoolAccreditations';
    var filtername = 'SchoolAccreditationFilter';
    var templatepath = "schoolaccreditation";
    var tableOptions = "schoolaccreditationFieldOptions";
    var url = "schoolaccreditations";
    var usersettings = null;
    //var mapview = 'SchoolMapView';

    // root state for 'schoolaccreditation' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath);

    // default 'api' in this feature is schoolaccreditationsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "schoolAccreditationsAPI";

    let statename = "site.schoolaccreditations";
    $stateProvider.state("site.schoolaccreditations", state);

    // takes an editable list state
    state = Sw.Utils.RouteHelper.editableListState("SchoolAccreditation");
    statename = "site.schoolaccreditations.list";
    state.url = "^/schoolaccreditations/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/schoolaccreditations/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("schoolaccreditation/item");
        $templateCache.remove("schoolaccreditation/searcher");
        $state.go("site.schoolaccreditations.list");
      }]
    };
    statename = "site.schoolaccreditations.reload";
    $stateProvider.state(statename, state);

    // state for a high level dashboard
    state = {
      url: "^/schoolaccreditations/dashboard",
      data: {
        permissions: {
          only: 'SchoolReadX'
        }
      },
      views: {
        "@": {
          component: "schoolAccreditationDashboardComponent"
        }

      },
    };
    $stateProvider.state("site.schoolaccreditations.dashboard", state);

    // chart table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    // Sw.Utils.RouteHelper.addMapState($stateProvider, featurename, mapview);

    // new - state with a custom url route
    state = {
      url: "^/schoolaccreditations/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.schoolaccreditations.list": {
          component: "componentSchoolAccreditation"
        }

      },
      resolve: {
        model: ['schoolAccreditationsAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.schoolaccreditations.list.new", state);

    state = {
      url: "^/schoolaccreditations/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "School_Accreditations",  // actually School Accreditation folder in Jasper but URI is underscore
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.schoolaccreditations.reports", state);

    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/schoolaccreditations/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.schoolaccreditations.list": {
          component: "componentSchoolAccreditation"
        }
      },
      resolve: {
        model: ['schoolAccreditationsAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.schoolaccreditations.list.item", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}
