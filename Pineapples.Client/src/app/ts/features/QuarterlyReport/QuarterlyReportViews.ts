namespace Pineapples.QuarterlyReports {

  let modes = [
    {
      key: "Quarterly Aggregate Days of Attendance",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'AggDaysAtt',
            name: 'AggDaysAtt',
            displayName: 'Agg. Days of Attendance',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysAttM',
            name: 'AggDaysAttM',
            displayName: 'Agg. Days of Attendance (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysAttF',
            name: 'AggDaysAttF',
            displayName: 'Agg. Days of Attendance (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Average Days of Attendance",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'AvgDailyAtt',
            name: 'AvgDailyAtt',
            displayName: 'Avg. Daily of Attendance',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AvgDailyAttM',
            name: 'AvgDailyAttM',
            displayName: 'Avg. Daily of Attendance (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AvgDailyAttF',
            name: 'AvgDailyAttF',
            displayName: 'Avg. Daily of Attendance (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Aggregate Membership",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'AggDaysMem',
            name: 'AggDaysMem',
            displayName: 'Aggregate Days of Membership',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysMemM',
            name: 'AggDaysMemM',
            displayName: 'Aggregate Days of Membership (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysMemF',
            name: 'AggDaysMemF',
            displayName: 'Aggregate Days of Membership (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Average Membership",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'AvgDailyMem',
            name: 'AvgDailyMem',
            displayName: 'Average Daily of Membership',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AvgDailyMemM',
            name: 'AvgDailyMemM',
            displayName: 'Average Daily of Membership (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AvgDailyMemF',
            name: 'AvgDailyMemF',
            displayName: 'Average Daily of Membership (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Absence",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'AggDaysAbs',
            name: 'AggDaysAbs',
            displayName: 'Aggregate Days of Absence',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysAbsM',
            name: 'AggDaysAbsM',
            displayName: 'Aggregate Days of Absence (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'AggDaysAbsF',
            name: 'AggDaysAbsF',
            displayName: 'Aggregate Days of Absence (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Dropouts",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'Dropout',
            name: 'Dropout',
            displayName: 'Dropout',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'DropoutM',
            name: 'DropoutM',
            displayName: 'Dropout (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'DropoutF',
            name: 'DropoutF',
            displayName: 'Dropout (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Transfers In",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'Trout',
            name: 'Trout',
            displayName: 'Transfers Out',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TroutM',
            name: 'TroutM',
            displayName: 'Transfers Out (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TroutF',
            name: 'TroutF',
            displayName: 'Transfer Out (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Transfers Out",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'Trout',
            name: 'Trout',
            displayName: 'Transfers Out',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TroutM',
            name: 'TroutM',
            displayName: 'Transfers Out (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TroutF',
            name: 'TroutF',
            displayName: 'Transfer Out (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    {
      key: "Quarterly Enrolments",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'TotEnrolToDate',
            name: 'TotEnrolToDate',
            displayName: 'Total Enrol to Date',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TotEnrolToDateM',
            name: 'TotEnrolToDateM',
            displayName: 'Total Enrol to Date (Male)',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'TotEnrolToDateF',
            name: 'TotEnrolToDateF',
            displayName: 'Total Enrol to Date (Female)',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    }
  ]; // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['QuarterlyReportFilter', pushModes]);
}
