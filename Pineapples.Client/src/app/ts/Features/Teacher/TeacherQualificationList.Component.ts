﻿namespace Pineapples.Teachers {

  interface IBindings {
    qualifications: any;
    teacher: Teacher;
  }

  class Controller extends Sw.Component.ListMonitor implements IBindings {
    public qualifications: any[];
    public teacher: Teacher;
    public parentkeyname = "tID";

    public isWaiting: boolean;

    static $inject = ["$scope", "$mdDialog", "$location", "$state", "Restangular", "ApiUi", "Lookups"];
    constructor(
      private scope: Sw.IScopeEx
      , public mdDialog: ng.material.IDialogService
      , private $location: ng.ILocationService
      , public $state: ng.ui.IStateService
      , public restangular: restangular.IService
      , private apiUi: Sw.Api.IApiUi
      , public lookups: Sw.Lookups.LookupService) {
      super();
      this.monitor(scope, "teacherqualifications", "trID");
      this.multiSelect = false;
    }

    public monitoredList() {
      return this.qualifications;
    }
    public isListMember(newData) {
      return (this.teacher && this.teacher._id() == newData[this.parentkeyname]);
    }

    public $onChanges(changes) {
      if (changes.qualifications) {
        //        console.log(this.surveys);
      }
    }

    public showQualification(qualification) {
      let locals: IQualificationDialogLocals = {
        model: this.clone(qualification)
      }
      this.mdDialog.show(
        QualificationDialog.options(locals)
      );
    }
    private deleteConfirm(q) {
      let qname = this.lookups.byCode("teacherQuals", q.trQual).N;
      return this.mdDialog.confirm()
        .title("Delete Qualification")
        .content("Delete qualification: " + qname + "?")
        .clickOutsideToClose(true)
        .ok("Delete")
        .cancel("Cancel");
    }
    public deleteQualification(q) {
      this.mdDialog.show(this.deleteConfirm(q))
        .then(() => {
          this.isWaiting = true;
          // clone is a bit of overkill here
          // but it does allow restangular to now handle the deletion
          let tq = this.clone(q);
          tq.remove()
            .then(() => {
              this.isWaiting = false;
            })
            .catch((error) => {
              this.isWaiting = false;
              this.apiUi.showErrorResponse(q, error);
            })
        });
    }

    // record a new qualification for this teacher
    public new() {
      // set up the defaults for the new qualification
      let model = {
        trYear: new Date().getFullYear(),
        tID: this.teacher._id()
      }
      this.showQualification(model);
    }

    // this function will clone a qualification row for editing
    // this makes it easier to support 'undo' without having to recreate the source object
    // Instead, any changes that are made are indirecty propogated back to the source object (ie, row)
    //  by the ListMonitor.
    //
    // It doesn't matter if the input object is a TeacherQualifiaction object, or if it 
    // is restangularized - the output object will need to be created and restangularized anyway
    // (noting that you cannot successfully 'clone' a restangularized object becuase the 
    // restangular methods are bound to the source object
    private clone(q) {
      let tq: TeacherQualification;
      // drop any reqstangular stuff
      let modeldata = (q.plain ? q.plain() : q);
      // create the new teacher object
      tq = TeacherQualification.create(modeldata);
      //restangularize it
      this.restangular.restangularizeElement(null, tq, "teacherqualifications");
      return tq;
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        qualifications: '<',
        teacher: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/qualificationlist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "qualification"];
    constructor(mdDialog: ng.material.IDialogService, public payslip) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentTeacherQualificationList", new Component());
}
