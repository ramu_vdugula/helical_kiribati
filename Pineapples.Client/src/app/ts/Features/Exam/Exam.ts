﻿namespace Pineapples.Exams {

  export class Exam extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(examData) {
      super();
      this._transform(examData);
      angular.extend(this, examData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let exam = new Exam(resultSet);
      return exam;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).exCode;
    }
    public _type() {
      return "exam";
    }
    public _id() {
      return (<any>this).exID
    }

    public _transform(newData) {
      // convert these incoming data values
      // transformDates is deprecated: http interceptor now converts dates on load
      //this._transformDates(newData, ["exDate"]);
      return super._transform(newData);
    }

    //public Enrollments: any[];
    //public Scorecards: any[];
    //public stuPhoto: string;    // the current photo

    //public get currentPhoto() {
    //  return _.find(this.Documents, { docID: this.stuPhoto });
    //}
  }
}