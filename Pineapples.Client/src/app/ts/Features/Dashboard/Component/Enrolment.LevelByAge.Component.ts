﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    // reference to the host dashboard is from the 'require' on the componentOptions
    public dashboard: SchoolsDashboard;
 
    // groups 
    public tabAge_Level: CrossFilter.Group<Enrolments.IxfData, string, any>;
    public tabAge_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, any>;
    public tot_Level: CrossFilter.Group<Enrolments.IxfData, string, any>;
    public tot_EdLevel: CrossFilter.Group<Enrolments.IxfData, string, any>;

    public onDashboardReady() {

      let xFilter = this.dashboard.xFilter;
      //crosstabulation by age and class level
      this.tabAge_Level = xFilter.xReduce(this.dashboard.dimAge, xFilter.getPropAccessor("ClassLevel"), Enrolments.va);
      this.tot_Level = xFilter.xReduceTotal(this.dashboard.dimAge, xFilter.getPropAccessor("ClassLevel"), Enrolments.va);

      //crosstabulation by age group and education level
      let edlAccessor = (d) => {
        return this.lookups.byCode("levels", d.ClassLevel,"L");
      }
      // crosstabs by education level
      this.tabAge_EdLevel = xFilter.xReduce(this.dashboard.dimAgeGroup, edlAccessor, Enrolments.va);
      this.tot_EdLevel = xFilter.xReduceTotal(this.dashboard.dimAgeGroup, edlAccessor, Enrolments.va);
    }

    public onOptionChange() {
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentLevelByAge", new EnrolComponent(Controller, "EnrolmentLevelByAge"));
}
