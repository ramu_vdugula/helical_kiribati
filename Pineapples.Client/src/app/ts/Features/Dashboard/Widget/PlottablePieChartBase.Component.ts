﻿namespace Pineappples.Dashboards {
  
  class Controller {
    private chart: any;
    public isSelected: boolean;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsColors: any;
    public datasetsVal: string;

    static $inject = ['$timeout', '$element', 'Lookups'];
    constructor(
      protected timeout: ng.ITimeoutService,
      public element) {
    }

    public $onChanges(changes) {
      this.timeout(() => {
        !changes.datasets && this.chart ? this.chart.redraw() : this.render();
      }, 50, true);
    }

    private render() {
			if (!this.datasets) {
				return;
			}
      const title = new Plottable.Components.TitleLabel(this.chartTitle);
      const scale = new Plottable.Scales.Linear();
      const colorScale = new Plottable.Scales.InterpolatedColor();
      colorScale.range(this.datasetsColors);

      const plot = new Plottable.Plots.Pie()
        .addDataset(new Plottable.Dataset(this.datasets))
        .sectorValue( d => d[this.datasetsVal], scale)
        .attr("fill", d => d[this.datasetsVal], colorScale)

      const cs = new Plottable.Scales.Color()
      cs.range(this.datasetsColors)
        .domain(this.datasetsMetadata);
      const legend = new Plottable.Components.Legend(cs);
      legend.maxEntriesPerRow(1);

      this.chart = new Plottable.Components.Table([
        [null, title, null],
        [null, plot, legend],
      ]);

      const svg = $(this.element).find("svg.plottablePieChart");
      const d3svg = d3.selectAll(svg.toArray());
      svg.children().remove();

      this.chart.renderTo(d3svg);
      svg.attr('width', '100%');
      svg.attr('height', '100%');
      this.chart.redraw();

      window.addEventListener("resize", () => {
        this.isSelected && this.chart.redraw();
      });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      isSelected: "<",   // needed to trigger re-render
      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsColors: "<",
      datasetsVal: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `<svg class="plottablePieChart" style="height: 100%; width: 100%" ></svg>`;
  }

  angular
    .module("pineapples")
    .component("plottablePieChartBase", new Component());
}