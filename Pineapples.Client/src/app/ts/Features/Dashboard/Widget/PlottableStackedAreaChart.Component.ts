﻿/* <PlottableStackedAreaChart />
 * Stacked Area Chart Component
 *
 * Attibutes
 * =========
 *
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasetsMetadata: metadata for datasets   i.e.  ["Female", "Male"]
 * datasets: data for chart - list of objects containing { dataset, meta, color }
         i.e. [
                {
                  "dataset": [
                    {
                      "year": 2013,
                      "enrol": 62
                    },
                    {
                      "year": 2014,
                      "enrol": 59
                    },
                    {
                      "year": 2015,
                      "enrol": 56
                    },
                    {
                      "year": 2016,
                      "enrol": 61
                    },
                    {
                      "year": 2017,
                      "enrol": 68
                    },
                    {
                      "year": 2018,
                      "enrol": 117
                    }
                  ],
                  "meta": "Female",
                  "color": "#34b24c"
                },
                {
                  "dataset": [
                    {
                      "year": 2013,
                      "enrol": 57
                    },
                    {
                      "year": 2014,
                      "enrol": 69
                    },
                    {
                      "year": 2015,
                      "enrol": 61
                    },
                    {
                      "year": 2016,
                      "enrol": 66
                    },
                    {
                      "year": 2017,
                      "enrol": 71
                    },
                    {
                      "year": 2018,
                      "enrol": 126
                    }
                  ],
                  "meta": "Male",
                  "color": "#ffa500"
                }
              ]

 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "year"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "enrol"
 */


namespace Pineappples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;

    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption">
              <md-option ng-value="opt" ng-repeat="opt in vm.viewOptions()">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
            <plottable-stacked-area-chart-base 
                chart-title="{{vm.chartTitle}}"
                is-selected="vm.isSelected()"
                datasets="vm.datasets"
                datasets-metadata="vm.datasetsMetadata"
                datasets-x="{{vm.datasetsX}}"
                datasets-y="{{vm.datasetsY}}" />

          </child-body>
        </dashboard-child>
`;
  }

  angular
    .module("pineapples")
    .component("plottableStackedAreaChart", new Component());
}
