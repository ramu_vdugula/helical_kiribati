﻿namespace Pineappples.Dashboards {
    
  class Controller {
    static $inject = ['Lookups'];
    constructor(public lookups) { }

    reduceIterator: any; // = (x) => x.EnrolF + x.EnrolM;
    group = _.memoize(() => this.dim.group().reduceSum(this.reduceIterator));
    valueAccessor = (x) => x.value; 
    aspectRatio = 2;
    
    // Bindings
    dim: any;
    selectedKey: any;
    onClick: any;

    //$onChanges(changes) {
    //  console.log('PieChartBase.$onChanges', changes);
    //}

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        dim: "<",
        selectedKey: "<",
        onClick: "<",
        reduceIterator: "<"
       };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/PieChartBase`;
    }
  }

  angular
    .module("pineapples")
    .component("pieChartBase", new Component());
}