﻿namespace Pineapples.Students {

  export class Student extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(studentData) {
      super();
      this._transform(studentData);
      angular.extend(this, studentData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let student = new Student(resultSet);
      return student;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).stuGiven;
    }
    public _type() {
      return "student";
    }
    public _id() {
      return (<any>this).stuID
    }

    public _transform(newData) {
      // convert these incoming data values
      // transformDates is deprecated: http interceptor now converts dates on load
      //this._transformDates(newData, ["stuDoB"]);
      return super._transform(newData);
    }

    public Enrollments: any[];
    //public Scorecards: any[];
    //public stuPhoto: string;    // the current photo

    //public get currentPhoto() {
    //  return _.find(this.Documents, { docID: this.stuPhoto });
    //}
  }
}