﻿// Students Routes
namespace Pineappples.Students {

  let routes = function ($stateProvider: ng.ui.IStateProvider) {
    var featurename = 'Students';
    var filtername = 'StudentFilter';
    var templatepath = "student";
    var tableOptions = "studentFieldOptions";
    var url = "students";
    var usersettings = null;
    //var mapview = 'StudentMapView';

    // root state for 'students' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath, url, usersettings, tableOptions);

    // default 'api' in this feature is studentsAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "studentsAPI";

    let statename = "site.students";
    $stateProvider.state(statename, state);

    // takes a list state
    state = Sw.Utils.RouteHelper.listState("Student");
    statename = "site.students.list";
    state.url = "^/students/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/students/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("student/item");
        $templateCache.remove("student/searcher");
        $state.go("site.students.list");
      }]
    };
    statename = "site.students.reload";
    $stateProvider.state(statename, state);

    // chart, table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

    // new - state with a custom url route
    state = {
      url: "^/students/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'SchoolWriteX'
        }
      },
      views: {
        "actionpane@site.students.list": {
          component: "componentStudent"
        }
      },
      resolve: {
        model: ['studentsAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.students.list.new", state);

    state = {
      url: "^/students/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Students",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.students.reports", state);    

    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/students/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.students.list": {
          component: "componentStudent"
        }
      },
      resolve: {
        model: ['studentsAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.students.list.item", state);

    // upload file / image state
    //state = {
    //  url: "^/upload/students/{id}",
    //  params: { id: null, columnField: null, rowData: {} },
    //  views: {
    //    "@": {
    //      component: "teacherLinkUploadComponent"
    //    }
    //  },
    //  resolve: {
    //    student: ['studentsAPI', '$stateParams', function (api, $stateParams) {
    //      return api.read($stateParams.id);
    //    }]
    //  }
    //};
    //$stateProvider.state("site.students.upload", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}