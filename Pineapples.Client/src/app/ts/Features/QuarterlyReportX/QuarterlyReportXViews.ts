namespace Pineapples.QuarterlyReportsX {

  let modes = [
    {
      key: "Audit Data",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'pCreateUser',
            name: 'pCreateUser',
            displayName: 'Created By',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'pCreateDateTime',
            name: 'pCreateDateTime',
            displayName: 'Created',
            cellFilter: "date:medium",
            cellClass: 'gdAlignRight'
          },
          {
            field: 'pEditUser',
            name: 'pEditUser',
            displayName: 'Edited By',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'pEditDateTime',
            name: 'pEditDateTime',
            displayName: 'Edited',
            cellFilter: "date:medium",
            cellClass: 'gdAlignRight'
          },          
        ]
      }
    },
    {
      key: "Enrolment by Quarters",
      columnSet: 0,
      gridOptions: {
        columnDefs: [
          {
            field: 'StudentEnrolBeginning',
            name: 'StudentEnrolBeginning',
            displayName: 'Student Enrolled at Beginning',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'StudentEnrolQ1',
            name: 'StudentEnrolQ1',
            displayName: 'Student Enrolled in Quarter 1',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'StudentEnrolQ2',
            name: 'StudentEnrolQ2',
            displayName: 'Student Enrolled in Quarter 2',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'StudentEnrolQ3',
            name: 'StudentEnrolQ3',
            displayName: 'Student Enrolled in Quarter 3',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'StudentEnrolQ4',
            name: 'StudentEnrolQ4',
            displayName: 'Student Enrolled in Quarter 4',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'StudentEnrolEnd',
            name: 'StudentEnrolEnd',
            displayName: 'Student Enrolled at End',
            cellClass: 'gdAlignRight'
          },
        ]
      }
    },
    
  ]; // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['QuarterlyReportXFilter', pushModes]);
}
