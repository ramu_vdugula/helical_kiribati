﻿namespace Pineapples.QuarterlyReports {

  class Controller {
    
    constructor() { }

  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "quarterlyreport/Dashboard";
    }
  }

  angular
    .module("pineapples")
    .component("quarterlyReportsDashboardComponent", new ComponentOptions());
}