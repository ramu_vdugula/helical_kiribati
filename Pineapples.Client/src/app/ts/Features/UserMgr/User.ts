﻿namespace Sw.Auth {

  interface IClaim {
    ClaimType: string;
    ClaimValue: string;
  }

  export class User extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    static $inject = ["$http", "FileSaver", "data"];
    constructor(private _http, private _filesaver: any, data) {
      super();
      this._transform(data);
      angular.extend(this, data);
      console.log(this.PermissionGrid);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any, injector: ng.auto.IInjectorService) {
      let app = injector.instantiate(User, { data: resultSet });
      return app;
    };

    public PermissionHash: string;
    public PermissionGrid: any[];

    public _filterOn: string;

    public get FilterOn() {
      return this._filterOn;
    }
    public set FilterOn(newfilter) {
      if (newfilter != this._filterOn) {
        this._filterOn = newfilter;
        this.FilterValue = null;
      }
    }
    public FilterValue: string;


    // IEditable
    public _name() {
      return (<any>this).UserName;
    }
    public _type() {
      return "user";
    }
    // id is the primary key, but also UserName and Email are unique
    public _id() {
      return (<any>this).Id;
    }
    // related data in custom properties
    public Roles: any[];          // single row is returned as a 1 element array when we retrn DataTab

    public _transform(newData) {
      // convert these incoming data values
      // ASP.NET Identity returns Roles as an array of role objects {RoleId; UserId}
      // we need this just to be a string array of RoleId ; simpler to edit; and easier to deal with when saving
      newData.Roles = (<Array<any>>newData.Roles).map(v => v.RoleId);
      // next we have to convert the Permission clim into its grid of permission
      let permissionClaim = _.find(newData.Claims, { ClaimType: "Permission" });
      if (permissionClaim) {
        newData.PermissionHash = permissionClaim["ClaimValue"];
      } 

      let permissionFilter = _.find(newData.Claims, (c: IClaim) => c.ClaimType.indexOf("filter") == 0);
      if (permissionFilter) {
        this._filterOn = permissionFilter.ClaimType;
        this.FilterValue = permissionFilter.ClaimValue;
      }

      newData.PermissionGrid = this.permissionHashToGrid(newData.PermissionHash);
      return super._transform(newData);
    }

    public _beforeSave() {

      this.PermissionHash = this.permissionGridToHash();

    // normally we don't send arrays properties back to be saved - because these are 
    // most often related records. however this object can save its claims and roles, which are sent back as arrays
    // So - override the default implementation to do nothing.
      return this;
    }

    /* the output of this function is a collection
    {
      enrolment: {}
    }
    */

    private permissionHashToGrid(claim: string) {

      let areas: string[] = [
        "Enrolment",
        "Establishment",
        "Exam",
        "Finance",
        "Infrastructure",
        "Inspection",
        "IST",
        "PA",
        "School",
        "Survey",
        "Teacher"
      ];

      let rights: any[][] = [
        ["Read", 1],
        ["ReadX", 2],
        ["Write", 4],
        ["WriteX", 8],
        ["Ops", 16],
        ["Admin", 32]
      ]

      let result = {};
      areas.forEach((areaname, idx) => {
        // get the claim
        let val = claim.charCodeAt(idx);
        let v = {};
        rights.forEach((r) => {
          // the name is the first index
          // the mask is the next
          v[r[0]] = ((val - 48) & r[1]) > 0 ? true : false;
        });
        result[areaname] = v;
      });
      return result;
    }

    private permissionGridToHash() {

      let areas: string[] = [
        "Enrolment",
        "Establishment",
        "Exam",
        "Finance",
        "Infrastructure",
        "Inspection",
        "IST",
        "PA",
        "School",
        "Survey",
        "Teacher"
      ];

      let rights: any[][] = [
        ["Read", 1, 1],
        ["ReadX", 2, 3],
        ["Write", 4, 5],
        ["WriteX", 8, 15],
        ["Ops", 16, 17],
        ["Admin", 32, 33]
      ]

      let result = "";
      areas.forEach((areaname, idx) => {
        // get the claim
        let areaRights = this.PermissionGrid[areaname];
        let v = 0;
        rights.forEach((r) => {
          if (areaRights[r[0]]) {
            v = v | r[2];   // this takes care of dependencies between permissions
          }
        });
        result += String.fromCharCode(v + 48);
      });
      return result;
    }
  }
}