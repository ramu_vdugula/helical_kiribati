﻿namespace Sw.Auth {

  export class Role extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    // constructor just expects an object that is the school data, not the related data
    // this can be called from new school
    constructor(data) {
      super();
      this._transform(data);
      angular.extend(this, data);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      return new Role(resultSet);
    };

    // IEditable
    public _name() {
      return (<any>this).Name;
    }
    public _type() {
      return "role";
    }
    public _id() {
      return (<any>this).Id
    }


    public _transform(newData) {
      return super._transform(newData);
    }
   
  }
}
