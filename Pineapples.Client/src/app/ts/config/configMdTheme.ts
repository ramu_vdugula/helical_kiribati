﻿namespace Pineapples {
  /**
   * see
   * https://material.angularjs.org/latest/Theming/03_configuring_a_theme
   *
   * Angularjs material provides this high level interface to configure palletes and themes
   * sooner or later we'll want to use this as we bid farewell to bootstrap and fully materialize
   * This is a default implementation, simply adapted from the documentation at the link above.
   * Note that currently many of the styles generated from this get overriden by our
   * 'quick and dirty' materialize fixes.
   * Note that all the styles relating to these themes get instantiated at run-time,
   * with 16 long <style tags> ( 4 palettes x 4 hues) added at start up.
   * See
   * https://material.angularjs.org/latest/Theming/05_under_the_hood
   */
  let configTheme = (themingProvider: ng.material.IThemingProvider) => {

    // Creating a new Pacific EMIS default palette
    themingProvider.definePalette('defaultPacificEMISPalette', {
      '50': 'e3edf7',
      '100': 'b9d1ec',
      '200': '8ab2e0',
      '300': '5b93d3',
      '400': '387cc9',
      '500': '1565c0',
      '600': '125dba',
      '700': '0f53b2',
      '800': '0c49aa',
      '900': '06379c',
      'A100': 'c8d7ff',
      'A200': '95b2ff',
      'A400': '628dff',
      'A700': '487aff',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100']
    });

    // Use that theme for the primary intentions
    themingProvider.theme('default')
      .primaryPalette("defaultPacificEMISPalette")
      .backgroundPalette("grey", {
        // here we override the default background color to use shade A100 (first accent)
        // from the grey palette. This color is actually white
        // Angularjs material uses this accent background for the backgrounds of 
        // md-autocomplete, md-select - ie popups
        // by setting this, we use it everywhere
        // note that when debugging at run-time, you can use
        // themingProvider._PALETTES to see predefined and user defined palettes
        "default": "A100"
      });

  }

  configTheme.$inject = ["$mdThemingProvider"];

  angular
    .module("pineapples")
    .config(configTheme);

}