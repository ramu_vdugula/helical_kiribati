﻿module Sw.Api {
  export interface IRestEx extends restangular.IElement {
    filterPaged: any;
    table: any;
    geo: any;
  }
  export class ApiFactory {
    public static getApi = (restangular: restangular.IService, errorService: any, name: string) => {
      let svc = <IRestEx>restangular.all(name);
      svc.addRestangularMethod("filterPaged", "post", "collection/filter");
      // svc.filterPaged = (fltr) => svc.customPOST(fltr, 'collection/filter').catch(errorService.catch);
      svc.table = (fltr) => svc.customPOST(fltr, 'collection/table').catch(errorService.catch);
      svc.geo = (fltr) => svc.customPOST(fltr, 'collection/geo').catch(errorService.catch);
      return svc;
    };
  }
}
