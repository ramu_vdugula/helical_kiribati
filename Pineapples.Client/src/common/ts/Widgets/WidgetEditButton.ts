﻿namespace sw {

  interface IBindings {
    action;
    isWaiting: boolean;
    form;
    showLabel: boolean;
  }

  class controller implements IBindings{
    public action;
    public isWaiting: boolean = false;
    public form;
    public showLabel: boolean = true;
  }
  class ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        action: "&",
        isWaiting: "<",
        form: "=",
        showLabel: "<"
      }
      this.controller = controller;
      this.controllerAs = "vm";
    }
  }

  class editButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();

      this.template =
        `<md-button class="md-raised md-primary" ng-click="vm.action()">
          <md-icon class="material-icons md-14">edit</md-icon>
          <span ng-show="vm.showLabel">Edit</span>
         </md-button>`;
    }
  }

  class saveButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.template =
        `<md-button class="md-raised md-primary" 
            ng-class="vm.form.$invalid ? 'invalid' : 'valid'"  ng-disabled="vm.isWaiting"
            ng-click="vm.action()">
            <div ng-if="vm.isWaiting" layout="row" layout-align="center center">
              <md-progress-circular ng-disabled="!vm.isWaiting" class="md-hue-2" md-diameter="14"></md-progress-circular>
            </div>
            <span ng-show="!vm.isWaiting"><md-icon class="material-icons md-14">save</md-icon> Save</span>
         </md-button>`;
    }
  }

  class undoButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.template =
        `<md-button class="md-raised" ng-disabled="vm.isWaiting" ng-click="vm.action()">
              <span><md-icon class="material-icons md-14">undo</md-icon> Undo changes</span>
        </md-button>`;
    }
  }

  class editUiButtonOptions extends ButtonComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      super();
      this.bindings = {
        editAction: "&",
        saveAction: "&",
        undoAction: "&",
        isWaiting: "<",
        isEditing: "<",
        form: "<",
        showLabels: "@"

      }
      this.template =
        `<div>
          <edit-button action="vm.editAction()" ng-show="!vm.isEditing" action=vm.editAction() show-label="vm.showLabels"></edit-button>
          <save-button action="vm.saveAction()" ng-show="vm.isEditing" is-waiting="vm.isWaiting" action=vm.saveAction() form="vm.form"></save-button>
          <undo-button action="vm.undoAction()" ng-show="vm.isEditing" action=vm.undoAction()></save-button>
        </div>
          `;
      this.controller = () => {
        return {};
      };
      this.controllerAs = "vm";
    }
  }

  angular
    .module("sw.common")
    .component("editButton", new editButtonOptions())
    .component("saveButton", new saveButtonOptions())
    .component("undoButton", new undoButtonOptions())
    .component("editUiButtons", new editUiButtonOptions());

}
