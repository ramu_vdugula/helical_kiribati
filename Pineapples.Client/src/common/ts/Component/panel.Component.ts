﻿namespace Pineapples.Components {

  class PanelController {
    public panelClass: string;
    public panelIcon: string;
    public panelClose: boolean;
    public panelControlCollapse: boolean = false;
    public heading: string;

    public $onChanges(changes) {
      if (changes.heading) {
      }
    }
    public $onInit() {

    }

    private _collapsed: boolean = false;
    public get collapsed() {
      return this._collapsed;
    }
    public toggleCollapse() {
      this._collapsed = !this._collapsed;
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;
    public transclude;

    constructor() {
      this.bindings = {
        panelClass: "@",
        panelIcon: "@",
        panelClose: "<",          // we want the literal true to be boolean true, not string "true"
        panelControlCollapse: "<",
        heading: "@"
      };
      this.transclude = {};
      this.transclude["panelControls"] = "?panelControls";
      this.controller = PanelController;
      this.controllerAs = "vm";
      this.templateUrl = "generic/panel";
    }
  }

  angular
    .module("pineapples")
    .component("panel", new Component());

}