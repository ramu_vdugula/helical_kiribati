﻿module Sw.Api {

  export interface IMapService extends restangular.IService {
    topo: (id: string) => any;
  }

  let factory = (restAngular, errorService) => {
    let svc: IMapService = restAngular.all("geo");
    svc.topo = (id: string) => {
      return svc.customGET("topo/" + id);
    };
    return svc;
  };

  angular.module("sw.common")
    .factory("mapAPI", ['Restangular', 'ErrorService', factory]);
}
