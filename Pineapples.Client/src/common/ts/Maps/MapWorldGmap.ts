﻿module Sw.Maps {

  interface CountryDatum {
    n3: string;
  }

  class DirectiveBase implements ng.IDirective {
    public static factory: () => any;

    public static register(module: string) {
      angular
        .module(module)
        .directive("worldmap", this.factory());
    }
  }

  interface IIsolateScope extends ng.IScope {
    width: number;
    height: number;
    world: any[];    // the topojson object, contains all elements that may possibly be drawn
    featureSet: string; // set of features in the topojsaon
		extent: { id }[];   // the data set defining the regions that will appear in the map
		values: { id }[];   // set of numeric data values for the entities   subset of entities in _extent
    idAccessor: (feature: any) => string;
    nameAccessor: (feature: any) => string;
    valueAccessor: (feature: any) => number;
    extentAccessor: (feature: any) => any;
    palette: IColorBrewerPalette;
    qAs: string;    // implement the colors with css styles or fill
    onMouseOver: any;
    onMouseOut: any;
    gmap: google.maps.Map;
  }

  class WorldGMap implements ng.IDirective {
    public template = '<map zoom="6" center="[-8, 154]" style="display:block; height:600px" fit-content></map>';
    public link: any;
    public compile: any;
    public scope = {
      width: "@",
      height: "@",
      world: "=",
      featureSet: "@",
      extent: "=",
      values: "=",
      idAccessor: "=",
      nameAccessor: "=",
      valueAccessor: "=",
      extentAccessor: "=",
      palette: "=",
      qAs: "@",
      onMouseOver: "=",
      onMouseOut: "=",
      gmap: "="
    };
    public restrict = "EA";

    private _scope: IIsolateScope;

    constructor() { // constructor gets list of dependencies too
      WorldGMap.prototype.compile = (tElem: ng.IAugmentedJQuery, tAttrs: ng.IAttributes, transclude: ng.ITranscludeFunction) => {
        return {
          pre: (scope: IIsolateScope, iElem: ng.IAugmentedJQuery, iAttrs: ng.IAttributes) => {
            // collect the google map when it is ready
            scope.$on('mapInitialized', (event, map) => {
              this._scope = scope;
              scope.gmap = map;
            });
          },
          post: (scope: IIsolateScope, element, attrs, ctrlr, transclude) => {

            // set up watchers()
            scope.$watch("values", () => {
              this._colorMap(scope, element);
            }); // don't test deep equality
            scope.$watch("world", () => {
              this._makeMap(scope, element);
              this._colorMap(scope, element);
            }); // don't test deep equality
            scope.$watch("extent", () => {
              this._makeMap(scope, element);
              this._colorMap(scope, element);
            }); // don't test deep equality
            scope.$watch("palette", () => {
              this._colorMap(scope, element);
            }, true); // deep equality needed
          }
        };
      };
    }

    // function mapping a feature to its value
    private _valueAccessor: (feature: any) => number =
    (feature) => {
      if (this._scope.valueAccessor) {
        return this._scope.valueAccessor(feature);
			}
			// cf https://stackoverflow.com/questions/50876817/visual-studio-angular-argument-of-type-is-not-assignable-to-parameter-type-obj
			// for why we need the type <{id}[]>
			return <number>_.result(_.find(this._scope.values, { id: this._idAccessor(feature) }), "value");
    };

    // return the id of the feature
    private _idAccessor: (feature: any) => string =
    (feature) => {
      if (this._scope.idAccessor) {
        return this._scope.idAccessor(feature);
      }
      return feature.getId().toString();
    };
    // return the object in extent related to this feature
    private _extentAccessor: (feature: any) => any =
    (feature) => {
      if (this._scope.extentAccessor) {
        return this._scope.idAccessor(feature);
      }
      return _.find(this._scope.extent, { id: this._idAccessor(feature) });
    };
    // return the name of the feature
    private _nameAccessor: (feature: any) => string =
    (feature) => {
      if (this._scope.nameAccessor) {
        return this._scope.nameAccessor(feature);
      }
      return feature.properties.name;
    };

    private _currentWorld: any;
    // colour definition
    // quantizing:
    // number of quantize bands
    private _colorScale: d3.scale.Quantize<string>;
    /**
     *
     * @returns {}
     */
    private _makeMap(scope, element) {
      if (!scope.gmap) {
        // return if we don't have the google map yet
        return;
      }

      if (!scope.world) {
        return;
      };
      if (scope.world === this._currentWorld) {
        return;
      }

      // under objects is a parent node for the geometries - the name of this may change
      let featureset = scope.featureSet || Object.keys(scope.world.objects)[0];
      let features = (<GeoJSON.FeatureCollection>topojson.feature(scope.world, scope.world.objects[featureset])).features;
      // let neighbors = topojson.neighbors(world.objects.countries.geometries);
      const filter = (f) => {
        return (this._extentAccessor(f) === undefined ? false : true);
      };

      // extent it the collection of countries that will be drawn
      // draw everything if no extent = []
      let extentFeatures;
      if (scope.extent.length === 0) {
        extentFeatures = features;
      } else {
        extentFeatures = _.filter(features, filter);
      };

      // remove any existing features
      scope.gmap.data.forEach((f) => { scope.gmap.data.remove(f); });
      // add the new ones
      var style = {
        strokeWeight: .5,
        fillOpacity: .1,
        fillColor: "pink",
        visible: true
      };
      scope.gmap.data.forEach((f) => { scope.gmap.data.remove(f); });
      scope.gmap.data.setStyle(style);
      features.forEach((f) => {
        scope.gmap.data.addGeoJson(f);
      });

      // work out the bounding box
      this._currentWorld = scope.world;
    }
    /**
     * Color the map
     * @param scope
     * @param element
     */
    private _colorMap(scope: IIsolateScope, element): void {
      if (!scope.gmap) {
        return;
      }
      if (!scope.world) {
        return;
      }
      if (scope.world !== this._currentWorld) {
        this._makeMap(scope, element);
      }

      const makeQuantize = () => {
        var m = <number>_.result(_.maxBy(scope.values, 'value'), 'value');
        return d3.scale.quantize<string>()
          .domain([0, m])
          .range(d3.range(scope.palette.bands).map((i) => {
            // here we need to get the actual color, from the js, not the css
            if (scope.palette.invert) {
              return colorbrewer[scope.palette.scheme][scope.palette.bands.toString()][scope.palette.bands - 1 - i];
            } else {
              return colorbrewer[scope.palette.scheme][scope.palette.bands.toString()][i];
            }
          }));
      };
      const getFeatureColor = (feature) => {
        // find the value for the related entity
        let value = this._valueAccessor(feature);

        // convert the value using the quantize scale
        // to a string, which is a CSS class name
        if (value === undefined) {
          return ""; // the accessor function shuld handle the case null
        }
        return this._colorScale(<number>value);
      };

      this._colorScale = makeQuantize();

      let stylingFunction = (f) => {
        let color = getFeatureColor(f);
        return {
          strokeWeight: .5,
          fillOpacity: .5,
          fillColor: color,
          visible: true
        };
      };
      scope.gmap.data.setStyle(stylingFunction);
    }

  public static factory() {
      var directive = () => {
        return new WorldGMap();
      };
      return directive;
    }
  }

  angular
    .module("sw.common")
    .directive("worldGmap", WorldGMap.factory());
}
