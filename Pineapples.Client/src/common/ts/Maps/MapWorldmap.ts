﻿module Sw.Maps {

  interface IIsolateScope extends ng.IScope {
    width: number;
    height: number;
    world: any[];    // the topojson object, contains all elements that may possibly be drawn
    featureSet: string; // set of features in the topojsaon
		extent: { id }[];   // the data set defining the regions that will appear in the map
		values: { id }[];   // set of numeric data values for the entities   subset of entities in _extent
    idAccessor: (feature: any) => string;
    nameAccessor: (feature: any) => string;
    valueAccessor: (feature: any) => number;
    extentAccessor: (feature: any) => any;
    palette: IColorBrewerPalette;
    mapColorScale: MapColorScale;
    qAs: string;    // implement the colors with css styles or fill
    onMouseOver: any;
    onMouseOut: any;
    vm: WorldMapController;
    backColor: any;
  }

  class WorldMapController {

    public scope: IIsolateScope;
    private d3svg: any;
    private d3legend: any;
    private _colorScale: d3.scale.Threshold<number, any>;
    private _currentWorld: any;         // the loaded topojson
    private _currentHeight: number;     // the size of the element when the map was rendered
    private _currentWidth: number;

    public elementHeight: number;
    public elementWidth: number;

    // wrappers around functions that may be supplioed obn the directive
    // to provide default implementations

    private _valueAccessor: (feature: any) => number =
      (feature) => {
        if (this.scope.valueAccessor) {
          return this.scope.valueAccessor(feature);
        }
        return <number>_.result(_.find(this.scope.values, { id: this._idAccessor(feature) }), "value");
    };

    // return the id of the feature
    private _idAccessor: (feature: any) => string =
    (feature) => {
      if (this.scope.idAccessor) {
        return this.scope.idAccessor(feature);
      }
      return feature.id.toString();
    };

    // return the object in extent related to this feature
    private _extentAccessor: (feature: any) => any =
    (feature) => {
      if (this.scope.extentAccessor) {
        return this.scope.idAccessor(feature);
      }
      return _.find(this.scope.extent, { id: this._idAccessor(feature) });
    };

    // return the name of the feature
    private _nameAccessor: (feature: any) => string =
    (feature) => {
      if (this.scope.nameAccessor) {
        return this.scope.nameAccessor(feature);
      }
      return feature.properties.name;
    };

    /**
     *
     * @returns {}
     */
    public makeMap(scope, element) {
      if (!scope.world) {
        return;
      };
      if (scope.world === this._currentWorld
        && this._currentHeight === element[0].offsetHeight
        && this._currentWidth === element[0].offsetWidth) {
        return;
      }
      let svg = $(element).find("svg.worldmap g.map");
      this.d3svg = d3.select(svg.toArray()[0]);
      svg = $(element).find("svg.worldmap g.legend");
      this.d3legend = d3.select(svg.toArray()[0]);
      let self = this;
      let myMouseOver = function (feature, i) {
        let elem = d3.select(this);
        console.log(elem.attr("id"));
        console.log();
        scope.onMouseOver.call(scope, self._nameAccessor(feature), feature, this);
      };

      // under objects is a parent node for the geometries - the name of this may change
      let featureset = scope.featureSet || Object.keys(scope.world.objects)[0];
      let features = (<GeoJSON.FeatureCollection>topojson.feature(scope.world, scope.world.objects[featureset])).features;
      // let neighbors = topojson.neighbors(world.objects.countries.geometries);
      const filter = (f) => {
        return (this._extentAccessor(f) === undefined ? false : true);
      };

      // extent it the collection of countries that will be drawn
      // draw everything if no extent = []
      let extentFeatures;
      if (scope.extent === undefined || scope.extent.length === 0) {
        extentFeatures = features;
      } else {
        extentFeatures = _.filter(features, filter);
      };

      //        color = d3.scale.category20(),
      //        graticule = d3.geo.graticule();

      // work out the bounding box
      let mercator = d3.geo.mercator();           // d3.geo.kavrayskiy7(),
      let path = d3.geo.path()
        .projection(mercator);

      //  other attemots at projection functions
      // .projection(null);
      // .projection(([x, y]: [number, number]) => [x, y * -1] as [number, number]);

      let mn = (a: number, b: number) => a < b ? a : b;
      let mx = (a: number, b: number) => a < b ? b : a;

      let featureBBs: any[] = extentFeatures.map((feature) => path.bounds(feature));
      let bounds: GeoJSON.Position[] = featureBBs.reduce((prev, current, index, array) =>
        [
          [
            mn(current[0][0], prev[0][0]),
            mn(current[0][1], prev[0][1])
          ],
          [
            mx(current[1][0], prev[1][0]),
            mx(current[1][1], prev[1][1])
          ]
        ]
      );
      let dx = bounds[1][0] - bounds[0][0];
      let dy = bounds[1][1] - bounds[0][1];
      let x = (bounds[0][0] + bounds[1][0]) / 2;
      let y = (bounds[0][1] + bounds[1][1]) / 2;
      let gWidth = element[0].offsetWidth * .8;

      let attrH = element[0].getAttribute("height");
      if (isNaN(+attrH)) {
        attrH = 0;
      }

      let gHeight = Math.max(element[0].offsetHeight, +attrH) ;
      let scale = .9 / Math.max(dx / gWidth, dy / gHeight);
      let translate = [gWidth / 2 - scale * x, gHeight / 2 - scale * y];

      this.d3svg
        .transition()
        .duration(750)
        .style("stroke-width", 1.5 / scale + "px")
        .attr("transform", "translate(" + translate + ")scale(" + scale + ")"); // assume y co-ordinates are reversed

      let update = this.d3svg.selectAll(".feature")
        .data(extentFeatures, this._idAccessor);

      update.enter()
        .insert("path", ".graticule") // insert before the graticule
        .attr("class", "feature")
        .attr("id", this._idAccessor)
        .on("click", myMouseOver)
        // .on("mouseout",  scope.onMouseOut)
        .attr("d", path)
        .append("svg:title")
        .text(this._nameAccessor);
      update.exit()
        .remove();

      this._currentWorld = scope.world;
      this._currentHeight = element[0].offsetHeight;
      this._currentWidth = element[0].offsetWidth;
    }

    public colorMap(scope: IIsolateScope, element): void {
      if (!scope.world) {
        return;
      }
      if (scope.world !== this._currentWorld) {
        this.makeMap(scope, element);
      }

      this._colorScale = scope.mapColorScale.getColorBrewerScale(scope.palette.bands, scope.palette.invert);

      switch (scope.qAs) {
        case "fill":
          this._colorMapFill(scope, element);
          break;
        default:
          this._colorMapCss(scope, element);
          break;
      }
    }

    private _colorMapCss(scope: IIsolateScope, element): void {
      // set up the helper functions
      // return the quantize scale to convert a value into a band => into a css class name

      // accessor function to get the color for the feature
      let getFeatureColor = (feature) => {
        // find the value for the related entity
        let value = this._valueAccessor(feature);

        // convert the value using the quantize scale
        // to a string, which is a CSS class name
        if (value === undefined) {
          return this._colorScale.range()[0];    // the accessor function treats null as 0
        }
        return this._colorScale(<number>value);
      };

      let getTitle = (feature) => {
        // find the value for the related entity
        let value = this._valueAccessor(feature);
        let name = this._nameAccessor(feature);
        if (value === undefined || value === null) {
          return name;
        } else {
          return name + ": " + value.toString();
        }
      };

      // with the helper functions defined, set the color
      // if working with Css, we need
      this.d3svg
        .attr("class", "map " + scope.palette.scheme)
        .selectAll(".feature")
        .attr("class", getFeatureColor)
        .classed("feature", true)
        .selectAll("title")
        .text(getTitle);

      let update: d3.selection.Update<any> = this.d3legend
        .attr("class", "legend " + scope.palette.scheme)
        .selectAll("g")
        .data(this._colorScale.range());

      let g = update.enter()
        .append("g");

      g
        .append("rect");

      g
        .append("text");

      update
        .select("rect")     // subtle d3 thing - select, not selectAll - propogates the current data
        .attr("x", 0)
        .attr("y", (d, i, o) => i * 15) // with selectAll, its a [[]] so you need o
        .attr("height", 10)
        .attr("width", 10)
        .attr("class", (d) => d);

      update
        .select("text")
        .attr("x", 20)
        .attr("y", (d, i, o) => i * 15 + 10)

        .text((d, i, o) => {
          return scope.mapColorScale.getBandLabel(i);
        });

      update.exit()
        .remove();

    }

    private _colorMapFill(scope: IIsolateScope, element): void {
      // implementation using fill rather than css
    }

    public onElementResize = (scope, element) => {
      this.makeMap(scope, element);
      this.colorMap(scope, element);
    };

    public mapBackStyle() {
      let backColor = this.scope.backColor || "royalblue";
      return {
        fill: backColor
      };
    }
  }

  class WorldMap implements ng.IDirective {
    public template = ['<svg class="worldmap" height="{{height}}" width="{{width}}">',
      '<rect height="{{height}}" width="{{width}}" ng-style="vm.mapBackStyle()" opacity=".4"/>',
      '<g class="map" width="{{width * .8}}"></g>',
      '<g class="legend" transform="translate({{width * .8}}, 0)" width="{{width * .2}}"></g>',
      '</svg>'].join("");
    public link: any;
    public scope = {
      width: "@",
      height: "@",
      world: "=",
      featureSet: "@",
      extent: "=",
      values: "=",
      idAccessor: "=",
      nameAccessor: "=",
      valueAccessor: "=",
      extentAccessor: "=",
      palette: "=",
      mapColorScale: "=",
      qAs: "@",
      onMouseOver: "=",
      onMouseOut: "=",
      backColor: "@"
    };

    public restrict = "EA";
    public controller = WorldMapController;
    public controllerAs = "vm";

    constructor() { // constructor gets list of dependencies too
      WorldMap.prototype.link = (scope: IIsolateScope, element, attrs, ctrlr: WorldMapController) => {
        // trap the svg element
        ctrlr.scope = scope;
        scope.vm = ctrlr;     // COM programmers: be afraid!!

        // set up watchers()
        scope.$watch("values", () => {
          ctrlr.colorMap(scope, element);
        });         // don't test deep equality
        scope.$watch("world", () => {
          ctrlr.makeMap(scope, element);
          ctrlr.colorMap(scope, element);
        });  // don't test deep equality
        scope.$watch("extent", () => {
          ctrlr.makeMap(scope, element);
          ctrlr.colorMap(scope, element);
        }); // don't test deep equality
        scope.$watch("palette", () => {
          ctrlr.colorMap(scope, element);
        }, true); // deep equality needed

        scope.$watch("mapColorScale", () => {
          ctrlr.colorMap(scope, element);
        }, true); // deep equality needed

        // a generl purpose pattern for a responsive directive?
        // see if the element has changed height or width
        scope.$watchGroup([
          () => element[0].offsetHeight,
          () => element[0].offsetWidth
        ],
        (newValue) => {
          ctrlr.onElementResize(scope, element);
        });

      };
    }

    public static factory() {
      var directive = () => {
         return new WorldMap();
      };
      return directive;
    }
  }

  angular
    .module("sw.common")
    .directive("worldmap", WorldMap.factory());
}
