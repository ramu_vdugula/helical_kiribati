﻿module Sw {
  // simple bar chart directive that highlights one datum
  // args -
  // data - a data array
  // keyField - the property in the data that represents the key
  // hiliteValue - key value for the highlighted point
  // yField - property for the y value
  // yLabel - label for the y values
  // hiliteClass - class for the hilite bar

  // define the IsolateScope of the directive
  // this will include those members bound to the directive attributes
  // and any other values we may want to place on scope
  // we can use the generic name IIsolateScope becuase it is not exported outside this closure
  interface IIsolateScope extends ng.IScope {
    height: number;
    width: number;
    data: any[];
    // keyField - the property in the data that represents the key
    // hiliteValue - key value for the highlighted point
    // yField - property for the y value
    // yLabel - label for the y values
    // hiliteClass - class for the hilite bar
    keyField: string;
    yField: string;
    yLabel: string;
    hiliteValue: any;
    hiliteClass: string;   // field to format the hilited point
  }

  // declaration of the class that implements the directive
  // does not need to be exported
  class HiliteChart implements ng.IDirective {
    // properties of the directive are public properties of this class

    // scope defines the bindings from the directive attributes to the isolate scope
    // it is not the scope itself
    public scope = {
      height: '@',
      width: '@',
      data: "=dataset",
      // keyField - the property in the data that represents the key
      // hiliteValue - key value for the highlighted point
      // yField - property for the y value
      // yLabel - label for the y values
      // hiliteClass - class for the hilite bar
      keyField: '@',
      yField: '@',
      yLabel: '@',
      hiliteValue: '@',
      hiliteClass: '@' // field to format the hilited point
    };
    public restrict = "EA";
    public template = "<svg class=\"hiliteChart\" height={{height}} width={{width}}></svg>";

    public link: ng.IDirectiveLinkFn;

    // hold some private variables relating to the chart
    private _dataset: Plottable.Dataset;
    private _chart: Plottable.Components.Table;

    // the arguments to the constructor are the injected dependencies
    constructor() {

      // define the directive link function in the constructor
      // put it on the prototype
      HiliteChart.prototype.link = (scope: IIsolateScope, element, attrs) => {
        // link function sets up any required watches on the scope
        // this allows the chart to respond to chnages in the controller
        scope.$watch("data", function (newValue, oldValue) {
          if (newValue) {
            this._drawChart(scope, element);
          }
        }, true);

        scope.$watch("hiliteClass", function (newValue, oldValue) {
          if (newValue) {
            this._drawChart(scope, element);
          }
        }, true);

        scope.$watch("hiliteValue", function (newValue, oldValue) {
          if (newValue) {
            this._drawChart(scope, element);
          }
        }, true);
      };
    }

    // function to establish the chart objects and render
    private _makeChart = (scope: IIsolateScope, element) => {
      // use plottable to make the chart
      // very clean - no axes?

      // ------ datasets -----------------
      this._dataset = new Plottable.Dataset()
        .keyFunction((d, i) => d[scope.keyField]);

      // ------ scales, axes, axislabels
      let xScale: Plottable.Scale<number, number> = new Plottable.Scales.Linear();
      let yScale: Plottable.Scale<number, number> = new Plottable.Scales.Linear();

      // -----------animators
      var barAnimator = new Plottable.Animators.Easing()
        .stepDuration(300)
        .stepDelay(0);

      // the chart
      let cht = new Plottable.Plots.Bar()
        .addDataset(this._dataset)
        .x((d, i) => i, xScale)
        .y((d) => d[scope.yField], yScale)
        .attr("opacity", (d) => (d[scope.keyField] === scope.hiliteValue ? 1 : .5))
        .attr("schNo", (d) => d.schNo)
        .attr("index", (d, i) => i)
        .attr("fill", (d) => (d[scope.keyField] === scope.hiliteValue ? 'red' : 'gray'))
        .addClass("hilitebar");

      /// ----- make a table component for the chart
      this._chart = new Plottable.Components.Table([
        [cht]
      ]);

      //// Create
      // this is a bit of translation to turn a jQuery element to a d3 element
      let svg = $(element).find('svg.hiliteChart');
      let d3svg = d3.selectAll(svg.toArray());
      this._chart.renderTo(d3svg);
    };

    private _drawChart = (scope, element) => {
      if (!scope.chart) {
        this._makeChart(scope, element);
      }
      // set up the data
      this._dataset.data(_.sortBy(scope.data, scope.yField));
    };

    // directive always get the factory method
    public static factory() {
      let directive = () => {
        return new HiliteChart(); // (/*list of dependencies*/);
      };
      // directive['$inject'] = ['/*list of dependencies*/'];
      return directive;
    }
  }

  angular
    .module('sw.common')
    .directive('hiliteBarChart', HiliteChart.factory());
}
