﻿module Sw.Filter {

  /**
   * @name Sw.Filter.UtilityFilters
   * @description class that holds mostly static public methods all with simple
   * utility behaviors for formatting output in the UI
   */
  class UtilityFilters {

    /**
     * @name Sw.Filter.UtilityFilters.capitalize
     * @description Capitalizes the first letter of a string (default behavior)
     * or all the letter of a string.
     *
     * @returns {function} Refer to anonmous function below
     *
     * @example <h1>{{listvm.theFilter.entity|capitalize:first}}</h1>
     */
    public static capitalize() {

      /**
       * @description anonymous filter function doing the actual capitalize work
       *
       * @param {string} input The string to be formatted.
       * @param {string} [format] The format to be applied being the options 'first' (default) or 'all'.
       * @param {string} [separator] The character(s) to be used for separating the string (e.g. go from
       *                             "school-entity" to "School entity"). Space is the default.
       *
       * @returns {string} the capitalized string
       */
      let fcn = (input: string, format: string, separator: string) => {
        if (!input) {
          return input;
        }

        format = format || 'first';
        separator = separator || ' ';

        if (format === 'first') {
          let words = input.split(separator);
          let output = [];
          words.forEach(w => {
            output.push(w.charAt(0).toUpperCase() + w.slice(1).toLowerCase());
          });
          return output.join(' ');
        }
      }

      return fcn;

    }

  }

  angular
    .module("sw.common")
    .filter("capitalize", UtilityFilters.capitalize);

}
