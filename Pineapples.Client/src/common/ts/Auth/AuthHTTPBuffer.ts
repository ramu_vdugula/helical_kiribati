﻿module Sw.Auth {

  interface IBufferedRequest {
    config: any;
    deferred: ng.IDeferred<any>;
  };

  export interface IHttpBuffer {
    append(config: any, deferred: ng.IDeferred<any>): void;
    retry(config: any, deferred: ng.IDeferred<any>): void;
    rejectAll(reason: any): void;
    retryAll(): void;
  }

  class HttpBuffer implements IHttpBuffer {

    private _http: ng.IHttpService;

    private _buffer: IBufferedRequest[] = [];
    static $inject = ["$injector"];

    constructor(private _injector: ng.auto.IInjectorService) { }

    public retry(config, deferred: ng.IDeferred<any>) {
      let successCallback = (response: any) => {
        deferred.resolve(response);
      };
      let errorCallback = (response: any) => {
        deferred.reject(response);
      };
      this._http = this._http || this._injector.get<ng.IHttpService>("$http");
      this._http(config).then(successCallback, errorCallback);
    }
    /**
     * Appends HTTP request configuration object with deferred response attached to buffer.
     */
    public append = (config: any, deferred: ng.IDeferred<any>) => {
      this._buffer.push({
        config: config,
        deferred: deferred
      });
    };
    /**
     * Abandon or reject (if reason provided) all the buffered requests.
     */
    public rejectAll = (reason: any) => {
      if (reason) {
        for (var i = 0; i < this._buffer.length; ++i) {
          this._buffer[i].deferred.reject(reason);
        }
      }
      this._buffer = [];
    };

    /**
     * Retries all the buffered requests clears the buffer.
     * updater is a function applied to the config - it will add the new bearer token
     */
    public retryAll = () => {
      for (var i = 0; i < this._buffer.length; ++i) {
        let buffer = this._buffer[i];
        this.retry(buffer.config, buffer.deferred);
      }
      this._buffer = [];
    };

  }
  angular
    .module("sw.common")
    .service('httpBuffer', HttpBuffer);
}
