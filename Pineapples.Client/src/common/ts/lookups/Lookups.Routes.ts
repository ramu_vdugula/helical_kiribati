﻿module Sw.Lookups {
  let routes = ($stateProvider) => {
    // root state for 'school' feature
    let state: ng.ui.IState;
    state = {

      url: "^/tables",
      //params: { id: null, columnField: null, rowData: {} },
      views: {
        "@": {
          component: "componentTableSelector"
        }
      },
      resolve: {
        tableList: ["lookupsAPI", (api) => {
          return api.getEditableTables();
        }]
      }
    };
    $stateProvider.state("site.lookups", state);
    state = {

      url: "^/tables/{lkp}",
      //params: { id: null, columnField: null, rowData: {} },
      views: {
        "tablegrid@site.lookups": {
          component: "superGrid"
        }

      },
      resolve: {
        dataResult: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
          return restangular.all($stateParams.lkp).getList();
        }],
        entityType: ['$stateParams', ($stateParams) => {
          return $stateParams.lkp;
        }],
        viewMode: ['$stateParams', 'Restangular', ($stateParams, restangular: restangular.IService) => {
          return restangular.all($stateParams.lkp).get("info/viewmode");
        }],
        isEditing: () => true
      }
    };
    $stateProvider.state("site.lookups.lookup", state);

  }
  angular
    .module("sw.common")
    .config(["$stateProvider", routes]);
}
