﻿/* ------------------------------------------
    lookupsAPI
    ----------------------------------------
*/
namespace Sw.Lookups {

  export class LookupsApi {
    protected _wreqService: restangular.IElement;
    static $inject = ["Restangular","$http"];
    constructor(restangular: restangular.IService, private http: ng.IHttpService) {
      this._wreqService = restangular.all("lookups");
    }
		public core = () => this.http.get("api/lookups/collection/core");		//this._wreqService.customGET('collection/core');
    public getList = (name: string) => this._wreqService.customGET("getlist/" + name);
    public post = (entry: Sw.Lookups.ILookupEntry) => this._wreqService.customPOST(entry, entry.C);
    public getEditableTables = () => this._wreqService.customGET("editabletables");
  }
  angular
    .module('sw.common')
    .service('lookupsAPI', LookupsApi);
}
