﻿declare module d3 {
  export var tip: () => d3tip.tip;
}

declare module d3tip {
  export enum direction {
    "n",
    "s",
    "e",
    "w",
    "nw",
    "ne",
    "sw",
    "se"
  }
  export interface tip {
    direction() :direction;
    direction(d: direction): tip;

    offset(): [number, number];
    offset(xy: [number, number]): tip;

    html(): string;
    html(string): tip;

    show(): tip;
    hide(): tip;

    attr(n: string): any;
    attr(n: string, v: any): tip;

    destroy(): void;
  }
}
